import ez_setup
# automatically install setuptools if the end user hasn't
ez_setup.use_setuptools(version="6.0.2")
from setuptools import setup, find_packages

setup(
  name="allostery", 
  version="0.0.0.a1",
  author = "Yuhang Wang",
  #author_email = "stevenyhw@gmail.com",
  description = "This package deals with analysis of MD trajectories in order to understantd allostery.",
  license = "CC BY-NC-SA 4.0",
  keywords = "allostery",
  #url = "http://creativecommons.org/licenses/by-nc-sa/4.0/",
  platform = "Mac OS; Linux; Unix",
  install_requires = [ 
	   "pandas",
	   "numpy",
	   "matplotlib",
	   "toposort",
	   ],
  packages = find_packages(),
)
#setup(name="allostery.core", version="0.0.0")
#setup(name="allostery.iolib", version="0.0.0")
#setup(name="allostery.atomselectionlib", version="0.0.0")
