# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: manager

    Role: an interface for talking to the actual data processing classes/proxies
    Developer Note: It plays the role of "controller" in the MVC design pattern
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-17-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import numpy
import os
import re
#------------------------------
# `allostery` package modules
#------------------------------
from allostery.iolib import dataio
from allostery.mathcalc.matrixlib import MatrixCalc
from allostery.plotting.plot_matrix_manager import PlotMatrix


#========================================================================
#                   [[[ CommuteTimeMixin ]]]
#========================================================================
class CommuteTimeMixin(object):
    """
    .. py:class:: CommuteTimeMixin()

        Role: calculate commute time
        :param iterObj: an instance object with methods:
                ".iter_get_pairwise_com_coords()"
        :param atomsObj: an instance object that knows about the atom selection
    """
    def __init__(self, **kwargs):
        self.universeObj = kwargs['universe']
        self.iteratorObj = kwargs['IteratorMixin']

        # external dependencies
        self.plotMatrixMixin = PlotMatrix()
        self.attr_plotMatrixMixin = numpy.array(dir(self.plotMatrixMixin), dtype=numpy.str_)
        self.attr_self_builtin = [name for name in dir(CommuteTimeMixin) if not re.match(r'^_.*', name)]
        self.public_method_register = self.attr_self_builtin

        self.tick_label_offset = None
        self.figObj = None
        self.axisObj = None

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: CommuteTimeMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: set of class objects
        """
        return {'IteratorMixin'}

    def get_commute_time(self, reCalc=True, frame_start=0, frame_end=None, frame_skip=1,
                         mode="com", *args, **kwargs):
        """
        .. py:method:: CommuteTimeMixin.get_commute_time()

            Role: compute the commute time matrix
            :param bool reCalc: if "True" (default), then re-calculate commute time matrix
            :param int frame_start: starting frame (0-based); default: 0
            :param int frame_end: ending frame (0-based); default: last frame
            :param int frame_skip: frame window size; default: 1 (every frame)
            :param str mode: options:
                        "com": use center of mass of each selected residues
                        "allatom": use the coordinates of every selected atoms
            :return: commute time matrix
            :rtype: numpy array
            :raise: UserWarning("Error Hint: please select a mode") if no mode is specified
        """
        if frame_end == None:
            frame_end = self.universeObj.get_total_number_of_frames()

        if reCalc is True: # re-calculate commute time
            if mode == "com":
                _iterObj = self.iteratorObj.iter_get_pairwise_com_distances(frame_start=frame_start,
                                frame_end=frame_end,frame_skip=frame_skip)
            elif mode == "allatom":
                _iterObj = self.iteratorObj.iter_get_pairwise_distances(frame_start=frame_start,
                                frame_end=frame_end,frame_skip=frame_skip)
            else:
                msg = "Error Hint: please select a mode"
                raise UserWarning(msg)
            total_number_of_particles = self.universeObj.get_number_of_selected_residues()

            self.commute_time = MatrixCalc.calc_matrix_variance(_iterObj,total_number_of_particles)
        else:
            self.__check_attr_exists('commute_time')

        return self.commute_time

    def __check_attr_exists(self,attr_name):
        """
        Role: check whether self.commute_time exists. If not, raise a warning.
        """
        if not hasattr(self, attr_name):
            msg = "Error Hint: you need to compute '{0}' first.\n".format(attr_name)
            raise UserWarning(msg)
        else:
            return True

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Return a list of public methods for class

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()
