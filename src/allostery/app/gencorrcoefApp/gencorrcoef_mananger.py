# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: app.correlation_gencorrcoef

    Role: generalized correlation coefficient
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '12-11-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import numpy
import ctypes
from scipy.sparse import bsr_matrix as sparse_matrix_maker
import scipy.sparse as sparse
from multiprocessing import Queue,Process
import multiprocessing
# from IPython.parallel import Client
from ipyparallel import Client
import pp
import re
import time

#------------------------------
# `allostery` package modules
#------------------------------
from allostery.app.gencorrcoefApp.generalized_correlation_coefficient import GenCorrCoefMixin

class GenCorrCoefApp(object):
    """
    .. py:class:: GenCorrCoefApp
        Role: compute generalized correlation for a trajectory
    """

    def __init__(self, rand_seed=None):
        super(GenCorrCoefApp, self).__init__()
        self.genCorrCoefObj = GenCorrCoefMixin(rand_seed)

    def calc_gen_corr_coef_serial(self,
                                   make_trajectory_iter,
                                   n_selected_frames,
                                   k,
                                   algo,
                                   method,
                                   norm_type):
        """
        .. py:method:: CorrelationMixin._calc_gen_corr_coef_serial
            Role: calculate the generalized correlation coefficient
            :param make_trajectory_iter:
            :param n_selected_frames:
            :param k:
            :param algo:
            :param method:
            :param norm_type:
            :return: a matrix of generalized correlation coefficient

            Developer's note:
            Idea:
            Compute the pairwise generalized correlation coefficient
              only for j>i, since the value = 1 for i=j.
              the lower triangluar part can be maded
              by adding its transpose to itself.
        """

        ## make a trajectory iterator
        trajIter, n_selected_particles = make_trajectory_iter()

        ## store the calculate pairwise geneneralized coorelation coefficients
        _n_upper_triangle_elemennts = int(0.5*(n_selected_particles**2-n_selected_particles))
        _gen_corr_coef = numpy.zeros(_n_upper_triangle_elemennts)

        ## initialize the row/column indices for the sparse version of the output matrix
        _row_ids = numpy.zeros(_n_upper_triangle_elemennts)
        _col_ids = numpy.zeros(_n_upper_triangle_elemennts)

        ccc = 0 # counter
        for _i in range(0, n_selected_particles-1):
            _Xi = self.__get_one_particle_trajectory(_i,trajIter,n_selected_frames)
            for _j in range(_i+1, n_selected_particles):
                _row_ids[ccc] = _i
                _col_ids[ccc] = _j
                ## restart the trajectory iterator
                trajIter, _n_selected_particles = make_trajectory_iter()
                _Xj = self.__get_one_particle_trajectory(_j,trajIter,n_selected_frames)

                ##======================================================================
                ## Now, compute Generalized Correlation Coefficients
                ##======================================================================
                _gen_corr_coef[ccc] = \
                    self.genCorrCoefObj.get_generalized_correlation_coefficient(_Xi,
                                                                                _Xj,
                                                                                k,
                                                                                algo,
                                                                                method,
                                                                                norm_type)
                if numpy.isnan([_gen_corr_coef[ccc]]):
                    print("NaN found")
                    print("Xi=\n",_Xi)
                    print("Xj=\n",_Xj)


                #------------------
                # increase counter
                #------------------
                ccc += 1

            #-----------------------------------------------------------------------
            # re-initialize the iterator object
            #-----------------------------------------------------------------------
            trajIter, _ = make_trajectory_iter()
            #--------------------------------------------------------------------------------

        ## Note: in order to form a square shaped sparse matrix,
        #  we still need one more element: the last entry.
        #  Otherwise, the created sparse matrix will be rectangular
        #   (limitation of scipy sparse matrix)
        # ID of last particle (0-based indexing)
        _id_last = n_selected_particles - 1
        _row_ids = numpy.append(_row_ids,_id_last)
        _col_ids = numpy.append(_col_ids,_id_last)

        # for now, just set the value of the last entry to 0
        # later on, it will be replaced by 1, which is
        # the correlation coefficient of the last element with
        # itself.
        _tmp_value = 0
        _gen_corr_coef = numpy.append(_gen_corr_coef,_tmp_value)

        upperTriSparse = sparse_matrix_maker((_gen_corr_coef, (_row_ids,_col_ids)))

        ## now create a symmetric generalized correlation coefficients
        genCorrCoefSparseMatrix = upperTriSparse + upperTriSparse.transpose() + \
                                  sparse.eye(n_selected_particles)

        return genCorrCoefSparseMatrix.todense()

    def calc_gen_corr_coef_parallel_mp(self,
                                   make_trajectory_iter,
                                   n_selected_frames,
                                   k,
                                   algo,
                                   method,
                                   norm_type):
        """
        .. py:method:: CorrelationMixin._calc_gen_corr_coef_parallel_mp
            Role: (Parallel) calculate the generalized correlation coefficient
                using multiprocessing
            :param make_trajectory_iter:
            :param n_selected_frames:
            :param k:
            :param algo:
            :param method:
            :param norm_type:
            :return: a matrix of generalized correlation coefficient

            Developer's note:
            Idea:
            Compute the pairwise generalized correlation coefficient
              only for j>i, since the value = 1 for i=j.
              the lower triangluar part can be maded
              by adding its transpose to itself.
        """

        ## make a trajectory iterator
        trajIter, n_selected_particles = make_trajectory_iter()

        ## store the calculate pairwise geneneralized coorelation coefficients
        _n_upper_triangle_elemennts = int(0.5*(n_selected_particles**2-n_selected_particles))
        gen_corr_coef = numpy.zeros(_n_upper_triangle_elemennts)

        ## initialize the row/column indices for the sparse version of the output matrix
        row_ids = numpy.zeros(_n_upper_triangle_elemennts)
        col_ids = numpy.zeros(_n_upper_triangle_elemennts)

        ccc = 0 # counter
        ## First make a shareable array
        n_dim = 3 # number of dimensions
        # total number of floating point numbers needed to store the trajectory
        n_floats_trajecotry = n_dim*n_selected_particles*n_selected_frames
        # make a shared raw C-array
        shared_trajectory_base = multiprocessing.Array(ctypes.c_double,
                                                       n_floats_trajecotry,
                                                       lock=False)
        all_traj = numpy.frombuffer(shared_trajectory_base,dtype=ctypes.c_double)
        all_traj = all_traj.reshape((n_selected_frames, n_selected_particles, n_dim))

        for _coord in trajIter:
            all_traj[ccc,:,:] = _coord[:]
            ccc += 1

        def _worker(i,j, outputQueue):
            """
            Role: an individual worker who will calculate the
                generalized correlation coefficient
            """
            ## get trajectories for particle i and j
            Xi = all_traj[:,i,:]
            Xj = all_traj[:,j,:]
            ##======================================================================
            ## Now, compute Generalized Correlation Coefficients
            ##======================================================================
            gen_corr_coef = \
                self.genCorrCoefObj.get_generalized_correlation_coefficient(Xi,
                                                                            Xj,
                                                                            k,
                                                                            algo,
                                                                            method,
                                                                            norm_type)
            if numpy.isnan(gen_corr_coef):
                print("NaN found at position [{0},{1}]".format(i,j))

            outputQueue.put((i, j, gen_corr_coef))
            return


        outputQueue = Queue()
        all_processes = []
        ## define processes
        for _i in range(0, n_selected_particles-1):
            for _j in range(_i+1, n_selected_particles):
                all_processes.append(Process(target=_worker,
                                             args=(_i,_j,outputQueue)))

        ## run all processes
        for p in all_processes:
            p.start()

        ## exit completed processes
        for p in all_processes:
            p.join()

        ## collect results
        for i in range(len(all_processes)):
            _result = outputQueue.get()
            row_ids[i] = _result[0]
            col_ids[i] = _result[1]
            gen_corr_coef[i] = _result[2]

        ## Note: in order to form a square shaped sparse matrix,
        #  we still need one more element: the last entry.
        #  Otherwise, the created sparse matrix will be rectangular
        #   (limitation of scipy sparse matrix)
        # ID of last particle (0-based indexing)
        _id_last = n_selected_particles - 1
        row_ids = numpy.append(row_ids,_id_last)
        col_ids = numpy.append(col_ids,_id_last)

        # for now, just set the value of the last entry to 0
        # later on, it will be replaced by 1, which is
        # the correlation coefficient of the last element with
        # itself.
        _tmp_value = 0
        gen_corr_coef = numpy.append(gen_corr_coef,_tmp_value)

        upperTriSparse = sparse_matrix_maker((gen_corr_coef, (row_ids,col_ids)))

        ## now create a symmetric generalized correlation coefficients
        genCorrCoefSparseMatrix = upperTriSparse + upperTriSparse.transpose() + \
                                  sparse.eye(n_selected_particles)

        return genCorrCoefSparseMatrix.todense()

    def calc_gen_corr_coef_parallel_ipython(self,
                                   make_trajectory_iter,
                                   n_selected_frames,
                                   k,
                                   algo,
                                   method,
                                   norm_type):
        """
        .. py:method:: CorrelationMixin._calc_gen_corr_coef_ipython
            Role: (Parallel) calculate the generalized correlation coefficient
                using ipython parallel engine
            :param make_trajectory_iter:
            :param n_selected_frames:
            :param k:
            :param algo:
            :param method:
            :param norm_type:
            :return: a matrix of generalized correlation coefficient

            Developer's note:
            Idea:
            Compute the pairwise generalized correlation coefficient
              only for j>i, since the value = 1 for i=j.
              the lower triangluar part can be maded
              by adding its transpose to itself.
        """

        ## make a trajectory iterator
        trajIter, n_selected_particles = make_trajectory_iter()

        ## store the calculate pairwise geneneralized coorelation coefficients
        _n_upper_triangle_elemennts = int(0.5*(n_selected_particles**2-n_selected_particles))
        gen_corr_coef = numpy.zeros(_n_upper_triangle_elemennts)

        ## initialize the row/column indices for the sparse version of the output matrix
        row_ids = numpy.zeros(_n_upper_triangle_elemennts)
        col_ids = numpy.zeros(_n_upper_triangle_elemennts)

        ccc = 0 # counter
        ## First make a shareable array
        n_dim = 3 # number of dimensions
        # total number of floating point numbers needed to store the trajectory
        n_floats_trajecotry = n_dim*n_selected_particles*n_selected_frames
        # make a shared raw C-array
        shared_trajectory_base = multiprocessing.Array(ctypes.c_double,
                                                       n_floats_trajecotry,
                                                       lock=False)
        all_traj = numpy.zeros((n_selected_frames, n_selected_particles, n_dim))



        for _coord in trajIter:
            all_traj[ccc,:,:] = _coord[:]
            ccc += 1

        def _worker(arg_tuple):
            """
            Role: an individual worker who will calculate the
                generalized correlation coefficient
            """
            ## get trajectories for particle i and j
            i,j,fn_genCorrCoef = arg_tuple[:]
            Xi = all_traj[:,i,:]
            Xj = all_traj[:,j,:]
            ##======================================================================
            ## Now, compute Generalized Correlation Coefficients
            ##======================================================================
            gen_corr_coef = fn_genCorrCoef(Xi,Xj)

            if numpy.isnan(gen_corr_coef):
                print("NaN found at position [{0},{1}]".format(i,j))

            return (i, j, gen_corr_coef)

        fn_get_gcc = self.genCorrCoefObj.get_generalized_correlation_coefficient

        def _gen_corr_coef(Xi,Xj):
            """
            A curried function for getting generalized correlation coefficient
            """
            return fn_get_gcc(Xi,Xj,k,algo,method,norm_type)


        arg_list = []
        ## define processes args
        for _i in range(0, n_selected_particles-1):
            for _j in range(_i+1, n_selected_particles):
                arg_list.append((_i,_j, _gen_corr_coef))

        ##-------------------------------------------
        ## iPython cluster settings
        engines = Client(profile='default')
        cluster_view = engines[:]
        cluster_view.use_dill()
        cluster_view.block = True
        #---------------------------------------------

        ##---------------------
        ## run jobs
        ##---------------------
        results = cluster_view.map(_worker, arg_list)

        ##---------------------
        ## collect results
        ##---------------------
        for _i in range(len(results)):
            row_ids[_i], col_ids[_i], gen_corr_coef[_i] = results[_i][:]


        ## Note: in order to form a square shaped sparse matrix,
        #  we still need one more element: the last entry.
        #  Otherwise, the created sparse matrix will be rectangular
        #   (limitation of scipy sparse matrix)
        # ID of last particle (0-based indexing)
        _id_last = n_selected_particles - 1
        row_ids = numpy.append(row_ids,_id_last)
        col_ids = numpy.append(col_ids,_id_last)

        # for now, just set the value of the last entry to 0
        # later on, it will be replaced by 1, which is
        # the correlation coefficient of the last element with
        # itself.
        _tmp_value = 0
        gen_corr_coef = numpy.append(gen_corr_coef,_tmp_value)

        upperTriSparse = sparse_matrix_maker((gen_corr_coef, (row_ids,col_ids)))

        ## now create a symmetric generalized correlation coefficients
        genCorrCoefSparseMatrix = upperTriSparse + upperTriSparse.transpose() + \
                                  sparse.eye(n_selected_particles)

        return genCorrCoefSparseMatrix.todense()

    def calc_gen_corr_coef_parallel_pp(self,
                                   make_trajectory_iter,
                                   n_selected_frames,
                                   k,
                                   algo,
                                   method,
                                   norm_type):
        """
        .. py:method:: CorrelationMixin._calc_gen_corr_coef_parallel_pp
            Role: (Parallel) calculate the generalized correlation coefficient
                using parallelpython engine
            :param make_trajectory_iter:
            :param n_selected_frames:
            :param k:
            :param algo:
            :param method:
            :param norm_type:
            :return: a matrix of generalized correlation coefficient

            Developer's note:
            Idea:
            Compute the pairwise generalized correlation coefficient
              only for j>i, since the value = 1 for i=j.
              the lower triangluar part can be maded
              by adding its transpose to itself.
        """

        ## make a trajectory iterator
        trajIter, n_selected_particles = make_trajectory_iter()

        ## store the calculate pairwise geneneralized coorelation coefficients
        _n_upper_triangle_elemennts = int(0.5*(n_selected_particles**2-n_selected_particles))
        gen_corr_coef = numpy.zeros(_n_upper_triangle_elemennts)

        ## initialize the row/column indices for the sparse version of the output matrix
        row_ids = numpy.zeros(_n_upper_triangle_elemennts)
        col_ids = numpy.zeros(_n_upper_triangle_elemennts)

        ccc = 0 # counter
        ## First make a shareable array
        n_dim = 3 # number of dimensions
        # total number of floating point numbers needed to store the trajectory
        n_floats_trajecotry = n_dim*n_selected_particles*n_selected_frames
        # make a shared raw C-array
        shared_trajectory_base = multiprocessing.Array(ctypes.c_double,
                                                       n_floats_trajecotry,
                                                       lock=False)
        all_traj = numpy.zeros((n_selected_frames, n_selected_particles, n_dim))


        t0 = time.time()
        for _coord in trajIter:
            all_traj[ccc,:,:] = _coord[:]
            ccc += 1
        t1 = time.time()
        print("Done with preparing trajectory. time cost = {0} s".format(t1-t0))

        def _worker(arg_tuple):
            """
            Role: an individual worker who will calculate the
                generalized correlation coefficient
            """
            ## get trajectories for particle i and j
            i,j,fn_genCorrCoef,all_traj = arg_tuple[:]
            Xi = all_traj[:,i,:]
            Xj = all_traj[:,j,:]
            ##======================================================================
            ## Now, compute Generalized Correlation Coefficients
            ##======================================================================
            gen_corr_coef = fn_genCorrCoef(Xi,Xj)

            if numpy.isnan(gen_corr_coef):
                print("NaN found at position [{0},{1}]".format(i,j))

            return (i, j, gen_corr_coef)

        def _fn_get_gen_corr_coef(Xi,Xj):
            """
            A curried function for getting generalized correlation coefficient
            """
            return self.genCorrCoefObj.get_generalized_correlation_coefficient(Xi,Xj,k,algo,method,norm_type)


        arg_list = []
        ## define processes args
        for _i in range(0, n_selected_particles-1):
            for _j in range(_i+1, n_selected_particles):
                arg_list.append((_i,_j, _fn_get_gen_corr_coef, all_traj))

        ##-------------------------------------------
        ## Parallel python setup
        pp_server = pp.Server()
        print("ParallelPython: using {0} CPU workers".format(pp_server.get_ncpus()))
        pp_func_dependencies = (_fn_get_gen_corr_coef,self.genCorrCoefObj.get_generalized_correlation_coefficient,)
        pp_module_dependencies = ('numpy',)
        pp_workers = []
        t0 = time.time()
        print("Starting parallel python jobs ")
        for _arg in arg_list:
            pp_arg_tuple = (_arg,)
            pp_workers.append(pp_server.submit(_worker, pp_arg_tuple, pp_func_dependencies, pp_module_dependencies))

        #---------------------------------------------

        ##---------------------
        ## run jobs
        ##---------------------

        results = []
        for _worker in pp_workers:
            results.append(_worker())
        t1 = time.time()
        print("Done with parallel python workers; time cost: {0} s".format(t1-t0))

        ##---------------------
        ## collect results
        ##---------------------
        for _i in range(len(results)):
            row_ids[_i], col_ids[_i], gen_corr_coef[_i] = results[_i][:]


        ## Note: in order to form a square shaped sparse matrix,
        #  we still need one more element: the last entry.
        #  Otherwise, the created sparse matrix will be rectangular
        #   (limitation of scipy sparse matrix)
        # ID of last particle (0-based indexing)
        _id_last = n_selected_particles - 1
        row_ids = numpy.append(row_ids,_id_last)
        col_ids = numpy.append(col_ids,_id_last)

        # for now, just set the value of the last entry to 0
        # later on, it will be replaced by 1, which is
        # the correlation coefficient of the last element with
        # itself.
        _tmp_value = 0
        gen_corr_coef = numpy.append(gen_corr_coef,_tmp_value)

        upperTriSparse = sparse_matrix_maker((gen_corr_coef, (row_ids,col_ids)))

        ## now create a symmetric generalized correlation coefficients
        genCorrCoefSparseMatrix = upperTriSparse + upperTriSparse.transpose() + \
                                  sparse.eye(n_selected_particles)

        return genCorrCoefSparseMatrix.todense()

    def __get_one_particle_trajectory(self, id, coordIterObj, n_frames):
        """
        Role: the coordinates for record with <id> for the whole
            trajectory.
        :param id: id of the particle whose X, Y, Z coordinate
            will be extracted.
        :param coordIterObj: an iterator object that yields
            a coordinate matrix of shape (N,3) where N is the
            number of particles at each loop.
        :param n_frames: total frames provided by the coordIterObj
        :return: a matrix of shape (n_frames, 3), i.e. coordinates
            of selected particle for all frames
        """
        output = numpy.zeros((n_frames, 3))
        ccc = 0 # counter
        for _all_coords in coordIterObj:
            output[ccc,:] = _all_coords[id,:]
            ccc += 1
        return output

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Get a list of public methods

            :return: list
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list