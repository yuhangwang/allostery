"""
.. py:module:: generalized_correlation_coefficient
    Role: calculate the generalized correlation coefficient
"""

#============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '12-09-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import numpy

#------------------------------
# my modules
#------------------------------
from allostery.app.mutualinfoApp.mutual_information import MutualInfoMixin

#------------------------------

class GenCorrCoefMixin(object):
    """
    .. py:class:: GenCorrCoef
        Role: compute the generalized correlation coefficient
    """
    def __init__(self, rand_seed=None):
        self.mutualInfoObj = MutualInfoMixin(rand_seed)

    def get_generalized_correlation_coefficient(self,X,Y,
                                                k=1,
                                                algo="KSG2",
                                                method="kdtree",
                                                norm_type='max'):
        """
        .. py:method:: GenCorrCoefMixin.get_generalied_correlation_coefficient()
            Role: calculate the generalized correlation coefficient
            :param X: an array of shape (m,n) where "n" is the dimension
                and "m" is the number of sample points
            :param Y: cf. X
            :param k: which nearest neighbor (default: 1)
            :param algo: algorithm for estimating mutual information (MI)
                options:
                'KSG1': Kraskov-Strogbauer-Grassberger type-I MI estimator
                'KSG2': Kraskov-Strogbauer-Grassberger type-II MI estimator
                default: 'KSG2'
                comment: 'KSG1' is more precise, but 'KSG2' is more accurate
            :param method: method for nearest neighbor calculation which is needed
                by KSG1 and KSG2 algorithms.
                options:
                'simple': will use pairwise distances calculations (O(N^2) algorithms)
                'kdtree': use kDTree algorithm (most likely will be more efficient)
                default: 'simple'
            :param norm_type: type of the vector norm (default: 'max')
                note: same options as in numpy.lingalg.norm()
                options:
                    max: infinity-norm
            :return: scalar (genearalized correlation coefficient)
            :raise: error if X and Y do not have the same array shape
        """
        ## X and Y must have the same shape
        if numpy.shape(X) != numpy.shape(Y):
            msg = "ERROR HINT: input X and Y must have the same shape"
            raise UserWarning(msg)

        ## get dimension of the space
        _, dimension_of_space = numpy.shape(X)

        ## compute mutual information
        _mutual_info = self.mutualInfoObj.get_mutual_information(X,Y,
                                                                 k=k,
                                                                 algo=algo,
                                                                 method=method,
                                                                 norm_type=norm_type)
        if _mutual_info < 0:
            msg = "ERROR HINT: Negative mutual information found: {0}".format(_mutual_info)
            print(msg)
            _mutual_info = 0

        ## computer generalized correlation coefficient
        gen_corr_ceof = numpy.sqrt(1.0-numpy.exp(-2.0/dimension_of_space*_mutual_info))

        return gen_corr_ceof
