# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: distance mixin

    Role: provide tools for calculating distances
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import re
#------------------------------
# `allostery` package modules
#------------------------------
from allostery.mathcalc.matrixlib import MatrixCalc


#========================================================================
#                   [[[ DistanceMixin ]]]
#========================================================================
class DistanceMixin(object):
    """
    .. py:class:: CalcMixin(proxyObj)

        Role: calculate higher level quantities like pairwise distances

        :param proxyObj: some instance object with methods:
                ".get_selected_coords()"
                ".get_selected_com_coords()"
    """
    def __init__(self, **kwargs):
        self.universeObj = kwargs['universe']

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: DistanceMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: list of class objects
        """
        return None

    def get_pairwise_distances(self):
        """
        .. py:method:: CalcMixin.get_pairwise_distances()

            Role: get the pairwise distances of selected atoms in current frame

            :return: pairwise distances
            :rtype: numpy.array with dtype=numpy.float32
        """
        return MatrixCalc.calc_pairwise_distance_matrix(self.universeObj.get_selected_coords())

    def get_pairwise_com_distances(self):
        """
        .. py:method:: CalcMixin.get_pairwise_com_distances()

            Role: get the pairwise center of mass distances of selected atoms in current frame

            :return: pairwise center of mass distances
            :rtype: numpy.array with dtype=numpy.float32
        """
        return MatrixCalc.calc_pairwise_distance_matrix(self.universeObj.get_selected_com_coords())

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Return a list of public methods for class

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()