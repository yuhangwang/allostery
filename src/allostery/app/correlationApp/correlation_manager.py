# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: app.correlation_coefficient

    Role: calculate the correlation coefficient
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-27-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import numpy
import re

#------------------------------
# `allostery` package modules
#------------------------------
from allostery.app.gencorrcoefApp.gencorrcoef_mananger import GenCorrCoefApp

#========================================================================
#                   [[[ CorrelationCoefficientMixin ]]]
#========================================================================
class CorrelationMixin(object):
    """
    .. py:class:: CorrelationMixin()

        Role: calculate the correlation coefficient | cross-correlation
                of a trajectory of coordinates

    """

    def __init__(self,**kwargs):
        super(CorrelationMixin, self).__init__()
        self.universeObj = kwargs['universe']
        self.iteratorObj = kwargs['IteratorMixin']
        self.genCorrCoefApp = None # placeholder for now

    def __prepare_iter(self,frame_start, frame_end, frame_skip, mode):
        """
        Role: prepare the iterator object
        :return: [iterObj,numberOfParticles]
        """
        if frame_end == None:
            frame_end = self.universeObj.get_total_number_of_frames()

        if mode == "com":
            number_of_selected_particles= self.universeObj.get_number_of_selected_residues()
            _iterObj = self.iteratorObj.iter_get_selected_com_coords(frame_start=frame_start,
                                                                     frame_end=frame_end,
                                                                     frame_skip=frame_skip)
        elif mode == "allatom":
            number_of_selected_particles= self.universeObj.get_number_of_selected_atoms()
            _iterObj = self.iteratorObj.iter_get_selected_coords(frame_start=frame_start,
                                                                 frame_end=frame_end,
                                                                 frame_skip=frame_skip)
        else:
            msg = "Error Hint: please select a mode"
            raise UserWarning(msg)

        return [_iterObj,number_of_selected_particles]

    def __get_one_particle_trajectory(self, id, coordIterObj, n_frames):
        """
        Role: the coordinates for record with <id> for the whole
            trajectory.
        :param id: id of the particle whose X, Y, Z coordinate
            will be extracted.
        :param coordIterObj: an iterator object that yields
            a coordinate matrix of shape (N,3) where N is the
            number of particles at each loop.
        :param n_frames: total frames provided by the coordIterObj
        :return: a matrix of shape (n_frames, 3), i.e. coordinates
            of selected particle for all frames
        """
        output = numpy.zeros((n_frames, 3))
        ccc = 0 # counter
        for _all_coords in coordIterObj:
            output[ccc,:] = _all_coords[id,:]
            ccc += 1
        return output


    def get_avg_position(self,reCalc=True, frame_start=0, frame_end=None, frame_skip=1,
                                    mode="com",dim=3):
        """
        .. py:method:: CorrelationMixin.get_avg_position()

            Role: get the average position for each selected residue/atom

            Developer note: this will be used for cross correlation calculation

            :param bool reCalc: if "True" (default), then re-calculate commute time matrix
            :param int frame_start: starting frame (0-based); default: 0
            :param int frame_end: ending frame (0-based); default: last frame
            :param int frame_skip: frame window size; default: 1 (every frame)
            :param str mode: options:
                    "com": use center of mass of selected residues
                    "allatom": use the raw coordinates of every selected atoms
            :param int dim: dimension of the coordination system
            :return: an array of average positions of shape (N,dim)
                    where **N** is the number of selected residues/atoms
                    and **dim** is the number of dimensions of the coordinate system
        """
        if reCalc is True: # re-calculate commute time
            _iterObj, total_number_particles = self.__prepare_iter(frame_start,
                                                                   frame_end,
                                                                   frame_skip,
                                                                   mode)
            _dim = dim # dimension of the coordinate system
            self.avg_coords = numpy.zeros((total_number_particles, _dim))
            n_frames = 0
            for _coords in _iterObj:
                self.avg_coords += _coords
                n_frames += 1
            self.avg_coords /= n_frames
        else:
            self.__check_attr_exists("avg_coords")

        return self.avg_coords


    def get_cross_correlation(self,reCalc=True, frame_start=0, frame_end=None, frame_skip=1,
                                    mode="com",dim=3):
        """
        .. py:method:: CorrelationMixin.get_cross_correlation(mode)
            Role: get the cross-correlation for the selected atoms/residues
            :param bool reCalc: if "True" (default), then re-calculate commute time matrix
            :param int frame_start: starting frame (0-based); default: 0
            :param int frame_end: ending frame (0-based); default: last frame
            :param int frame_skip: frame window size; default: 1 (every frame)
            :param str mode: options:
                    "com": use center of mass of selected residues
                    "allatom": use the raw coordinates of every selected atoms
            :param int dim: dimension of the coordination system
            :return: cross-correlation matrix
        """
        if reCalc is True: # re-calculate commute time
            _iterObj, total_number_particles = self.__prepare_iter(frame_start,
                                                                   frame_end,
                                                                   frame_skip,
                                                                   mode)
            self.cross_correlation = numpy.zeros((total_number_particles, total_number_particles))
            _avg_coords = self.get_avg_position(reCalc,frame_start,frame_end,frame_skip,mode,dim)
            n_frames = 0
            for _coords in _iterObj:
                for _d in range(dim):
                    _coords[:,_d] -= _avg_coords[:,_d] # remove global motion
                    self.cross_correlation += numpy.outer(_coords[:,_d],_coords[:,_d])
                n_frames += 1
            self.cross_correlation /= n_frames-1 # statistically N-1 is usually used for normalization
        else:
            self.__check_attr_exists("cross_correlation")

        return self.cross_correlation


    def get_correlation_coefficient(self,reCalc=True, frame_start=0, frame_end=None, frame_skip=1,
                                    mode="com", dim=3):
        """
        .. py:method:: CorrelationCoefficientMixin.get_correlation_coefficient(mode)
            Role: get the correlation coefficient of selected atoms/residues
            :param bool reCalc: if "True" (default), then re-calculate commute time matrix
            :param int frame_start: starting frame (0-based); default: 0
            :param int frame_end: ending frame (0-based); default: last frame
            :param int frame_skip: frame window size; default: 1 (every frame)
            :param str mode: options:
                    "com": use center of mass of selected residues
                    "allatom": use the raw coordinates of every selected atoms
            :param int dim: dimension of the coordination system
            :return: correlation coefficient matrix
        """
        if reCalc is True: # re-calculate commute time
             _iterObj, total_number_particles = self.__prepare_iter(frame_start,
                                                                   frame_end,
                                                                   frame_skip,
                                                                   mode)
             _cross_correlation = self.get_cross_correlation(reCalc,frame_start,frame_end,frame_skip,mode,dim)
             _std = numpy.sqrt(numpy.diag(_cross_correlation)) # standard deviations
             _prod_std = numpy.outer(_std, _std) # outer product of standard deviations
             self.correlation_coefficient = _cross_correlation/_prod_std
        else:
            self.__check_attr_exists("correlation_coefficient")

        return self.correlation_coefficient

    def get_generalized_correlation_coefficient(self,
                                                frame_start=0,
                                                frame_end=None,
                                                frame_skip=1,
                                                mode="allatom",
                                                k=1,
                                                algo="KSG2",
                                                method="kdtree",
                                                norm_type='max',
                                                reCalc=True,
                                                parallel=False,
                                                rand_seed=None):
        """
        .. py:method:: CorrelationMixin.get_generalized_correlation_coefficient()

            Role: get the generalized correlation coefficient

            Reference: Lange and Grubmuller Proteins 62:1053 (2006)

            :param bool reCalc: if "True" (default), then re-calculate commute time matrix
            :param int frame_start: starting frame (0-based); default: 0
            :param int frame_end: ending frame (0-based); default: last frame
            :param int frame_skip: frame window size; default: 1 (every frame)
            :param str mode: options:
                    "com": use center of mass of selected residues
                    "allatom": use the raw coordinates of every selected atoms
                    default: "allatom"
            :param k: which nearest neighbor for mutual information estimation (default: 1)
            :param algo: algorithm for estimating mutual information (MI)
                options:
                'KSG1': Kraskov-Strogbauer-Grassberger type-I MI estimator
                'KSG2': Kraskov-Strogbauer-Grassberger type-II MI estimator
                default: 'KSG2'
                comment: 'KSG1' is more precise, but 'KSG2' is more accurate
            :param method: method for nearest neighbor calculation which is needed
                by KSG1 and KSG2 algorithms.
                options:
                'simple': will use pairwise distances calculations (O(N^2) algorithms)
                'kdtree': use kDTree algorithm (most likely will be more efficient)
                default: 'simple'
            :param int dimension: dimension of the coordination system
            :param norm_type: type of the vector norm (default: 'max')
                note: same options as in numpy.lingalg.norm()
                options:
                    max: infinity-norm
            :param reCalc: if True then recalcuate; otherwise, return the result
                from most recent calculation.
            :param parallel: run in parallel? (default: False)
            :param rand_seed: randome number generation seed for KSG1/KSG2 algorithms,
                which requires adding low random noise to input data to break data
                degeneracy (see Kraskov et. al. Phys. Rev. E 69:066138 (2004))
                default: None, which means pick random seed from sources such as current time.
            :return: a matrix of shape (N,N) where N is the number of selected particles
                or number of selected residues if "com" mode is chosen.
        """
        self.genCorrCoefApp = GenCorrCoefApp(rand_seed)

        ## total number of selected frames
        if frame_end == None:
            frame_end = self.universeObj.get_total_number_of_frames()
        n_selected_frames = (frame_end - frame_start - 1)//frame_skip + 1

        def _make_trajectory_iter(frame_start=frame_start, frame_end=frame_end,
                                  frame_skip=frame_skip,mode=mode):
            """
            Role: local function for making a specialized trajectory iterator
            """
            _iterObj, n_selected_particles = self.__prepare_iter(frame_start,
                                                                       frame_end,
                                                                       frame_skip,
                                                                       mode)
            return [_iterObj, n_selected_particles]

        if n_selected_frames <= 0:
            msg = "ERROR HINT: the frame range you specified does not yield \"{0}\" frames. ".format(n_selected_frames)
            msg += "Please specified another frame range."
            raise UserWarning(msg)

        if reCalc is True:
            if parallel:
                genCorrCoeff = self.genCorrCoefApp.calc_gen_corr_coef_parallel_ipython(_make_trajectory_iter,
                                                                 n_selected_frames,
                                                                 k,
                                                                 algo,
                                                                 method,
                                                                 norm_type
                                                                 )
            else:
                genCorrCoeff = self.genCorrCoefApp.calc_gen_corr_coef_serial(_make_trajectory_iter,
                                                                 n_selected_frames,
                                                                 k,
                                                                 algo,
                                                                 method,
                                                                 norm_type)

            self.generalized_correlation_coefficients = genCorrCoeff
        else:
            self.__check_attr_exists("generalized_correlation_coefficients")

        return self.generalized_correlation_coefficients


    def __check_attr_exists(self,attr_name):
        """
        Role: check whether an attribute exists. If not, raise a warning.
        """
        if not hasattr(self, attr_name):
            msg = "Error Hint: you need to compute '{0}' first.\n".format(attr_name)
            raise UserWarning(msg)
        else:
            return True

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: CommuteTimeMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: set of class objects
        """
        return {'IteratorMixin'}

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Get a list of public methods

            :return: list
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list


