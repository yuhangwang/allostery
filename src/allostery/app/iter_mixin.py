# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: iter_mixin

    Role: provide iterators for coordinates and distances
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import re
#------------------------------
# my modules
#------------------------------
from allostery.mathcalc.matrixlib import MatrixCalc

#========================================================================
#                   [[[ IterMixin ]]]
#========================================================================
class IteratorMixin(object):
    """
    note: this probably should be in allo_manager.py
    Role: provide host class with the ability to
        make generator functions for the whole trajectory
    """

    def __init__(self, **kwargs):
        self.universeObj = kwargs['universe']

    def iter_get_selected_coords(self, frame_start=0, frame_end=None, frame_skip=1, *args, **kwargs):
        """
        Role: a generator function that returns the coordinates
              of selected atoms for all trajectory frames
        :param frame_start: starting frame (0-based index)
               frame_end: last frame
               frame_skip: interval between frames
        :return: numpy array
        """
        if frame_end == None:
            frame_end = self.universeObj.get_total_number_of_frames()

        for frameId in xrange(frame_start, frame_end, frame_skip):
            self.universeObj.goto_frame(frameId)
            yield self.universeObj.get_selected_coords()

    def iter_get_selected_com_coords(self, frame_start=0, frame_end=None, frame_skip=1, *args, **kwargs):
        """
        Role: a generator function that returns the center of mass coordinates
              of selected atoms
        :param  frame_start: starting frame (0-based) (default: 0)
                frame_end: ending frame (0-based) (default: last frame of the trajectory)
                frame_skip: re-sample trajectory every [frame_skip] frames
        :return: numpy array (shape: Nx3, N is the number of selected residues)
        """
        if frame_end == None:
            frame_end = self.universeObj.get_total_number_of_frames()

        for frameId in range(frame_start, frame_end, frame_skip):
            self.universeObj.goto_frame(frameId)  # update frame
            yield self.universeObj.get_selected_com_coords()

    def iter_get_pairwise_distances(self, frame_start=0, frame_end=None, frame_skip=1, *args, **kwargs):
        """
        Role: a generator function that returns the pairwise distances between the
              selected atoms
        :param  frame_start: starting frame (0-based) (default: 0)
                frame_end: ending frame (0-based) (default: last frame of the trajectory)
                frame_skip: re-sample trajectory every [frame_skip] frames
        :return: numpy array (shape: Nx3, N is the number of selected residues)
        """
        if frame_end == None:
            frame_end = self.universeObj.get_total_number_of_frames()

        for frameId in range(frame_start, frame_end, frame_skip):
            self.universeObj.goto_frame(frameId)  # update frame
            yield MatrixCalc.calc_pairwise_distance_matrix(self.universeObj.get_selected_coords())

    def iter_get_pairwise_com_distances(self, frame_start=0, frame_end=None, frame_skip=1, *args, **kwargs):
        """
        Role: a generator function that returns the pairwise center of mass distances between the
              selected atoms
        :param  frame_start: starting frame (0-based) (default: 0)
                frame_end: ending frame (0-based) (default: last frame of the trajectory)
                frame_skip: re-sample trajectory every [frame_skip] frames
        :return: numpy array (shape: Nx3, N is the number of selected residues)
        """
        if frame_end == None:
            frame_end = self.universeObj.get_total_number_of_frames()

        for frameId in range(frame_start, frame_end, frame_skip):
            self.universeObj.goto_frame(frameId)  # update frame
            com_coords = self.universeObj.get_selected_com_coords()
            yield MatrixCalc.calc_pairwise_distance_matrix(com_coords)

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: IteratorMixin.get_dependency()

            Role: return a list of mix-in's that it depends on
                If no dependencies, return None

            :return: list of class objects
        """
        return None

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Return a list of public methods for class

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()


