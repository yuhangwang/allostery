# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: app.mutual_information
    Role: calculate the mutual information
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '11-30-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import numpy
import re
from scipy.spatial import distance as distance_kit
import scipy.special as special_functions
import scipy.linalg as linear_algebra
import numpy.random as RNG

## aliases
digamma_function = special_functions.psi
pdist_function = distance_kit.pdist
norm_function = linear_algebra.norm
square_matrix_form = distance_kit.squareform
matrix_sort = numpy.sort # sort matrix along rows/columns


#------------------------------
# `allostery` package modules
#------------------------------
from allostery.app._dependency import PluginsForMutualInfoMixin
from allostery.algorithms.nearest_neighbors import NearestNeighborMixin
from allostery.pkgtools.pkg_manager import DependencyResolver

#========================================================================
#                   [[[ CorrelationCoefficientMixin ]]]
#========================================================================
class MutualInfoMixin(object):
    """
    .. py:class:: NearestNeighborMixin
        Goal: compute nearest-neighbor related quantities,
            including the number of data points within a cutoff
    """
    _list_addon_attr = []

    def __init__(self, rand_seed=None):
        super(MutualInfoMixin, self).__init__()
        self.delegate_dict = dict()
        self.attr_delegate_dict = dict()

        self._add_delegates(PluginsForMutualInfoMixin.mixin_list)
        self.kNNObj = NearestNeighborMixin()
        self.accepted_kNN_methods = ['simple', 'kdtree']
        self.accepted_norm_types = ['max']
        self.random_noise_level = 1E-10
        if rand_seed is not None:
            numpy.random.seed(rand_seed)

    def get_mutual_information(self, X, Y, k=1, algo="KSG1", method='simple', norm_type='max'):
        """
        .. py:method:: NearestNeighborMixin.count_within_cutoff()
            Role: count the number of data points within
                cutoff from the center point
            :param X: a matrix of of shape (m,n) for random variable X
                    where m is the total number of samples, and
                    n is dimension of X
            :param Y: cf. X
            :param k: which nearest neighbor (default: 1)
            :param algo: algorithm for estimating mutual information (MI)
                options:
                'KSG1': Kraskov-Strogbauer-Grassberger type-I MI estimator
                'KSG2': Kraskov-Strogbauer-Grassberger type-II MI estimator
                default: 'KSG2'
                comment: 'KSG1' is more precise, but 'KSG2' is more accurate
            :param method: method for nearest neighbor calculation which is needed
                by KSG1 and KSG2 algorithms.
                options:
                'simple': will use pairwise distances calculations (O(N^2) algorithms)
                'kdtree': use kDTree algorithm (most likely will be more efficient)
                default: 'simple'
            :param norm_type: type of the vector norm (default: 'max')
                options:
                max: infinity-norm
            :return: a scalar (mutual information between X and Y)
        """

        ## add some low level random noise to avoid degeneracy
        #  when searching for k-th neighbors
        X = self.add_rand_noise(X, self.random_noise_level)
        Y = self.add_rand_noise(Y, self.random_noise_level)

        if algo == "KSG1":
            mutual_info = self._KSG_mutual_info_type1(X, Y,k, method,norm_type)
        elif algo == 'KSG2':
            mutual_info = self._KSG_mutual_info_type2(X, Y, k, method,norm_type)
        else:
            mutual_info = None

        return mutual_info

    def _KSG_mutual_info_type1(self, X, Y, k, method="simple",norm_type='max'):
        """
        Role: calculate the mutual information
            using type-1 Kraskov-Strogbauer-Grassberger algorithm
        Note: X and Y must have the same number of rows
        :param X: samples of random variable X
                It has shape [m,n], where
                m is the number of samples;
                n is the dimension of each sample point
        :param Y: (cf. X)
        :param k: which nearest neighbor, e.g. k=1 means
                first nearest neighbor.
        :param norm_type: type of the norm for calculating lengths of vectors
                default: 'max'
        :return: scalar (mutual information)
        :raise: error when method name is unknown
        """
        ## lets think of X for residue-1 and Y for residue-2
        N = len(X)
        if N != len(Y):
            msg = "ERROR HINT: length of input X should equal to that of Y"
            raise UserWarning(msg)

        ## combine X, Y into one random variable
        Z = numpy.hstack((X,Y))

        ## step 1. find the kNN distances using each data point
        #   as center
        if method == "simple":
            kNN_distances = self.kNNObj.simple_find_kNN_distance(Z,k)
        elif method == "kdtree":
            kNN_distances = self.kNNObj.kdtree_find_kNN_distance(Z,k,norm_type)
        else:
            msg = self._error_msg_unknown_kNN_method(method)
            raise UserWarning(msg)


        ## step 2. use the kNN_distances as the cutoff
        #  and count the number of data points along X and Y
        if method == "simple":
            Nx_array = self.kNNObj.batch_count_number_of_nearest_neighbors(X, cutoffs=kNN_distances)
            Ny_array = self.kNNObj.batch_count_number_of_nearest_neighbors(Y, cutoffs=kNN_distances)
        elif method == 'kdtree':
            Nx_array = self.kNNObj._kdtree_count_nearest_neighbors(X,cutoffs=kNN_distances,norm_type=norm_type)
            Ny_array = self.kNNObj._kdtree_count_nearest_neighbors(Y,cutoffs=kNN_distances,norm_type=norm_type)
        else:
            msg = self._error_msg_unknown_kNN_method(method)
            raise UserWarning(msg)

        # print("mean of Nx = {0}".format(numpy.mean(Nx_array)))
        # print("mean of Ny = {0}".format(numpy.mean(Ny_array)))

        ## compute mutual information
        mutual_info = digamma_function(k) + digamma_function(N) - \
                      numpy.mean(digamma_function(Nx_array+1)) - \
                      numpy.mean(digamma_function(Ny_array+1))

        return mutual_info

    def _KSG_mutual_info_type2(self, X, Y, k, method="simple", norm_type='max'):
        """
        Role: calculate the mutual information
            using type-2 Kraskov-Strogbauer-Grassberger algorithm
        Note: X and Y must have the same number of rows
        :param X: samples of random variable X
                It has shape [m,n], where
                m is the number of samples;
                n is the dimension of each sample point
        :param Y: (cf. X)
        :param k: which nearest neighbor, e.g. k=1 means
                first nearest neighbor.
        :param norm_type: type of the norm for calculating lengths of vectors
                default: 'max'
        :return: scalar (mutual information)
        :raise: error when method name is unknown
        """
        if len(X) != len(Y):
            msg = "ERROR HINT: length of input X should equal to that of Y"
            raise UserWarning(msg)


        ## combine X, Y into one random variable
        Z = numpy.hstack((X,Y))
        N_data_points = numpy.shape(Z)[0]

        ## dimension of the X (or Y)
        subdim = numpy.shape(X)[1]
        n_subdims = 2 # number of sub-dimensions for Z = 2 (i.e. X and Y)

        ## step 1. find the kNN distances using each data point
        #   as center
        if method == "simple":
            kNN_distances_multi = self.kNNObj.simple_find_kNN_distance_subdim(Z,k,
                                                                              subdim=subdim,
                                                                              norm_type=norm_type)
        elif method == 'kdtree':
            kNN_distances_multi = self.kNNObj.kdtree_find_kNN_distance(Z,k,
                                                                       norm_type=norm_type,
                                                                       subdim=subdim)
        else:
            msg = self._error_msg_unknown_kNN_method(method)
            raise UserWarning(msg)

        kNN_dist_X = kNN_distances_multi[:,0].reshape((-1,1))
        kNN_dist_Y = kNN_distances_multi[:,1].reshape((-1,1))

        # print("kNN_dist_X\n",kNN_dist_X)
        # print("kNN_dist_Y\n",kNN_dist_Y)

        ## step 2. use the kNN_distances as the cutoff
        #  and count the number of data points along X and Y
        Nx_array = self.kNNObj.batch_count_number_of_nearest_neighbors(X, cutoffs=kNN_dist_X)
        Ny_array = self.kNNObj.batch_count_number_of_nearest_neighbors(Y, cutoffs=kNN_dist_Y)

        # print("X=\n",X)
        # print("Y=\n",Y)
        # print("Nx_array\n",Nx_array)
        # print("Ny_array\n",Ny_array)

        # print("mean of Nx = {0}".format(numpy.mean(Nx_array)))
        # print("mean of Ny = {0}".format(numpy.mean(Ny_array)))

        ## step 3. compute mutual information.
        mutual_info = digamma_function(k) + digamma_function(N_data_points) - \
                      numpy.mean(digamma_function(Nx_array+1)) - \
                      numpy.mean(digamma_function(Ny_array+1)) - 1/k

        return mutual_info

    def _error_msg_unknown_kNN_method(self, unknown_method_string):
        """
        Role: raise an unknown kNN method
        :return: error message
        """
        msg = "ERROR HINT: you have specified an unknown kNN method: \"{0}\" ".format(unknown_method_string)
        msg += "The accepcted kNN methods are:"
        for method in self.accepted_kNN_methods:
            msg += "{0}\n".format(method)
        return msg

    def add_rand_noise(self, X, scale=1E-10):
        """
        Role: add some low-level random noise to the input data
            in order to break degeneracy.
        :param X: a mxn matrix where n is the dimension and
            m is the number of data points
        :param scale: scaling factor to the random noise (default: 1E-10)
        :return: X+random_noise
        """
        return X + scale*RNG.rand(*numpy.shape(X))

    def _error_msg_unknown_norm_type(self, unknown_norm_type):
        """
        Role: return an msg about the unknown norm type
        :return: msg (a string)
        """
        msg = "ERROR HINT: you have specified an unknown norm type: \"{0}\"".format(unknown_norm_type)
        msg += "\t Please choose one of the following norm types:\n"
        for _norm_type in self.accepted_norm_types:
            msg += "{0}\n".format(msg)
        return msg

    def _add_delegates(self, list_of_mixins):
        """
        Define axis property refinement delegates
        """
        for _class in DependencyResolver(list_of_mixins).get_sorted():
            _class_name = _class.__name__
            self.delegate_dict[_class_name] = _class(**self.delegate_dict)
            self.attr_delegate_dict[_class_name] = dir(_class)

        for _delegate in self.attr_delegate_dict:
            MutualInfoMixin._list_addon_attr += self.attr_delegate_dict[_delegate]

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Get a list of public methods

            :return: list
        """
        raw_list = dir(cls)
        raw_list += cls._list_addon_attr
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list