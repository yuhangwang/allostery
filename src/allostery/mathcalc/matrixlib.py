#============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-03-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
# import MDAnalysis.core.distances as distance_module
import MDAnalysis.lib.parallel.distances as distance_module
import numpy

# import itertools
# import numba
#------------------------------
# my modules
#------------------------------

#------------------------------


#============================================================================
#               [[[ Toolkit: atomselectionlib for data analysis ]]]
#============================================================================

class MatrixCalc(object):
    """
    .. py:class:: MatrixCalc()

        Role: provide static methods for matrix calculations
    """

    @staticmethod
    def calc_pairwise_distance_matrix(input_coordinates):
      """
      build pair-wise distance matrix for center of mass coordinates
      """
      #----------------------------------------------------------
      # CALL MDAnalysis.core.distances.distance_array
      # instead of the MDAnalysis.core.parallel.distances
      # (the latter requires inputs to be of Cython DTYPE_t type,
      #  which is not easy to satisfy, but fast!)
      #----------------------------------------------------------
      return distance_module.distance_array(input_coordinates, input_coordinates)

    @staticmethod
    # @numba.jit("f4[:,:](f4[:,:], f4[:,:])")
    def calc_matrix_average(iter_matrices,  avg_init):
        """
        Role: compute the average of a series of matrices/vectors
        :param iter_matrices: a generator for a series of numpy array/matrix
             n_rows: number of rows of the input matrix
             n_cols: number of columns of the input matrix
        :return: 2D numpy array
        """
        ccc = numpy.array([0]) # counter
        for matrix in iter_matrices:
            avg_init += matrix
            ccc[0] += 1
            # change data type of ccc
            ccc.astype(float)
            avg_init /= ccc[0]
        return avg_init

    @staticmethod
    # @numba.jit("f4[:,:](f4[:,:], i4)")
    def calc_matrix_variance(matrix_iterator, n_rows, ddof=0):
        """
        Role: compute the commute time matrix
            (variance of inter-residue distances)
        :param matrix_iterator: a python generator which returns a matrix
        :param int n_rows: number of rows for each matrix that com_coords_iterator generates
        :param int ddof: delta degree of freedom. The averaging is done by dividing by #frames - ddof
        :return: numpy matrix
        note: for numerical stability reason, it is better
            to use the formula < (d - avg(d))^2>
            rather than <d^2> -<d>^2
        """
        #-----------------------------------------------------
        # Make two copies of the original generator
        # because two loops are needed
        # itertools.tee() Return n independent iterators from a single iterable
        #-----------------------------------------------------
        # Cautions!!!
        #-----------------------------------------------------
        # Once itertools.tee() has made a split, the original iterable
        # should not be used anywhere else; otherwise,
        # the iterable could get advanced without
        # the tee objects being informed.
        # ref: https://docs.python.org/2/library/itertools.html#itertools.tee
        #-----------------------------------------------------
        # iter_copy1, iter_copy2 = itertools.tee(input_matrix_iterator)
        # shape = (number_of_rows, number_of_rows)
        # commute_time_matrix = numpy.zeros(shape, dtype=numpy.float32)
        #
        # avg = MatrixCalc.calc_matrix_average(iter_copy1, numpy.zeros(shape, dtype=numpy.float32))
        # numberFrames = 0
        # for _matrix in iter_copy2:
        #     numberFrames += 1
        #     commute_time_matrix += numpy.power((_matrix - avg), 2)
        #     commute_time_matrix /= numberFrames
        # return commute_time_matrix

        shape = (n_rows, n_rows)
        # commute_time_matrix = numpy.zeros(shape, dtype=numpy.float32)
        matrix_sqr_avg = numpy.zeros(shape, dtype=numpy.float32)
        matrix_avg = numpy.zeros(shape, dtype=numpy.float32)
        n_frames = 0 # counter
        for _matrix in matrix_iterator:
            matrix_avg += _matrix
            matrix_sqr_avg += numpy.power(_matrix, 2)
            n_frames += 1

        if n_frames > 0: # if there are more than one frame
            normalizer = float(n_frames - ddof)
            print("normalizer=",normalizer)
            return (matrix_sqr_avg/normalizer) - numpy.power(matrix_avg/normalizer, 2)
        else:
            msg = "Error Hint: not data in the input matrix iterator"
            raise UserWarning(msg)





