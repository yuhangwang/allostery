# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
A list of mix-in's to be added to manager.UserDataManger()
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-28-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

from allostery.app.iter_mixin import IteratorMixin
from allostery.app.distance_mixin import DistanceMixin
from allostery.app.commute_time_mixin import CommuteTimeMixin
from allostery.app.correlationApp.correlation_manager import CorrelationMixin
from allostery.plotting.plot_matrix_manager import PlotMatrix
from allostery.iolib.dataio import DataIO

class MixinForManger(object):
    """
    .. py:class:: MixinForManager()

        Role: provide a list of classes to be registered in UserDataManager()

        Developer note:
        Please import and append new modules to the end of the 'mixin_list'
    """
    mixin_list = [IteratorMixin,
                  CommuteTimeMixin,
                  DistanceMixin,
                  CorrelationMixin,
                  PlotMatrix,
                  DataIO]