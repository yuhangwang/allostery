# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: manager

    Role: an interface for talking to the actual data processing classes/proxies
    Developer Note: It plays the role of "controller" in the MVC design pattern
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import re
#------------------------------
# `allostery` package modules
#------------------------------
from allostery.proxy.prodyProxy import manager as DataManagerProxy
from allostery.pkgtools.pkg_manager import DependencyResolver
from allostery.core.allo_dependency import MixinForManger

#------------------------------
## Developer's Notes ##
# 1. To avoid method name clashing during "Aggregation"|"Association",
#    it is better to to letter the "mixin" return an object and
#    let the client to call additional method attribute upon it,
#    instead going through the interface to call the method attribute.
#    This also reduces the bureaucracy :)
#    (Steven(Yuhang) Wang; 10-20-14)

#============================================================================
#               [[[ Interface: read/manipulate user data]]]
#============================================================================
class UserDataManager(object):
    """
    .. py:class:: UserDataManager(psf_filename, trajectories)

        Role: an universal interface between user input data and analysis atomselectionlib
        Responsibilities:
            1. provide user access to the public methods from the registered delegate modules
            2. provide user an instance object of the registered delegate modules
               using .get_module(moduleName)

        :param str psf_filename: name of the topology file
        :param str | list trajectories: name | list of trajectory file names
    """
    def __init__(self, psf_filename, trajectories, frame_stride=1):
        """
        Role: use delegation to create an user data object
        """
        self.delegate_dict = dict()
        self.attr_delegate_dict = dict()

        self.delegate_dict['universe'] = DataManagerProxy.Universe(psf_filename, trajectories, frame_stride=frame_stride)
        self.attr_delegate_dict['universe'] = self.delegate_dict['universe'].getPublicMethods()

        for _class in DependencyResolver(MixinForManger.mixin_list).get_sorted():
            _class_name = _class.__name__
            self.delegate_dict[_class_name] = _class(**self.delegate_dict)
            self.attr_delegate_dict[_class_name] = _class.getPublicMethods()

        # add guest attributes to the host class
        UserDataManager._add_guest_attr(self.attr_delegate_dict)



    @classmethod
    def _add_guest_attr(cls,guest_attr_dict):
        """
        Add the attributes from the guest objects into the host's _list_guest_attr list
        :param guest_attr_dict:
        :return:
        """
        cls._list_guest_attr = []
        for _delegate in guest_attr_dict:
            cls._list_guest_attr += guest_attr_dict[_delegate]

    def __getattr__(self, method_name):
        def _method_delegation(*args, **kwargs):
            notFound = True

            for _delegate in self.attr_delegate_dict:
                if method_name in self.attr_delegate_dict[_delegate]:
                    return getattr(self.delegate_dict[_delegate], method_name)(*args, **kwargs)

            if notFound:
                msg = "Error Hint: method named \"{0}\" not found by class UserDataManager(). Typo?".format(method_name)
                raise UserWarning(msg)

        return _method_delegation

    def get_module(self,moduleName):
        """
        .. py:method:: UserDataManager.get_module(moduleName)
            Role: get an instance object of the registered modules
            :param str moduleName: name of the module (str), e.g. "PlotMatrix"
            :return: an instance object
            :raise: UserWarning() when moduleName is not found in the registered module list
        """
        try:
            return self.delegate_dict[moduleName]
        except KeyError:
            msg = "Error Hint: module \"{0}\" is not part of Allostery project. Typo?"
            raise UserWarning(msg)

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Return a list of public methods for class

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        raw_list += cls._list_guest_attr
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    @classmethod
    def getCoreMethods(cls):
        """
        .. py:classmethod:: .getCoreMethods()

            Get a list of core methods (not from the delegated objects)

            :return: list
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()