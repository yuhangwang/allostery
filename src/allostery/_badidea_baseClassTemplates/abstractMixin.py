"""
Note: using inheritance at high level turns out to be a bad idea.
There are so many hidden build-in methods for each python class
that I am not aware of and other packages like `dill` rely on it.
Conclusion: inheritance make things complicated and not worth it.
"""

# ============================================================
# Compatibility with python 3
# ============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: _badidea_baseClassTemplates.abstract_mixin

    Role: provide an abstract class for a generic
        mixin.
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '11-30-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import re
#------------------------------
# `allostery` package modules
#------------------------------
from allostery.pkgtools.pkg_manager import DependencyResolver


class AlloMixinTemplate(object):
    """
    .. py:class::
        Goal: compute nearest-neighbor related quantities,
            including the number of data points within a cutoff
    """

    def __init__(self):
        self.delegate_dict = dict()
        self.attr_delegate_dict = dict()
        self.attr_self_builtin = [name for name in dir(self.__class__) if not re.match(r'^_.*', name)]
        self.public_method_register = self.attr_self_builtin

    def _add_delegates(self, list_of_mixins):
        """
        Define axis property refinement delegates
        """
        for _class in DependencyResolver(list_of_mixins).get_sorted():
            _class_name = _class.__name__
            self.delegate_dict[_class_name] = _class(**self.delegate_dict)
            self.attr_delegate_dict[_class_name] = dir(_class)

        for _delegate in self.attr_delegate_dict:
            self.public_method_register += self.attr_delegate_dict[_delegate]

    def __getattr__(self, method_name):
        def _method_delegation(*args, **kwargs):
            notFound = True

            for _delegate in self.attr_delegate_dict:
                if method_name in self.attr_delegate_dict[_delegate]:
                    return getattr(self.delegate_dict[_delegate], method_name)(*args, **kwargs)

            if notFound:
                try:
                    getattr(self,method_name)(*args,**kwargs)
                except:
                    msg = "Error Hint: method named \"{0}\" not found by class PlotMatrix(). Typo?".format(method_name)
                    raise UserWarning(msg)

        return _method_delegation

    def __dir__(self):
        """
        Role: define public method list
        :return: python list
        """
        return self.get_public_method_list()

    def get_public_method_list(self):
        """
        Role: return a list all public methods offered by :class Proxy_MDAnalyhsis
        :return: numpy string array
        """
        return self.public_method_register
