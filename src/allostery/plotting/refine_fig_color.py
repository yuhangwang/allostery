#============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
purpose: refine the color scale of the plot
author: Yuhang Wang
date: 10/23/2014
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#============================================================
# External Module Dependencies
#============================================================


class RefineColorMixin(object):
    """
    .. py:class:: RefineColorMixin(axis_object)
    """
    def __init__(self, **kwargs):
        self.matshow_obj = kwargs['matshow_obj']

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: DistanceMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: list of class objects
        """
        return None

    def get_color_limits(self):
        """
        .. py:method:: RefineColorMixin.get_clim()

            Role: get the color limits for image scaling
        :return: min, max
        :rtype: list
        """
        return self.matshow_obj.get_clim()

    def set_color_limits(self, cmin, cmax):
        """
        .. py:method:: RefineColorMixin.set_clim()

            Role: set the color limits for image scaling

        """
        self.matshow_obj.set_clim(cmin,cmax)