#============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
purpose: show some statistics of the plotted data
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '11-01-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#============================================================
# External Module Dependencies
#============================================================
import re
import numpy


class ShowFigStatsMixin(object):
    """
    .. py:class:: ShowFigStatsMixin(axis_object)

        Role: show some statistics of the plotted data
        :param axis_object: Matplotlib axes object
    """
    def __init__(self, **kwargs):
        self.axis_obj = kwargs['axis_obj']
        self.fig_obj = kwargs['fig_obj']

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: DistanceMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: list of class objects
        """
        return None

    def show_sum(self, data_matrix, x_n_periods=None, y_n_periods=None,
                 x_head_skip=0, x_tail_skip=0,
                 y_head_skip=0, y_tail_skip=0,
                 x_offset=0, y_offset=0,
                 font_size=40,
                 font_color='w',
                 font_weight='bold',
                 box_alpha=0.5,
                 box_padding=0.2,
                 box_style='round',
                 box_color='w',
                 box_edgecolor='none',
                 n_decimals=0,
                 operations=[]):
        """
        .. py:method:: ShowFigStatsMixin.show_sum(data_matrix,
                 x_period=None, y_period=None,
                 x_head_skip=0, x_tail_skip=0,
                 y_head_skip=0, y_tail_skip=0,
                 x_offset=0, y_offset=0,
                 font_size=10,
                 font_color='k',
                 font_weight='bold',
                 box_alpha=0.5,
                 box_padding=0.2,
                 box_style='round',
                 box_color='w',
                 box_edgecolor='none',
                 n_decimals=0,
                 operations=[])
            Role: show the sum of values of the plotted matrix
            :param data_matrix: input data matrix
            :param int x_n_periods: periodicity along x, e.g. x_period=3 means
                                the residue ID's is repeated 3 times along x.
            :param int y_n_periods: c.f. x_period
            :param int x_head_skip: data points between 0 and x_head_skip along X will not be
                                used in the summation.
            :param int y_head_skip: c.f. x_head_skip
            :param int x_tail_skip: data points between x_tail_skip and x_period will not be used
                                in the summation.
            :param int y_tail_skip: c.f. x_tail_skip
            :param float x_offset: offset for the labels (of the sum for each sub-matrix)
            :param float y_offset: c.f. x_offset
            :param int font_size: default 10
            :param int font_color: default 'k' (i.e. black)
            :param int font_weight: default: 'bold'
            :param int box_alpha: the transparency of the background for the text box
            :param float box_padding: padding around the text box
            :param str box_style: shape of the text box.
                Options: 'square', 'round', 'round4', 'roundtooth', 'sawtooth', 'larrow', 'rarrow'
            :param str box_color: default 'w'
            :param str box_edgecolor: default 'none'
            :param int n_decimals: number of decimals to show for the calculated sum's
            :param list operations: a list of funcitons to be applied to the results (i.e. sum's)
                    example: operations=[numpy.abs, numpy.log] will give the log(abs(result))
            :raise: UserWarning() if x_period or y_period is 0.
            :return: a matrix of sum's

        """
        if data_matrix.ndim != 2:
            msg = "Error Hint: the input matrix has dimension: {0}, but it must be 2".format(data_matrix.ndim)
            raise UserWarning(msg)

        n_xpoints, n_ypoints = numpy.shape(data_matrix)

        if x_n_periods is not None:
            if x_n_periods == 0:
                msg = "Error Hint: You have specified x_period=0. Invalid input! Try again!"
                raise UserWarning(msg)
            else:
                _x_block_size = n_xpoints//x_n_periods

        else:
            _x_block_size = 1

        if y_n_periods is not None:
            if y_n_periods == 0:
                msg = "Error Hint: You have specified y_period=0. Invalid input! Try again!"
                raise UserWarning(msg)
            else:
                _y_blocks_size = n_xpoints//x_n_periods
        else:
            _y_blocks_size = 1

        # relative scale (from 0 to 1)
        _fig_total_x_scale = 0.98
        _fig_total_y_scale = 0.98
        _scaled_x_block = _fig_total_x_scale/x_n_periods
        _scaled_y_block = _fig_total_y_scale/y_n_periods

        _sum_matrix = numpy.zeros((x_n_periods, y_n_periods))


        ## calculate sum's
        for _i_row in range(x_n_periods):
            for _i_col in range(y_n_periods):
                _x_id_one = _i_row * _x_block_size + x_head_skip
                _x_id_end = (_i_row + 1) * _x_block_size - x_tail_skip

                _y_id_one = _i_col * _y_blocks_size + y_head_skip
                _y_id_end = (_i_col + 1) * _y_blocks_size - y_tail_skip

                _data = data_matrix[_x_id_one:_x_id_end, _y_id_one:_y_id_end]

                _sum_matrix[_i_row,_i_col] = numpy.sum(_data)

                for _op in operations:
                    _sum_matrix[_i_row, _i_col] = _op(_sum_matrix[_i_row, _i_col])

                ## plot

                _txt = "{0:.{1}f}".format(_sum_matrix[_i_row,_i_col], n_decimals)
                _x_native_offset = -0.1*_scaled_x_block
                _y_native_offset = -0.05*_scaled_y_block

                _x_pos = 0.5 * _scaled_x_block + _i_row * _scaled_x_block + _x_native_offset + x_offset
                _y_pos = 0.5 * _scaled_y_block + _i_col * _scaled_y_block + _y_native_offset + y_offset
                self.axis_obj.text(_x_pos, _y_pos, _txt,
                            transform=self.axis_obj.transAxes, # use relative scale 0 to 1
                            fontsize=font_size,
                            fontweight=font_weight,
                            color=font_color,
                            bbox=dict(facecolor=box_color,
                                      alpha=box_alpha,
                                      edgecolor=box_edgecolor,
                                      boxstyle="{0},pad={1}".format(box_style,box_padding)),
                            )

        return _sum_matrix


    def __dir__(self):
        """
        Role: define public accessible methods
        :return: numpy list
        """
        output = dir(ShowFigStatsMixin)
        return [name for name in output if not re.match(r'^_.*', name)]
