#============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
purpose: refine figure text
author: Yuhang Wang
date: 10/23/2014
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#============================================================
# External Module Dependencies
#============================================================
import re


class RefineFigTextMixin(object):
    """
    .. py:class:: RefineFigTextMixin(axis_object)

        Role: refine the text fields in the figure.
            example: title, annotations
        :param axis_object: Matplotlib axes object
    """
    def __init__(self, **kwargs):
        self.axis_obj = kwargs['axis_obj']

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: DistanceMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: list of class objects
        """
        return None

    def set_fig_title(self,title="",font_size=20):
        """
        .. py:method:: RefineFigTextMixin.set_title(title="", font_size=20)

            Role: set|refine figure title
            :param str title: title of the figure
            :param int font_size: font size of the figure title
        """
        self.axis_obj.set_title(title,fontsize=font_size)

    def __dir__(self):
        """
        Role: define public accessible methods
        :return: numpy list
        """
        output = dir(RefineFigTextMixin)
        return [name for name in output if not re.match(r'^_.*', name)]
