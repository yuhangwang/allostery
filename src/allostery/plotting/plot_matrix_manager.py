# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
purpose: plotting commute time matrix
author: Yuhang Wang
date: 10/06/2014
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-06-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import matplotlib.pyplot as pyplot
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import rcParams
import numpy
import re
#------------------------------
# `allostery` package modules
#------------------------------
from allostery.pkgtools.pkg_manager import DependencyResolver
from allostery.plotting._dependency import MixinForPlotMatrix


#------------------------------
# matplotlib setup
#------------------------------
rcParams['xtick.direction'] = 'out'
rcParams['ytick.direction'] = 'out'


class PlotMatrix(object):
    """
    .. py:class:: PlotMatrix()

        Role: plotting the matrix
    """

    _list_addon_attr = []

    def __init__(self,**kwargs):
        self.delegate_dict = dict()
        self.attr_delegate_dict = dict()

    def plot_matrix(self, input_matrix, width=10, height=10,cut_abs=None):
        """
        .. py:method:: PlotMatrix.plot_matrix(input_matrix, width=10, height=10, logscale=False)
            Role: plot a matrix
            :param input_matrix: input matrix
            :param width: figure width
            :param height: figure height
            :param float cut_abs: filter out abs(values)<cut_abs
        """

        self.data = input_matrix
        # set the min/max of color range to match the min/max of the data
        # self.fig is a matplotlib.figure.Figure object
        # self.axis0 is a axis object
        [self.fig_obj, self.axis_obj] = pyplot.subplots(figsize=(width, height))

        self.axis_divider_obj = make_axes_locatable(self.axis_obj)

        # note: user origin="lower" argument to show the first row at the bottom
        if cut_abs is not None:
            self.filter = numpy.abs(self.data) > cut_abs
            self.matshow_obj = self.axis_obj.matshow(numpy.multiply(self.data,self.filter), 
                origin="lower",
                cmap=pyplot.cm.viridis)
        else:
            self.matshow_obj = self.axis_obj.matshow(self.data, origin="lower",
                cmap=pyplot.cm.viridis)

        self.delegate_dict['axis_obj'] = self.axis_obj
        self.delegate_dict['fig_obj'] = self.fig_obj
        self.delegate_dict['axis_divider_obj'] = self.axis_divider_obj
        self.delegate_dict['matshow_obj'] = self.matshow_obj

        #--------------------------------------------------------------------------------
        # Add delegates
        #--------------------------------------------------------------------------------
        self.__add_delegates()

        #--------------------------------------------------------------------------------
        # change  aspect ratio automatically
        #--------------------------------------------------------------------------------
        self.axis_obj.set_aspect('auto', adjustable='box-forced', anchor='E')

        self.use_defaults()

        return [self.fig_obj, self.axis_obj]

    def __add_delegates(self):
        """
        Define axis property refinement delegates
        """
        for _class in DependencyResolver(MixinForPlotMatrix.mixin_list).get_sorted():
            _class_name = _class.__name__
            self.delegate_dict[_class_name] = _class(**self.delegate_dict)
            self.attr_delegate_dict[_class_name] = dir(_class)

        for _delegate in self.attr_delegate_dict:
            PlotMatrix._list_addon_attr += self.attr_delegate_dict[_delegate]


    def __getattr__(self, method_name):
        def _method_delegation(*args, **kwargs):
            notFound = True

            for _delegate in self.attr_delegate_dict:
                if method_name in self.attr_delegate_dict[_delegate]:
                    return getattr(self.delegate_dict[_delegate], method_name)(*args, **kwargs)

            if notFound:
                msg = "Error Hint: method named \"{0}\" not found by class PlotMatrix(). Typo?".format(method_name)
                raise UserWarning(msg)

        return _method_delegation

    def use_defaults(self):
        """
        Role: apply default fine-tuning parameters
        :return: None
        """
        self.delegate_dict['RefineColorBarMixin'].add_color_bar()
        self.delegate_dict['RefineAxisLabelMixin'].set_xlabel("Residues")
        self.delegate_dict['RefineAxisLabelMixin'].set_ylabel("Residues")
        self.delegate_dict['RefineAxisTickMixin'].grid()
        self.delegate_dict['RefineAxisTickMixin'].set_xtick_fontsize()
        self.delegate_dict['RefineAxisTickMixin'].set_ytick_fontsize()
        self.delegate_dict['RefineAxisTickMixin'].set_xtick_positions("bottom")

    def show(self):
        """
        Role: show the plotting
        :return: None
        """
        pyplot.show()

    def save(self, output_filename, dpi=150):
        """
        Save plotting
        :param output_figname: output figure file name
               dpi: resolution of the output figure (default: 150)
        :return: None
        """
        self.fig_obj.savefig(output_filename, bbox_inches='tight', dpi=dpi)

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: PlotMatrix.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: set of class objects
        """
        return None

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getCoreMethods()

            Get a list of core methods (not from the delegated objects)

            :return: list
        """
        raw_list = dir(cls)
        raw_list += cls._list_addon_attr
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()
