#============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
purpose: refine color bar
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#============================================================
# External Module Dependencies
#============================================================
import matplotlib.pyplot as pyplot
import re


class RefineColorBarMixin(object):
    """
    .. py:class:: AddColorBarMixin(axis_divider_object, matshow_obj)

        Role: provide the host class with the ability to add and finetune color bar
    """

    def __init__(self, **kwargs):
        self.axis_divider_obj = kwargs['axis_divider_obj']
        self.matshow_obj = kwargs['matshow_obj']

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: DistanceMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: list of class objects
        """
        return None

    def add_color_bar(self):
        """
        Role: add color bar
        """
        self.axis_color_bar = self.axis_divider_obj.append_axes("right", size="5%", pad=0.05)
        self.color_bar = pyplot.colorbar(self.matshow_obj, cax=self.axis_color_bar)

        ## use default figure refinement parameters
        self.set_color_bar_tick_font_size(self.color_bar)
        self.set_color_bar_label(self.color_bar)

    def set_color_bar_ticks(self, color_bar_obj, list_of_ticks):
        """
        Role: update the ticks for the color bar
        :param color_bar_obj: a matplotlib.pyplot.colorbar() object
               list_of_ticks: a list of numbers

        :return: None
        """
        color_bar_obj.set_ticks(list_of_ticks)

    def set_color_bar_tick_labels(self, color_bar_obj, list_of_labels):
        """
        Role: update the tick labels for the color bar
        :param color_bar_obj: a matplotlib.pyplot.colorbar() object
        :param list_of_labels: a list of strings
        :return: None
        """
        color_bar_obj.set_ticklabels(list_of_labels)

    def set_color_bar_tick_font_size(self, color_bar_obj=None, font_size=20):
        """
        Role: set the font size of the color bar ticks
        :return: None
        """
        # change color bar tick font size
        for tick in color_bar_obj.ax.get_yticklabels():
          tick.set_fontsize(font_size)


    def set_color_bar_label(self, color_bar_obj, text="", font_size=20, label_pad=20):
        """
        Role: set the label text for the color bar
        :param text: text for the color bar
               font_size: color bar text font size
               label_pad: padding for the color bar label
        :return: None
        """
        self.color_bar_text = text
        color_bar_obj.set_label(self.color_bar_text, fontsize=font_size, labelpad=label_pad)

    def __dir__(self):
        """
        Role: define public accessible methods
        :return: numpy list
        """
        output = dir(RefineColorBarMixin)
        return [name for name in output if not re.match(r'^_.*', name)]
