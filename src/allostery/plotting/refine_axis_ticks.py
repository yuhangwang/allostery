# ============================================================
# Compatibility with python 3
# ============================================================
from __future__ import print_function, division
#============================================================
"""
purpose: refine axis tick properties
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#============================================================
# External Module Dependencies
#============================================================
import re

class RefineAxisTickMixin(object):
    """
    .. py:class:: RefineAxisTicMixin(axis_object)

        Role: set the tick properties of any matplotlib axis object
    """

    def __init__(self, **kwargs):
        self.axis_obj = kwargs['axis_obj']

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: DistanceMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: list of class objects
        """
        return None

    def grid(self, on=False):
        """
        Role: add grids to the figure
        :param on: boolean True/False
        :return: None
        """
        if on:
            self.axis_obj.grid()

    def set_xtick_fontsize(self, font_size=16):
        """
        Role: update the font size of the ticks along X axis
        """
        for tick in self.axis_obj.xaxis.get_major_ticks():
            tick.label.set_fontsize(font_size)

    def set_ytick_fontsize(self, font_size=16):
        """
        Role: update the font size of the ticks along Y axis
        """
        for tick in self.axis_obj.yaxis.get_major_ticks():
            tick.label.set_fontsize(font_size)

    def set_xtick_labels(self, tick_labels, interval=None, period=None,rotation=0):
        """
        .. py:method:: RefineAxisTickMixin.set_xtick_labels(tick_labels, interval=None, period=None)
             Role: change the tick labels for X axis
            :param list tick_labels: a list of new tick labels
                    which must have the same length as the dimension of
                    the matrix plotted
            :param int interval: interval for showing ticks
            :param int period: period for tick labels (e.g. in the case of trimeric proteins)
            :param float rotation: degree of rotation of the labels
            :raise: UserWarning("Warning: the tick labels you specified is an empty list!")
                    if tick_labels is []
        """
        n_ticks = len(tick_labels)

        if n_ticks == 0:
            msg = "Warning: the tick labels you specified is an empty list!"
            raise UserWarning(msg)

        if period is None or period == 0:
            period = n_ticks

        if interval is None:
            interval = max(1, period // 10)

        while interval > period:
            interval = max(1, n_ticks // 5)

        new_tick_locations = []
        new_tick_labels = []

        for _p in range(n_ticks // period):
            offset = _p * period
            new_tick_locations += [i + offset for i in range(period) if i % interval == 0]
            new_tick_labels += [tick_labels[i] for i in new_tick_locations]

        self.axis_obj.set_xticks(new_tick_locations)
        self.axis_obj.set_xticklabels(new_tick_labels, rotation=rotation)

    def set_ytick_labels(self, tick_labels, interval=None, period=None, rotation=0):
        """
        .. py:method:: RefineAxisTickMixin.set_ytick_labels(tick_labels, interval=None, period=None)
             Role: change the tick labels for Y axis
            :param list tick_labels: a list of new tick labels
                    which must have the same length as the dimension of
                    the matrix plotted
            :param int interval: interval for showing ticks
            :param int period: period for tick labels (e.g. in the case of trimeric proteins)
            :param float rotation: degree of rotation of the labels
            :raise: UserWarning("Warning: the tick labels you specified is an empty list!")
                    if tick_labels is []
        """
        n_ticks = len(tick_labels)

        if n_ticks == 0:
            msg = "Warning: the tick labels you specified is an empty list!"
            raise UserWarning(msg)

        if period is None or period == 0:
            period = n_ticks

        if interval is None:
            interval = max(1, period // 10)

        while interval > period:
            interval = max(1, n_ticks // 5)

        new_tick_locations = []
        new_tick_labels = []

        for _p in range(n_ticks // period):
            offset = _p * period
            new_tick_locations += [i + offset for i in range(period) if i % interval == 0]
            new_tick_labels += [tick_labels[i] for i in new_tick_locations]

        self.axis_obj.set_yticks(new_tick_locations)
        self.axis_obj.set_yticklabels(new_tick_labels, rotation=rotation)

    def get_xticks(self):
        """
        Role: get current x tick locations
        :return: a list of tick locations
        :rtype: list
        """
        return self.axis_obj.get_xticks().tolist()

    def get_yticks(self):
        """
        Role: get current y tick locations
        :return: a list of tick locations
        :rtype: list
        """
        return self.axis_obj.get_yticks().tolist()

    def offset_xtick_labels(self, offset):
        """
        Role: make an offset of the tick labels of x axis
        :param offset: scalar
        """
        old_tick_labels = self.axis_obj.get_xticks().tolist()
        new_tick_labels = [str(float(x) + offset).rstrip('0').rstrip('.') for x in old_tick_labels]
        self.axis_obj.set_xticklabels(new_tick_labels)

    def offset_ytick_labels(self, offset):
        """
        Role: make an offset of the tick labels of y axis
        :param offset: scalar
        """
        old_tick_labels = self.axis_obj.get_yticks().tolist()
        new_tick_labels = [str(float(y) + offset).rstrip('0').rstrip('.') for y in old_tick_labels]
        self.axis_obj.set_yticklabels(new_tick_labels)

    def set_xtick_positions(self, position="bottom"):
        """
        Role: change the position of the x axis ticks
        """
        self.axis_obj.xaxis.set_ticks_position(position)

    def set_ytick_positions(self, position="left"):
        """
        Role: change the position of the x axis ticks
        """
        self.axis_obj.yaxis.set_ticks_position(position)

    def set_xticks(self, new_xticks):
        """
        .. py:method:: RefineAxisTickMixin.set_xticks(new_xticks)

            Role: set new x sticks
            :param new_xticks: array of new x ticks
        """
        self.axis_obj.set_xticks(new_xticks)

    def set_yticks(self, new_yticks):
        """
        .. py:method:: RefineAxisTickMixin.set_yticks(new_yticks)

            Role: set new y sticks
            :param new_yticks: array of new y ticks
        """
        self.axis_obj.set_yticks(new_yticks)

    def add_hline(self,positions,width=2, style='-',color='k'):
        """
        .. py:method:: RefineAxisTickMixin.add_hlines(postions, width, style, color)
            Role: add horizontal lines to the plots
            :param list positions: list of positions for the horizontal lines
            :param int width=2: line width
            :param str style='-': line style
            :param char color='w': line color
        """
        for _pos in positions:
            self.axis_obj.axhline(_pos, linewidth=width, linestyle=style, color=color)

    def add_vline(self,positions,width=2, style='-',color='k'):
        """
        .. py:method:: RefineAxisTickMixin.add_vlines(postions, width, style, color)
            Role: add vertical lines to the plots
            :param list positions: list of positions for the horizontal lines
            :param int width=2: line width
            :param str style='-': line style
            :param char color='w': line color
        """
        for _pos in positions:
            self.axis_obj.axvline(_pos, linewidth=width, linestyle=style, color=color)

    def __dir__(self):
        """
        Role: define public accessible methods
        :return: numpy list
        """
        output = dir(RefineAxisTickMixin)
        return [name for name in output if not re.match(r'^_.*', name)]