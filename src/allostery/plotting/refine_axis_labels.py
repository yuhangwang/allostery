#============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
purpose: refine axis tick label properties
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#============================================================
# External Module Dependencies
#============================================================
import re

class RefineAxisLabelMixin(object):
    """
    .. py:class:: RefineAxisLabelMixin(axis_object)

        Role: set label for any matplotlib axis object

        :param axis_object: matplotlib Axis instance object
    """
    def __init__(self, **kwargs):
        self.axis_object = kwargs['axis_obj']

    @classmethod
    def get_dependency(cls):
        """
        .. py:method: DistanceMixin.get_dependency()

            Role: return a list of external mix-in's that it depends on
                If no dependencies, return None

            :return: list of class objects
        """
        return None

    def set_xlabel(self, text="", font_size=30, label_pad=10):
        """
        Role: fine-tune X axis label
        """
        self.axis_object.set_xlabel(text,labelpad=label_pad,fontsize=font_size)

    def set_ylabel(self, text="", font_size=30, label_pad=10):
        """
        Role: fine-tune Y axis label
        """
        self.axis_object.set_ylabel(text,labelpad=label_pad,fontsize=font_size)

    def __dir__(self):
        """
        Role: define public accessible methods
        :return: numpy list
        """
        output = dir(RefineAxisLabelMixin)
        return [name for name in output if not re.match(r'^_.*', name)]

