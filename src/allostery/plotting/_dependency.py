# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
A list of mix-in's for :py:class`PlotMatrix`
"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

from allostery.plotting.refine_color_bar import RefineColorBarMixin
from allostery.plotting.refine_axis_labels import RefineAxisLabelMixin
from allostery.plotting.refine_axis_ticks import RefineAxisTickMixin
from allostery.plotting.refine_fig_text import RefineFigTextMixin
from allostery.plotting.refine_fig_color import RefineColorMixin
from allostery.plotting.show_fig_stats import ShowFigStatsMixin

class MixinForPlotMatrix(object):
    mixin_list = [RefineColorBarMixin,
                  RefineAxisTickMixin,
                  RefineAxisLabelMixin,
                  RefineFigTextMixin,
                  RefineColorMixin,
                  ShowFigStatsMixin,
                  ]