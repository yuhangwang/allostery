"""
Goal: compute residue pairwise center of mass distances
Author: Yuhang Wang
Date: 09-18-2014
"""
# ============================================================
# Compatibility with python 3
# ============================================================
from __future__ import print_function, division, absolute_import
# ============================================================

#============================================================
# External Module Dependencies
#============================================================
import re
import numpy

#------------------------------
# Allostery auxiliary modules
#------------------------------
from allostery.core import allo_manager
from allostery.atomselectionlib.selection_tools import SelectionStringMaker as selectionHelper
from allostery.atomselectionlib.allostery_keywords import AlloKeywords
from allostery.atomselectionlib.selection_style_lib import MDAnalysisSelectionStyle, ProdySelectionStyle

#============================================================


#============================================================================
#               [[[ Client(View): Allostery class ]]]
#============================================================================
#                   [[[ Main Frontend ]]]
#============================================================================
class Allostery(object):
    """
    .. py:class:: Allostery(psf_filename : str, trajectories=None : str|list|tuple)

        Goal: get the allosteric relations between residues
        Role: This "Allostery" class is a front-end interface
            for reading data and doing analyses

        :param str psf_filename: file name for the *.psf
        :param str|list|tuple trajectories: file name for the trajectory files, or list|tuple of file names
    """

    _list_guest_attr = []

    def __init__(self, psf_filename, trajectories=None, frame_stride=1):
        """
        .. py:method:: Allostery__init__(psf_filename, trajectories=None)

            Role: create an universe object to store user data

            :param psf_filename: file name for the *.psf
            :param trajectories: file name for the trajectory files, e.g. *.dcd, *.xtc
            :param frame_stride: stride for reading the frames in the trajectory
            :raise: UserWarning if not trajectory is given
        """
        self.psf_filename = psf_filename
        if isinstance(trajectories,basestring):
            self.trajectories = [trajectories]
        else:
            self.trajectories = [filename for filename in trajectories]

        if len(self.trajectories) == 0:
            msg = "ERROR HINT: You must specify at least one trajectory file.\n"
            msg += "\t e.g. allo.Allostery('my.psf', ['my.dcd'])\n"
            raise UserWarning(msg)

        Allostery._list_guest_attr = []

        self.userDataManager = allo_manager.UserDataManager(self.psf_filename,
                                                            self.trajectories,
                                                            frame_stride=frame_stride)
        Allostery._list_guest_attr += self.userDataManager.get_public_methods()

        self.selectorMixin = AlloSelectorMixin(self.userDataManager)
        Allostery._list_guest_attr += self.selectorMixin.get_public_methods()


    def __getattr__(self, method_name):
        def _method_delegation(*args, **kwargs):
            if method_name in Allostery.getCoreMethods():
                return getattr(self, method_name)(*args, **kwargs)

            elif method_name in self.selectorMixin.get_public_methods():
                return getattr(self.selectorMixin, method_name)(*args,**kwargs)

            elif method_name in self.userDataManager.get_public_methods():
                return getattr(self.userDataManager,method_name)(*args,**kwargs)


            else:
                print(self.delegate_method_register_array)
                msg = "Error Hint: method named \"{0}\" not found by class \"Allostery\". Typo?".format(method_name)
                raise UserWarning(msg)
        return _method_delegation

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Return a list of public methods

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        raw_list += cls._list_guest_attr
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    @classmethod
    def getCoreMethods(cls):
        """
        .. py:classmethod:: .getCoreMethods()

            Return a list of core public methods  (not from delegates)

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()


#========================================================================
#                   [[[ SelectorMixin ]]]
#========================================================================
class AlloSelectorMixin(object):
    """
    .. py:class:: AlloSelectorMixin(userDataManger,
                keyword_conversion_dict=MDAnalysisSelectionStyle.keyword_dict() : dict)

        Role: provide host class with the ability to make atom selection strings
            that conforms to the style of a delegate module like MDAnalysis

        :param pyobject userDataMater: a python object (delegate) that has method ".select(str)"
        :param dict keyword_conversion_dict: a dictionary that converts
            :py:class:`~allo_style_keywords.AlloKeywords` style. The default is
            :py:meth:`MDAnalysisSelectionStyle.keyword_dict`
    """

    def __init__(self, userDataManager, keyword_conversion_dict=MDAnalysisSelectionStyle.keyword_dict()):
        self.userDataManger = userDataManager
        self.keyword_conversion_dict = keyword_conversion_dict

        # developer note (Yuhang Wang 10-18-2014):
        #  I also need Prody to do an equivalent atoms selection
        #  due to a bug in MDAnalysis (when calling myselection.residues(),
        #  it returns residue instance from the whole system, not within the current
        #  atom selection.
        self.helper_keyword_conversion_dict = ProdySelectionStyle.keyword_dict()
        self.helper_selection_string = ""

        self.selection_keyword_list = []
        self.selection_member_lists = []
        self.selection_string = ""
        self.base_criteria = AlloKeywords.ALL
        self.keyword_list = []
        self.member_list_of_lists = []

    def select(self,base_criteria=AlloKeywords.ALL, keyword_list=None, member_list_of_lists=None):
        """
        .. py:method:: AlloSelectionMixin.select(base_criteria=None
        """

        self.base_criteria = base_criteria
        self.keyword_list = keyword_list
        self.member_list_of_lists = member_list_of_lists

        if self.keyword_list is None:
            self.selection_string = self.base_criteria
            self.helper_selection_string = self.base_criteria
            self.keyword_list = []
            self.member_list_of_lists = []

        else:
            ############################################################
            # Verify Inputs #
            ############################################################
            if len(keyword_list) != len(member_list_of_lists):
                msg = "Error Hint: atom selection \"keyword_list\" must have the same length as \"member_list_of_lists\""
                msg += "\n\t Your current inputs: keyword_list={0}; member_list_of_lists={1}".format(keyword_list,member_list_of_lists)
                raise UserWarning(msg)
            ############################################################

        # self.selection_string = selectionHelper(self.base_criteria,
        #                                         self.keyword_list,
        #                                         self.member_list_of_lists,
        #                                         self.keyword_conversion_dict).get_selection_string()
        self.helper_selection_string = selectionHelper(
                                                self.base_criteria,
                                                self.keyword_list,
                                                self.member_list_of_lists,
                                                self.helper_keyword_conversion_dict).get_selection_string()

        #====================#
        # Make atom selection
        #====================#
        self.userDataManger.make_selection(self.helper_selection_string)

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Return a list of public methods

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()


