"""
Role: provide class BuilderMixin to build new data
    such as mass weighting factor matrix
"""
#============================================================
# Compatibility with python 3
from __future__ import print_function, division
#============================================================


#========================================================================
import numpy
import scipy.sparse
#========================================================================


#========================================================================
#                   [[[ BuilderMixin ]]]
#========================================================================
class BuilderMixin(object):
    """
    .. py:class:: BuilderMixin

        Role: provide host class with the ability to build new data attributes
    """

    def __init__(self): pass

    def build_mass_weighting_vector(self, selected_masses, selected_residue_index_ranges):
        """
        .. py:method:: BuilderMixin.build_mass_weighting_vector(selected_masses,
                                    selected_residue_index_range)

            Role: build a mass weighting vector, i.e. the mass of each atom in each selected
                residue is normalized by the total mass of that residue

            :param selected_masses: a vector of masses
            :param selected_residue_index_ranges: range of atom indices for each selected residue
                   *developer note*
                    The 2nd element of each row corresponds to the number of atoms for this residues
                    within in the current atom selection. This makes it easier to index rows.
                    example:
                    i,j = selected_residue_index_ranges[0,:]
                    mass_of_residue_0 = masses[i:j]
            :return: [mass_eighting_vector, mass_of_all_residues]
            :rtype: list of numpy.array
        """
        mass_weighting_vector  = numpy.zeros(selected_masses.shape,dtype=numpy.float32)
        mass_of_residues = []

        number_of_residues = numpy.shape(selected_residue_index_ranges)[0]

        for _i in range(number_of_residues):
            _beg, _end = selected_residue_index_ranges[_i, :]
            _mass_of_residue = numpy.sum(selected_masses[_beg:_end])
            mass_of_residues.append(_mass_of_residue)
            mass_weighting_vector[_beg:_end] = selected_masses[_beg:_end]/_mass_of_residue
            # print("residue {0}: {1} -- {2}; mass={3}".format(_i, _beg, _end, _mass_of_residue))

        return [mass_weighting_vector,numpy.array(mass_of_residues,dtype=numpy.float32)]

    def build_center_of_mass_operator(self, selected_residue_index_ranges,
                                                   n_atoms, n_residues, mass_weighting_vector):
        """
        .. py:method:: BuilderMixin.build_sparse_residue_mass_summation_matrix()

            Role: build a sparse matrix for calculating the center of masses of selected residues

            Example: let the output of :py:meth:`~BuilderMixin.build_sparse_residue_mass_summation_matrix`
                by **"W"**
                and let the target atom coordinate array be **"C"**, then the center of mass coordinates
                of the selected residues is **"W*C"**.

            :param selected_residue_index_ranges: range of atom indices for each selected residue
                    *developer note*
                    The 2nd element of each row corresponds to the number of atoms for this residues
                    within in the current atom selection. This makes it easier to index rows.
                    example:
                    i,j = selected_residue_index_ranges[0,:]
                    mass_of_residue_0 = masses[i:j]
            :param int n_atoms: total number of selected atoms
            :param int n_residues: total number of selected residues
            :param mass_weighting_vector: an array of rescaled masses for selected atoms
                    i.e. each mass is scaled by 1/mass_of_residue_i
            :return: a sparse matrix
                    an input array of atomic masses
            :rtype: scipy.sparse.bsr_matrix of shape (n_residues, n_atoms)
        """
        row_ids = [] # row indices for non-zero elements of the output matrix
        col_ids = [] # colum indices for non-zero elements of the output matrix
        for i in range(n_residues):
            _beg, _end = selected_residue_index_ranges[i,:]
            col_ids += range(_beg,_end)
            row_ids += [i]*(_end - _beg)

        row_id_array = numpy.array(row_ids)
        col_id_array = numpy.array(col_ids)
        content = numpy.array(mass_weighting_vector, dtype=numpy.float32)

        return scipy.sparse.bsr_matrix((content, (row_id_array,col_id_array)), shape=(n_residues,n_atoms))

