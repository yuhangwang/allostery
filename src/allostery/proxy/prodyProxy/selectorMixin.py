"""
Provide host class with the ability to make atom selections using "mdtraj" style
"""
#============================================================
# Compatibility with python 3
from __future__ import print_function, division
#============================================================



#========================================================================
#                   [[[ SelectorMixin ]]]
#========================================================================
import re
import numpy
import prody as ProDy
from allostery.proxy.prodyProxy.builderMixin import BuilderMixin
from allostery.errorwarning.warn_color import TerminalColor


class SelectorMixin(object):
    """
    .. py:class:: SelectorMixin(userDataObj)

        Role: provide host class with the ability to make atom selections

        :param pyobject userDataObj: an :py:class:`MDTraj.AtomGroup` instance
    """

    _SelectorMixin_attr_list = ['atomId_range_per_selected_residue',
                                'center_of_mass_operator',
                                'currentProdySelectionObj',
                                'mass_weighting_vector',
                                'number_of_selected_atoms',
                                'number_of_selected_residues',
                                'selected_atom_masses',
                                'selected_resids',
                                'selected_residue_masses',
                                'selected_resnames',
                                'selected_segids',
                                'atom_selection_string']

    def __init__(self, topology_filename, *args, **kwargs):
        self.prodyTopoObj = ProDy.parsePSF(topology_filename)

        self.builderMixin = BuilderMixin()

        # initialize all data attributes
        # and add a getter function for each
        for _attr in SelectorMixin._SelectorMixin_attr_list:
            setattr(self, _attr, None)
            self._auto_add_attr_getter(_attr) # add a getter function

        # the following attributes can not have None as the initial value
        self.atom_selection_string = "all"

        # add attr_list getter
        self.selector_attr_list = SelectorMixin._SelectorMixin_attr_list
        self._auto_add_attr_getter('selector_attr_list')

        print("selection string: {0}".format(self.atom_selection_string))
        self._select(self.atom_selection_string)

    def update_selection(self, selection_str):
        """
        .. py:method:: .SelectorMixin.select_atoms(selection_str_list)

            Role: make|update atom selections
            :param selection_str: a list of two atom selecitons strings,
                one for mdtraj, the other for prody
        """
        self.atom_selection_string = selection_str
        self._select(self.atom_selection_string)


    def _select(self, selection_str):
        """
        .. py:method:: prodyProxy.SelectorMixin.__select(selection_str_list)

            Role: select part of the MD system
            :param selection_str: a string for atom selection
            :return: a data object that should know the coordinates of selected atoms
        """
        self.atom_selection_string = selection_str
        print("== Atom selection: {}".format(self.atom_selection_string))
        self.currentProdySelectionObj = self.prodyTopoObj.select(self.atom_selection_string)

        #####
        # print("== selected residue IDs", self.currentProdySelectionObj.getResnums())

        if self.currentProdySelectionObj is None:
            msg = "atom selectqion \"{0}\" yields zero atoms".format(self.atom_selection_string)
            msg = TerminalColor.give_warning(msg)
            print(msg)
            return
        else:
            self.number_of_selected_atoms = self.currentProdySelectionObj.numAtoms()
            self.selected_resids = list(set(self.currentProdySelectionObj.getResnums()))
            self.selected_resnames = list(set(self.currentProdySelectionObj.getResnames()))
            self.selected_segids = list(set(self.currentProdySelectionObj.getSegnames()))
            self.selected_atom_masses = self.currentProdySelectionObj.getMasses()
            self.number_of_selected_residues = self._count_num_selected_residues(self.currentProdySelectionObj)
            # developer note:
            # self.prodyAtomSelection must be converted to HierView() object
            # using .getHierView() in order to have .iterResidues() attribute
            selected_residue_iter = self.currentProdySelectionObj.getHierView().iterResidues()
            self.atomId_range_per_selected_residue = self.calc_atomId_range_per_selected_residue(self.number_of_selected_residues,
                                                                                                 selected_residue_iter)

            self.mass_weighting_vector, self.selected_residue_masses = self.builderMixin.build_mass_weighting_vector(
                self.selected_atom_masses,
                self.atomId_range_per_selected_residue)

            # center of mass kernel
            # usage: to get center of mass coordinates: self.center_of_mass_operator*self.selected_coordinates
            self.center_of_mass_operator = self.builderMixin.build_center_of_mass_operator(self.atomId_range_per_selected_residue,
                                                                                           self.number_of_selected_atoms,
                                                                                           self.number_of_selected_residues,
                                                                                           self.mass_weighting_vector)
            return self.currentProdySelectionObj


    def _count_num_selected_residues(self, currentSelectionObj):
        """
        Counter total number of selected residues.
        Note: this is non-trivial when the selection involves multiple
        segments. Thus len(self.selected_resids) will give wrong answers
        due to duplications of resids.

        :param currentSelectionObj: a :py:class:`prody.Selection` object
        :return: integer
        """
        total_num_selected_residues = 0
        unique_segnames = set(currentSelectionObj.getSegnames())
        for _segn in unique_segnames:
            tmp_sel = currentSelectionObj.select('segment {0}'.format(_segn))
            unique_resids = set(tmp_sel.getResnums())
            print("segment {0}: {1} residues".format(_segn, len(unique_resids)))
            total_num_selected_residues += len(unique_resids)
        return total_num_selected_residues


    def calc_atomId_range_per_selected_residue(self, numberResidues, residueIterator):
        """
        .. py:method:: SelectorMixin.calc_residue_index_range(numberResidues, residueIterator)

            Role: build an array to store the range of indices
                  for each residues in the current atom selection
            :param int numberResidues: number of selected residues
            :param residueIterator: an iterator object that yields a Residue() instance object
            :return: a 2D array of starting/ending indices for each selected residues
            :rtype: numpy.array dtype=numpy.int
            example output: [[0,5],
                             [6,11]]
        """
        print("== number of residues: {0}".format(numberResidues))
        output = numpy.zeros((numberResidues,2), dtype=numpy.int)
        ccc = 0 # counter for number of residues
        _id_end = 0 # starting value in order to make _id_begin = _id_end work

        for _residue in residueIterator:
            _numAtoms = _residue.numAtoms()
            _id_begin = _id_end
            _id_end   = _id_begin + _numAtoms
            output[ccc, 0] = _id_begin
            output[ccc, 1] = _id_end
            ccc += 1
        return output

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:method:: SelectorMixin.get_public_methods()
            Role: define public accessible methods
            :return: python list
        """
        raw_list = dir(cls)

        # add all the getter functions (which have been created automatically)
        for _attr in SelectorMixin._SelectorMixin_attr_list:
            raw_list.append('get_{0}'.format(_attr))

        # remove private methods
        public_methods = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_methods

    def get_public_methods(self):
        """
        .. py:method:: SelectorMixin.get_public_methods()

            An instance method to provide a list of public methods
            for :py:class:`~.get_public_methods`

            :return: python list
        """
        return SelectorMixin.getPublicMethods()

    def _auto_add_attr_getter(self, name_attr):
        """
        Automatically add a getter for a data attribute
        :param name_attr: name of the data attribute
        """
        setattr(self, 'get_{0}'.format(name_attr), lambda: getattr(self, name_attr))
        return