"""
An interface Class Proxy_MDTraj to talk to "mdtraj"

Developer's note: The reason to use mdtraj is that
it supports skipping frames when reading trajectories,
while MDAnalysis doesn't.
"""
#============================================================
# Compatibility with python 3
from __future__ import print_function, division
#============================================================

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-17-2014'
__update1__ = '01-31-2015'
__update2__ = '02-01-2015'
__update3__ = '02-06-2015' # simply the public method interface
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import re
from allostery.proxy.prodyProxy.frameMixin import FrameMixin



#---------------------------------------------------------------------------
#                 [[[ Proxy for MDAnalysis ]]]
#---------------------------------------------------------------------------
class Universe(object):
    """
    .. py:class:: Proxy_MDTraj(psf_filename :  str, trajectory_filename : str | list)

        Title: A particular type of Python module for creating user data object
        Purpose: act as a proxy for using "MDTraj" module
        Friend: MDAnalysis module (so Proxy_MDTraj is expected to
                know everything about MDTraj module)
        developer note: Proxy_MDTraj should be the only class that interacts with MDTraj module

        :param str topology_filename: name of the topology file
        :param str | list trajectory_filename: name or list of trajectory file names
    """

    def __init__(self, topology_filename, trajectory_filename,frame_stride=1):
        """
        Role: create an universe object based on user's files
        """
        self.topology_filename = topology_filename

        if isinstance(trajectory_filename, list):
            self.list_trajectory_filenames = trajectory_filename
        else:
            self.list_trajectory_filenames = [trajectory_filename]

        if isinstance(frame_stride,int):
            self.frame_stride = frame_stride
        else:
            msg = "ERROR HINT: please use a number of type 'int' for frame_stride (from Proxy_MDTraj.__init__())\n"
            raise UserWarning(msg)

        self.frameMixin = FrameMixin(self.list_trajectory_filenames, self.topology_filename, frame_stride)

    def __getattr__(self, method_name):
        """
        Re-route method calls to MDTraj' delegates
        :param item: string (i.e. method name)
        """
        def _method_delegation(*args, **kwargs):
            if method_name in Universe.getCoreMethods():
                return getattr(self, method_name)(*args, **kwargs)

            elif method_name in self.frameMixin.get_public_methods():
                return getattr(self.frameMixin, method_name)(*args, **kwargs)

            else:
                msg = "Error Hint: method named \"{0}\" not found by class Proxy_MDTraj. Typo?".format(method_name)
                raise UserWarning(msg)

        return _method_delegation

    def get_dependency(self):
        """
        Role: get a list of classes (str) that this class depends on
            If no dependencies, return None.
        """
        return None

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Return a list of public methods

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        raw_list += FrameMixin.getPublicMethods()
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    @classmethod
    def getCoreMethods(cls):
        """
        .. py:classmethod:: .getCoreMethods()

            Return a list of core public methods  (not from delegates)

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()
