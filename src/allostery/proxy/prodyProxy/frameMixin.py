"""
Role: provide FrameMixin class for dealing with trajectory frames
"""
#============================================================
# Compatibility with python 3
from __future__ import print_function, division
#============================================================



#========================================================================
import re
import os
import numpy
import prody as ProDy
from allostery.proxy.prodyProxy.selectorMixin import SelectorMixin
#========================================================================

#========================================================================
#                   [[[ FrameMixin ]]]
#========================================================================
class FrameMixin(object):
    """
    .. py:class:: FrameMixin(trajectory_filename)

        Provide a proxy object to handle client's trajectories using :py:mod:`mdtraj`

        :param trajectory_filename: a full file path to client's
    """

    def __init__(self, trajectory_filename_list, topology_filename, frame_stride=1):
        self.topology_filename = topology_filename
        self.current_atom_selection_str = "all"

        # define a trajectory object
        first_trajectory_filename = trajectory_filename_list[0]
        if re.match(r'.+.pdb$', first_trajectory_filename):
            self.USE_PDB_MODE = True
            self.trajectoryObj = ProDy.parsePDB(first_trajectory_filename)
            self.num_pdb_frames = 1
        else:
            self.USE_PDB_MODE = False
            self._check_file_exits(trajectory_filename_list[0])
            self.trajectoryObj = ProDy.Trajectory(trajectory_filename_list[0])

        for i in range(1,len(trajectory_filename_list)):
            _filename = trajectory_filename_list[i]
            if self.USE_PDB_MODE:
                print("====Use PDB mode")
                self.trajectoryObj.addCoordset(ProDy.parsePDB(_filename))
                self.num_pdb_frames += 1
            else:
                self._check_file_exits(_filename)
                self.trajectoryObj.addFile(_filename)


        self.frame_stride = frame_stride

        if hasattr(self.trajectoryObj, 'numFrames'):
            self.total_numFrames = self.trajectoryObj.numFrames()
        else:
            self.total_numFrames = self.num_pdb_frames

        self.selectorObj = SelectorMixin(topology_filename, trajectory_filename_list[0])
        self.currentSelectionObj = self.selectorObj.get_currentProdySelectionObj()

        # initialize to the first frame
        # note: frame 0 is the same as frame 1 because ProDy copied
        # the first frame from the trajectory as frame 0
        # and use it as the reference coordinates.
        self.current_frameId = 0
        self.current_selected_coords = self._load_frame(self.current_frameId)

        print("Total number of frames: {0}; frame reading stride: {1}".format(self.trajectoryObj.numFrames(),
                                                                              self.frame_stride))


    def _check_file_exits(self, filename):
        """
        Check whether file exits
        :param file: input file name
        :raise: error if file doesn't exists
        """
        if not os.path.isfile(filename):
            msg = "ERROR HINT: input file {0} does not exists".format(filename)
            raise UserWarning(msg)
        else:
            return

    def __getattr__(self, method_name):
        """
        Role: reroute method calls to MDTraj' delegates
        :param item: string (i.e. method name)
        """
        def _method_delegation(*args, **kwargs):
            if method_name in FrameMixin.getCoreMethods():
                return getattr(self, method_name)(*args, **kwargs)
            elif method_name in self.selectorObj.get_public_methods():
                return getattr(self.selectorObj, method_name)(*args, **kwargs)
            else:
                msg = "Error Hint: method named \"{0}\" not found by class prodyProxy.FrameMixin. Typo?".format(method_name)
                raise UserWarning(msg)

        return _method_delegation

    def get_current_frame_id(self,*args,**kwargs):
        """
        Role: get the current frame ID
        """
        return self.current_frameId

    def get_total_number_of_frames(self,*args,**kwargs):
        """
        Role: get the total number of frames
            in the current trajectory
        """
        return self.total_numFrames

    def goto_next_frame(self,*args,**kwargs):
        """
        Role: move on to the next trajectory frame
        :return None
        """
        if self.current_frameId + self.frame_stride < self.total_numFrames:
            # print("Current Frame: {0}/{1}".format(self.current_frameId, self.total_numFrames-1))
            return self.goto_frame(self.current_frameId+self.frame_stride)
        else:
            msg = "ERROR HINT: Already reached the last frame! "
            msg += "Current frame = {0} out of {1}".format(self.current_frameId, self.total_numFrames-1)
            raise UserWarning(msg)

    def goto_frame(self, frameId, *args,**kwargs):
        """
        Role: go to a specific trajectory frame
        :param frameId: Int scalar
        :return: coordinates for the current selected atoms in the new frame
        """
        if frameId < 0 or frameId >= self.total_numFrames:
            msg = "frameId must be within [0,{0})".format(self.total_numFrames)
            msg += ", but you specified frameId = {0}".format(frameId)
            raise UserWarning(msg)

        self.current_frameId = frameId # update
        self.current_selected_coords = self._load_frame(frameId)
        # print("Current Frame: {0}/{1}".format(self.current_frameId, self.total_numFrames-1))
        return self.current_selected_coords

    def make_selection(self, selection_str):
        """
        .. py:method:: FrameMixin.update_atom_selection(selection_str)

            Select a subset of atoms

            :return: current atom selection object (of class :py:class:`ProDy.Selection`)
        """
        self.current_atom_selection_str = selection_str
        self.selectorObj.update_selection(selection_str)
        self.currentSelectionObj = self.selectorObj.get_currentProdySelectionObj()
        self.current_selected_coords = self._load_frame(self.current_frameId)
        print("done atom selection update!")
        return self.currentSelectionObj

    def get_selected_coords(self):
        """
        .. py:method:: FrameMixin.get_selected_coords()

            Get the coordinates of selected atoms

            :return: numpy array
        """
        return self.current_selected_coords

    def get_selected_com_coords(self):
        """
        .. py:method:: FrameMixin.get_selected_com_coords()

            Get the center of mass matrix of selected residues

            :return: 2D numpy array of shape (N,3), where N is the number
                   of residues.
        """
        com_operator = self.selectorObj.get_center_of_mass_operator()
        coords = self.get_selected_coords()
        com = com_operator*coords
        return com

    def _load_frame(self, frameId):
        """
        Load a certain frame (one-based indexing)

        :param frameId: integer (zero-based)
        :return: numpy array for the coordiantes of shape (n_atoms, 3)
        """
        if frameId < 0:
            msg = "ERROR HINT: frameId can not be negative. Your frameId = {0}".format(frameId)
            raise UserWarning(msg)

        if self.USE_PDB_MODE:
            coords = self.trajectoryObj.select(self.selectorObj.get_atom_selection_string()).getCoordsets(frameId)
            print("shape of coords (_load_frame) (PDB-MODE): {0}".format(coords.shape))
            return coords
        else:
            # self.trajectoryObj.setAtoms(self.currentSelectionObj)
            # print("==total trajectory: {0}".format(self.trajectoryObj.getCoordsets()))

            # Bugs in prodyTrajectory.getCoordsets()
            # The following test will not pass
            #
            # frameId = 1
            # coords1 = self.trajectoryObj.getCoordsets()[frameId,:,:]
            # coords2 = self.trajectoryObj.getCoordsets(frameId)[0,:,:]
            # diff = coords - self.trajectoryObj.getCoordsets(frameId)[0,:,:]
            # error = numpy.sum(numpy.abs(diff))
            # print("===frame {2} diff {0}; error {1}".format(diff, error, frameId))
            # assert error < 1.0E-8
            # This only happens for frame 1, not frame 0 or others.
            # Somehow frame 0 is used twice, both for frame 0 and 1.
            # The actual frame 1 was skipped.
            #
            # Solution: use coords1 for now, which gives the correct behavior

            # coords = self.trajectoryObj.getCoordsets()[frameId,:,:]

            self.trajectoryObj.setAtoms(self.currentSelectionObj)
            self.trajectoryObj.reset() # go to frame 0
            self.trajectoryObj.skip(frameId) # skip n frames
            coords = self.trajectoryObj.nextCoordset()
            return  coords

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .getPublicMethods()

            Return a list of public methods for class

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        raw_list += SelectorMixin.getPublicMethods()
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    @classmethod
    def getCoreMethods(cls):
        """
        .. py:classmethod:: .getCoreMethods()

            Get a list of core methods (not from the delegated objects)

            :return: list
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()
