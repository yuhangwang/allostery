"""
Provide class :py:class:`.TrajectorySeriesInfoMixin` in order to
collect information about the names, number of frames for
a series of trajectory files.
"""
from __future__ import print_function, division


import re
import numpy
import mdtraj as MDTraj

class TrajectorySeriesInfoMixin(object):
    """
    .. py:class:: TrajectorySeriesInfoMixin(list_of_filenames)

        Collect all the information about a list
        of trajectories in one place to facilitate
        the reading of multiple trajectory files
        by :py:class:`~.proxy.prodyProxy.FrameMixin`.
    """

    def __init__(self, list_of_filenames, topology_filename, frame_stride=1):
        """
        Get all the essential information about a list of trajectories

        :param list_of_filenames: list trajectory file names
        :param frame_stride: stride when reading each trajectory
        """
        self.filename_list = list_of_filenames
        self.topology_filename = topology_filename
        self.total_num_trajectories = len(self.filename_list)
        self.num_frames_per_load = 1 # how many frames to load per iteration of loading
        self.list_num_frames = self._count_frames(list_of_filenames,
                                                 topology_filename,
                                                 frame_stride,
                                                 self.num_frames_per_load)
        self.list_cumulative_num_frames = numpy.cumsum(self.list_num_frames)
        self.total_num_frames = self.get_total_num_frames()
        self.maxFrameId = self.total_num_frames - 1

    def _count_frames(self, filename_list, topology_filename, frame_stride, n_frames_per_load=1):
        """
        Count number of frames in each trajectory file from filename_list

        :param filename_list: a list of file names
        :param topology_filename: name of the topology file
        :param frame_stride: stride size for reading the trajectory frames
        :param n_frames_per_load: number of frames per iteration when loading the trajectory
        :return: an array containing the number of frames for each trajectory
        """
        list_num_frames = []
        for _filename in filename_list:
            traj_iter = MDTraj.iterload(_filename, chunk=n_frames_per_load, top=topology_filename, stride=frame_stride)
            n_frames = 0
            for _traj in traj_iter:
                n = _traj.n_frames
                n_frames += n
            list_num_frames.append(n_frames)
        # make intermediate trajectory objects available for garbage collection
        traj_iter = None
        _traj = None

        return list_num_frames

    def get_num_files(self):
        """
        .. py:method:: TrajectorySeriesInfoMixin.get_num_files()

            Get the total number of trajectory files

            :return: number of files
        """
        return self.total_num_trajectories

    def get_total_num_frames(self):
        """
        .. py:method:: TrajectorySeriesInfoMixin.get_total_frames()

            Get the total number of frames

            :return: number of frames in total
        """
        return self.list_cumulative_num_frames[-1]

    def get_list_num_frames(self):
        """
        .. py:method:: TrajectorySeriesInfoMixin.get_n_frames_list()

            Get a list of number of frames for each trajectory files

            :return: list of numbers (int)
        """
        return self.list_num_frames

    def get_filename_and_frameId(self, frameId):
        """
        .. py:method:: TrajectorySeriesInfoMixin.get_filename_and_frameId(cumulative_frameId)

            Return the trajectory file name from the client's list of trajectories that corresponds to a
            cumulative frame ID. Also return the frame ID in that trajectory corresponding to the
            given cumulative frame ID.

            :param frameId: user requested index (zero-based indexing) corresponding to the global frame ID
            :return: a tuple (file_name, frameId_in_this_trajectory)
            :raise: UserWaring if input *cumulative_frameId < 0* or *cumulative_frameId* is not an integer
        """
        # check whether frameId exceeds lower bound
        if frameId < 0:
            msg = "ERROR HINT (from TrajectorySeriesInfoMixin): input for .get_filename_and_frameId() "
            msg += "must be a positive. \n"
            msg += "Your input is {0}".format(frameId)
            raise UserWarning(msg)

        # check whether frameId exceeds higher bound
        if frameId > self.maxFrameId:
            msg = "ERROR HINT (from TrajectorySeriesInfoMixin): input for .get_filename_and_frameId() "
            msg += "must be less or equal to {0} (the max of frame IDs).\n" .format(self.maxFrameId)
            msg += "Your input is {0}".format(frameId)
            raise UserWarning(msg)

        # check whether frameId is an integer
        if not isinstance(frameId, int):
            msg = "ERROR HINT (from TrajectorySeriesInfoMixin): input for .get_filename_and_frameId() "
            msg += "must be an integer. \n"
            msg += "Your input is {0}".format(frameId)
            raise UserWarning(msg)

        ## find out which trajectory that this frameId belongs to
        for i in range(0, self.total_num_trajectories):
            # (1). define an offset in order to calculate lower bound
            if i == 0:
                offset = self.list_cumulative_num_frames[0]
            else:
                offset = self.list_cumulative_num_frames[i-1]

            ## (2). define lower and higher bound of the frame ID range
            #  for the current trajectory file[i]
            high_bound = self.list_cumulative_num_frames[i]
            low_bound = high_bound - offset

            ## if frameId is found to be within the current trajectory file, then return
            if frameId >= low_bound and frameId < high_bound:
                local_frameId = frameId - low_bound
                traj_filename = self.filename_list[i]
                return (traj_filename, local_frameId)
            else:
                continue # move on to the next trajectory file

        # The thread should never reach here.
        # Since the initial safety checks ensures the frameId must be within reach of
        # of the given trajectory series.
        # I included return "False" just to make it easier to read the code logic
        return False

    @classmethod
    def getPublicMethods(cls):
        """
        .. py:classmethod:: .get_public_methods()

            Return a list of public methods for class

            :return: list of method names (str)
        """
        raw_list = dir(cls)
        public_method_list = [name for name in raw_list if not re.match(r'^_.*', name)]
        return public_method_list

    def get_public_methods(self):
        """
        .. py:method:: .get_public_methods()

            Return a list of public methods for class :py:class:`.TrajectorySeriesInfoMixin`

            :return: list of method names (str)
        """
        return self.__class__.getPublicMethods()
