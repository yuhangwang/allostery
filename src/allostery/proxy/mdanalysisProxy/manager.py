# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-17-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import numpy
import MDAnalysis
import prody

from allostery.proxy.mdanalysisProxy.selectorMixin import SelectorMixin
from allostery.proxy.mdanalysisProxy.frameMixin import FrameMixin


#---------------------------------------------------------------------------
#                 [[[ Proxy for MDAnalysis ]]]
#---------------------------------------------------------------------------
class Universe(object):
    """
    .. py:class:: Proxy_MDAnalysis(psf_filename :  str, trajectory_filename : str | list)

        Title: A particular type of Python module for creating user data object
        Purpose: act as a proxy for using "MDAnalysis" module
        Friend: MDAnalysis module (so Proxy_MDAnslysis is expected to
                know everything about MDAnalysis module)
        developer note: Proxy_MDAnalysis should be the only class that interacts with MDAnalysis module

        :param str psf_filename: name of the topology file
        :param str | list trajectory_filename: name or list of trajectory file names
    """

    def __init__(self, psf_filename, trajectory_filename):
        """
        Role: create an universe object based on user's files
        """
        self.psf_filename = psf_filename
        self.trajectory_filename = trajectory_filename
        self.userData = MDAnalysis.Universe(psf_filename, trajectory_filename)
        self.helperUserData =  prody.parsePSF(self.psf_filename)

        self.selectorMixin = SelectorMixin(self.userData, self.helperUserData)
        self.frameMixin    = FrameMixin(self.userData)

        self.public_method_register = ['select_atoms']

        self.attr_frameMixin = numpy.array(dir(self.frameMixin), dtype=numpy.str_)
        self.public_method_register += list(self.attr_frameMixin)

        self.attr_selectorMixin = numpy.array(dir(self.selectorMixin), dtype=numpy.str_)
        self.public_method_register += list(self.attr_selectorMixin)

    # re-route method calls to its delegates
    def __getattr__(self, method_name):
        """
        Role: reroute method calls to MDAnalysis' delegates
        :param item: string (i.e. method name)
        """
        def _method_delegation(*args, **kwargs):
            if method_name in self.attr_frameMixin:
                return getattr(self.frameMixin, method_name)(*args, **kwargs)
            elif method_name in self.attr_selectorMixin:
                return getattr(self.selectorMixin,method_name)(*args,**kwargs)
            else:
                msg = "Error Hint: method named \"{0}\" not found by class Proxy_MDAnalysis. Typo?".format(method_name)
                raise UserWarning(msg)

        return _method_delegation

    def get_public_method_list(self):
        """
        Role: return a list all public methods offered by :class Proxy_MDAnalyhsis
        :return: list
        """
        output = self.public_method_register
        return numpy.array(output,dtype=numpy.str_)

    def get_dependency(self):
        """
        Role: get a list of classes (str) that this class depends on
            If no dependencies, return None.
        """
        return None

    def __dir__(self):
        return list(self.get_public_method_list())

