"""
Provide host class with the ability to make atom selections using "mdanalysis" style
"""
#============================================================
# Compatibility with python 3
from __future__ import print_function, division
#============================================================

#========================================================================
#                   [[[ SelectorMixin ]]]
#========================================================================
import re
import numpy
from proxy.mdanalysisProxy.selectorGetterAddon import SelectorGetterAddon
from proxy.mdanalysisProxy.builderMixin import BuilderMixin


#========================================================================
#                   [[[ SelectorMixin ]]]
#========================================================================
class SelectorMixin(object):
    """
    .. py:class:: SelectorMixin(userDataObj)

        Role: provide host class with the ability to make atom selections

        :param pyobject userDataObj: an :py:class:`MDAnalysis.AtomGroup` instance
    """

    def __init__(self, userDataObj_0, userDataObj_1, *args, **kwargs):
        """
        :param pyobject userDataObj_0: MDAnalysis instance;
        :param pyobject userDataObj_1: ProDy AtomGroup instance
        """
        self.userData = userDataObj_0
        self.builderMixin = BuilderMixin()
        self.helperUserData = userDataObj_1
        self.selection_string = "all"
        self.helper_selection_string = "all"
        self.__select((self.selection_string, self.helper_selection_string))
        self._attr_dict = self.get_attr_dict()
        self.getterAddon = SelectorGetterAddon(self._attr_dict)

    # for method re-routing
    def __getattr__(self, method_name):
        def _method_delegation(*args,**kwargs):
            if re.match(r'^get_.*',method_name):
                # case 1: client is requesting "get_*) method
                try: # if the method_name is legal, ask the delegate (nicely) to fullfil the request
                    return getattr(self.getterAddon, method_name)()
                except AttributeError:
                    msg = "Error Hint: the getter method you requested was not found. You might have made a typo."
                    raise UserWarning(msg)
            else:
                msg = "Error Hint: SelectorMixin class has no method named \"{0}\".".format(method_name)
        return _method_delegation

    def update_atom_selection(self, selection_string_tuple):
        """
        .. py:method:: core.proxy_mdanalysis.SelectorMixin.select_atoms(selection_string_tuple : tuple)

            Role: make|update atom selections
            :param tuple selection_string_tuple: a tuple of atom selection strings,
             e.g. ("resid 41 and not H*", "resid 41 and not hydrogen)
             The first is used by a :py:class:`MDAnalysis.AtomGroup` instance,
             and the second is used by a :py:prody.AtomGroup: instance
        """
        self.selection_string = selection_string_tuple[0]
        self.helper_selection_string = selection_string_tuple[1]
        self.__select((self.selection_string, self.helper_selection_string))
        self.getterAddon.update(self.get_attr_dict())

    def get_attr_dict(self):
        """
        .. py:method:: SelectorMixin.get_attr_dict()

            Role: get a dictionary of :py:class:`.SelectorMixin`'s data attributes
            :return: dictionary of data attributes
                example:
                dict(selected_atoms=self.selected_atoms,
                               selected_atom_masses=self.selected_atom_masses,
                               selected_residue_masses=self.selected_residue_masses,
                               selected_residue_index_range=self.selected_residue_index_range,
                               number_of_selected_residues=self.number_of_selected_residues,
                               selection_string=self.selection_string,
                               mass_weighting_vector=self.mass_weighting_vector)
            :rtype: dict
        """
        return dict(
            selected_atoms=self.selected_atoms,
            center_of_mass_operator=self.center_of_mass_operator,
            selected_residue_masses=self.selected_residue_masses,
            selected_atom_masses=self.selected_atom_masses,
            selected_residue_index_range=self.selected_residue_index_range,
            number_of_selected_residues=self.number_of_selected_residues,
            selection_string=self.selection_string,
            mass_weighting_vector=self.mass_weighting_vector,
            )

    def __select(self, selection_string_tuple):
        """
        .. py:method:: core.proxy_mdanalysis.SelectorMixin.__select(selection_string_tuple : tuple)

            Role: select part of the MD system

            :return: a data object that should know the coordinates of selected atoms
        """
        self.selection_string = selection_string_tuple[0]
        self.helper_selection_string = selection_string_tuple[1]
        self.selected_atoms = self.userData.selectAtoms(self.selection_string)
        self.selected_resids = self.selected_atoms.resids()
        self.helper_selected_atoms = self.helperUserData.update_selection(self.helper_selection_string)

        if self.selected_atoms.numberOfAtoms() == 0:
            msg = "atom selection [{0}] yields zero atoms".format(selection_string_tuple)
            raise UserWarning(msg)
        else:
            self.selected_atom_masses = self.selected_atoms.masses()
            self.number_of_selected_residues = self.selected_atoms.numberOfResidues()
            self.number_of_selected_atoms = len(self.selected_atom_masses)
            # developer note:
            # self.helper_selected_atoms must be converted to HierView() object
            # using .getHierView() in order to have .iterResidues() attribute
            self.selected_residue_index_range = \
                self.calc_residue_index_range(self.number_of_selected_residues,
                                              self.helper_selected_atoms.getHierView().iterResidues())

            self.mass_weighting_vector, self.selected_residue_masses = self.builderMixin.build_mass_weighting_vector(
                self.selected_atom_masses,
                self.selected_residue_index_range)

            # center of mass kernel
            # usage: to get center of mass coordinates: self.center_of_mass_operator*self.selected_coordinates
            self.center_of_mass_operator = self.builderMixin.build_center_of_mass_operator(self.selected_residue_index_range,
                                                                                           self.number_of_selected_atoms,
                                                                                           self.number_of_selected_residues,
                                                                                           self.mass_weighting_vector)
            return self.selected_atoms

    def calc_residue_index_range(self, numberResidues, residueIterator):
        """
        .. py:method:: SelectorMixin.calc_residue_index_range(numberResidues, residueIterator)

            Role: build an array to store the range of indices
                  for each residues in the current atom selection
            :param int numberResidues: number of selected residues
            :param residueIterator: an iterator object that yields a Residue() instance object
            :return: a 2D array of starting/ending indices for each selected residues
            :rtype: numpy.array dtype=numpy.int
            example output: [[0,5],
                             [6,11]]
        """
        output = numpy.zeros((numberResidues,2), dtype=numpy.int)
        ccc = 0 # counter for number of residues
        _id_end = 0 # starting value in order to make _id_begin = _id_end work

        for _residue in residueIterator:
            _numAtoms = _residue.numAtoms()
            _id_begin = _id_end
            _id_end   = _id_begin + _numAtoms
            output[ccc, 0] = _id_begin
            output[ccc, 1] = _id_end
            ccc += 1
        return output

    def __dir__(self):
        """
        Role: define public accessible methods
        :return: numpy list
        """
        output = dir(SelectorMixin)
        output += dir(SelectorGetterAddon)
        return [name for name in output if not re.match(r'^_.*', name)]

