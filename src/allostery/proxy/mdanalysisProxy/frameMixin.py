"""
Provide class FrameMixin to deal with trajectory frames using MDAnalysis package
"""
#============================================================
# Compatibility with python 3
from __future__ import print_function, division
#============================================================


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-17-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#========================================================================
import re
#========================================================================

#========================================================================
#                   [[[ FrameMixin ]]]
#========================================================================
class FrameMixin(object):
    """
    .. py:class:: FrameMixin(userDataObj)

        Role: provide the host with handy gadgets.

        :param userDataObj: an instance of some class with the following methods:
                ".trajectory[#]" which change the current trajectory frame to a new one
    """

    def __init__(self, userDataObj):
        """
        dummy __init__()
        :return: None
        """
        self.userData = userDataObj
        self.total_numFrames = self.userData.trajectory.numframes
        self.current_frameId = 0

    def __dir__(self):
        """
        Role: define public accessible methods
        :return: numpy list
        """
        output = dir(FrameMixin)
        return [name for name in output if not re.match(r'^_.*', name)]

    def get_current_frame_id(self,*args,**kwargs):
        """
        Role: get the current frame ID
        """
        return self.current_frameId

    def get_total_number_of_frames(self,*args,**kwargs):
        """
        Role: get the total number of frames
            in the current trajectory
        """
        return self.total_numFrames

    def goto_next_frame(self,*args,**kwargs):
        """
        Role: move on to the next trajectory frame
        :return None
        """
        if self.current_frameId + 1 < self.total_numFrames:
            self.current_frameId += 1
            self.userData.trajectory[self.current_frameId]
        else:
            msg = "Already reached the last frame!"
            msg += "(current frameId = {0}/{1})".format(self.current_frameId, self.total_numFrames)
            raise UserWarning(msg)

    def goto_frame(self, frameId,*args,**kwargs):
        """
        Role: go to a specific trajectory frame
        :param frameId: Int scalar
        :return: None
        """
        if frameId < 0 or frameId >= self.total_numFrames:
            msg = "frameId must be within [0,{0})".format(self.total_numFrames)
            msg += ", but you specified frameId = {0}".format(frameId)
            raise UserWarning(msg)
        self.userData.trajectory[frameId]