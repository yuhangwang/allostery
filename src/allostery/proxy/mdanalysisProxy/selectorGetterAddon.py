"""
Provide class SelectorGetterAddon to define all getter functions
for class SelectorMixin
"""
#============================================================
# Compatibility with python 3
from __future__ import print_function, division
#============================================================


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '01-31-2015'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


class SelectorGetterAddon(object):
    """
    .. py:class:: SelectorGetterAddon

        Role: an auxiliary class for :class proxy.mdanalysisProxy.SelectorMixin.
            It provides many useful getter's.
    """
    def __init__(self, attr_dict):
        self.update(attr_dict)
        self.atom_selection_not_found_msg = "Please make an atom selection first, then call '.get_selected_residue_range()'"


    def update(self, attr_dict):
        """
        .. py::method: SelectorGetterAddon.update(attr_dict)

            Role: update to the latest atom selection

            :param dict attr_dict: a dictionary of
            :py:class:`.SelectorMixin` data attributes
        """
        self.selected_atoms = attr_dict['selected_atoms']
        self.center_of_mass_operator = attr_dict['center_of_mass_operator']
        self.selected_atom_masses = attr_dict['selected_atom_masses']
        self.number_of_selected_residues = attr_dict['number_of_selected_residues']
        self.number_of_selected_atoms = len(self.selected_atom_masses)
        self.selected_residue_index_range = attr_dict['selected_residue_index_range']
        self.mass_weighting_vector = attr_dict['mass_weighting_vector']
        self.selection_string = attr_dict['selection_string']
        self.selected_residue_masses = attr_dict['selected_residue_masses']

    def get_selected_residue_index_range(self):
        """
        Role: get the atom index ranges for select residues
        :return: 2D numpy array
        """
        return self.selected_residue_index_range

    def get_selected_atom_masses(self):
        """
        Role: get the masses of selected atoms
        :return: numpy array
        """
        try:
            return self.selected_atom_masses
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_mass_weighting_vector(self):
        """
        Role: get residue based mass weighting vector
            (to be used for calculating center of mass)
        :return: numpy array
        """
        return self.mass_weighting_vector

    def get_atom_selection_string(self):
        """
        Role: get atom selection string made by the user
        :return:
        """
        return self.selection_string

    def get_selected_coords(self):
        """
        Role: get the coordinates of selected atoms
        :return: numpy array
        """
        try:
            return self.selected_atoms.coordinates()
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_selected_residue_masses(self):
        """
        Role: get the masses of selected residues
        :return: numpy array
        """
        try:
            return self.selected_residue_masses
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_number_of_selected_residues(self):
        """
        Role: count the number of residues in the current atom selection
        :return scalar (number of residues)
        """
        try:
            return self.number_of_selected_residues
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_number_of_selected_atoms(self):
        """
        Role: count the number of atoms in the current atom selection
        :return scalar (number of atoms)
        """
        try:
            return self.number_of_selected_atoms
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_selected_resids(self):
        """
        Role: get the residue IDs of selected residues
        :return: numpy array (1D)
        """
        try:
            return self.selected_atoms.resids()
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_selected_resnums(self):
        """
        Role: get the canonical residue IDs of selected residues
        :return: numpy array (1D)
        """
        try:
            return self.selected_atoms.resnums()
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_selected_resnames(self):
        """
        Role: get the canonical residue names of selected residues
        :return: numpy array (1D)
        """
        try:
            return self.selected_atoms.resnames()
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_selected_segids(self):
        """
        Role: get the residue IDs of selected residues
        :return: numpy array (1D)
        """
        try:
            return self.selected_atoms.segids()
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)

    def get_selected_com_coords(self):
        """
        Role: get the center of mass matrix of selected residues
        :return: 2D numpy array of shape (N,3), where N is the number
               of residues.
        """
        try:
            return self.center_of_mass_operator*self.get_selected_coords()
        except AttributeError:
            raise UserWarning(self.atom_selection_not_found_msg)