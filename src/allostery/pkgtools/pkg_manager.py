# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================
"""
.. py:module:: class_helper

    Role: provide tools for facilitating class organization

"""
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-23-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
from  toposort import toposort_flatten

class DependencyResolver(object):
    """
    .. py:class:: DependencyResolver()

        Role: automatically add mix-in's to a host class
            A major feature: provide a sorted list of mix-in's
            and the order of which determines the order that
            these mix-in's should be registered in the host class.
    """
    def __init__(self, list_of_classes):
        """
        developer note: due to complication of using class object
            as dictionary indices, all classes need to be converted to
            numbers and then apply toposort.
        :param list_of_classes: list of class objects (not instances)
        """
        self.mixin_dict = dict()
        ccc = 0 # counter
        for _class in list_of_classes:
            self.mixin_dict[_class.__name__] = ccc
            ccc += 1

        # only use the ID's of the classes to do topo-sort
        self.dependency_dict = dict()
        for _class in list_of_classes:
            _current_class_name = _class.__name__
            _dependency_class_names = getattr(_class, 'get_dependency')()
            self.dependency_dict[self.mixin_dict[_current_class_name]] = set({})

            if _dependency_class_names is None:
                continue

            _dep_class_id_list = [self.mixin_dict[_class_name] for _class_name in _dependency_class_names]
            for _class_id in _dep_class_id_list:
                self.dependency_dict[self.mixin_dict[_current_class_name]].add(_class_id)

        sorted_class_ids = tuple(toposort_flatten(self.dependency_dict))
        self.sorted_mixin_list = [list_of_classes[_i] for _i in sorted_class_ids]

    def get_sorted(self):
        """
        Role: get a topologically sorted list of mix-in's
        :return: list
        """
        return self.sorted_mixin_list


if __name__ == "__main__":
    ## Tests ##
    import unittest
    class TestDependencyResolver(unittest.TestCase):
        def setUp(self):
            class A(object):
                @classmethod
                def get_dependency(cls):
                    return None

            class B(object):
                @classmethod
                def get_dependency(cls):
                    return {'A'}
            class C(object):
                @classmethod
                def get_dependency(cls):
                    return {'A','B', 'E'}
            class D(object):
                @classmethod
                def get_dependency(cls):
                    return {'A'}
            class E(object):
                @classmethod
                def get_dependency(cls):
                    return {'D'}
            self.to_be_registered = [A, B, C, D, E]

        def test1(self):
            reg = DependencyResolver(self.to_be_registered)
            sorted_list = reg.get_sorted()

            result = [obj.__name__ for obj in sorted_list]
            expected = ['A', 'B', 'D', 'E', 'C']
            self.assertTrue(result==expected)

    unittest.main()