#============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-03-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


class AlloKeywords(object):
    """
    Role: provide definitions for the atom selection keywords
          used by "allostery" module
    """

    ALL = "all"
    AND = "and"
    OR  = "or"

    ResID = "resid"
    SegID = "segid"

    Hydrogen = "hydrogen"