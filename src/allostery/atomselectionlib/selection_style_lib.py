# ============================================================
# Compatibility with python 3
#============================================================
from __future__ import print_function, division
#============================================================

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-03-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
#------------------------------
# my modules
#------------------------------
from allostery.atomselectionlib.allostery_keywords import AlloKeywords
#------------------------------


class MDAnalysisSelectionStyle(object):
    """
    .. py:class:: MDAnalysisSelectionStyle()

        Role: create an atom selection string that matches the atom selection style of :py:mod:`MDAnalysis` module

        usage: style_dict = MDAnalysisSelectionStyle.keyword_dict()
                which defines the keyword mapping dictionary
                from :py:class:`~allostery.atomselectionlib.allo_style_keywords.AlloKeywords` selection style
                to MDAnalysis selection style.
    """

    keyword_conversion_dict = dict({AlloKeywords.ResID      : "resid",
                                    AlloKeywords.SegID      : "segid",
                                    AlloKeywords.Hydrogen   : "name H*",
                                  })

    @classmethod
    def keyword_dict(cls):
        """
        .. :py:classsmethod: MDAnalysisSelectionStyle.keyword_dict()

            usage: style_dict = MDAnalysisSelectionStyle.keyword_dict()

            :return: a dictionary for mapping from
                    :py:class:`~allostery.atomselectionlib.allo_style_keywords.AlloKeywords` selection style
                    to :py:mod:`MDAnalysis` style.
            :rtype: dict
        """
        return MDAnalysisSelectionStyle.keyword_conversion_dict


class ProxyMDAnalysisSelectionStyle(object):
    """
    Role: provide a mapping from Allostery style selection keywords
         to MDAnalysis style selection keywords.
    """

    def __init__(self):
        self.binary = { AlloKeywords.AND : "and",
                        AlloKeywords.OR : "or",
                      }

        self.unary = { AlloKeywords.ResID : "resid",
                       AlloKeywords.SegID : "segid",
                     }


class ProdySelectionStyle(object):
    """
    .. py:class:: ProdySelectionStyle()

        Role: create an atom selection string that matches the atom selection style of :py:mod:`prody` module

        usage: style_dict = ProdySelectionStyle.keyword_dict()
            which defines a keyword mapping dictionary from
            :py:class:`~allostery.atomselectionlib.allo_style_keywords.AlloKeywords`
            to ProDy atom selection style.
    """

    keyword_conversion_dict = dict({AlloKeywords.ResID      : "resid",
                                    AlloKeywords.SegID      : "segment",
                                    AlloKeywords.Hydrogen   : "hydrogen",
                                  })

    @classmethod
    def keyword_dict(cls):
        """
        .. :py:classsmethod: Prody.keyword_dict()

            usage: style_dict = ProdySelectionStyle.keyword_dict()

            :return: a dictionary for mapping from
                    :py:class:`~allostery.atomselectionlib.allo_style_keywords.AlloKeywords` selection style
                    to :py:mod:`prody` style.
            :rtype: dict
        """
        return ProdySelectionStyle.keyword_conversion_dict