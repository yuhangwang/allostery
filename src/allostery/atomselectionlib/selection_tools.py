# ============================================================
# Compatibility with python 3
# ============================================================
from __future__ import print_function, division, absolute_import
#============================================================

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '10-03-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import re
#------------------------------
# my modules
#------------------------------

#------------------------------



#============================================================================
#               [[[Interface: make atom selections]]]
#============================================================================
class SelectionStringMaker(object):
    """
    .. py:class:: SelectionParser(extra_selection_criteria : str,
                    alloStyle_selection_keyword_list : list,
                    selection_member_lists : list,
                    keyword_conversion_dict : dict)

        Role: an interface for make atom selections, which delegate other
            classes to do the job, in order to match the style
            of different user data object creation modules (like :mod:'MDAnalysis')
    """
    def __init__(self,
                 base_selection_criteria,
                 alloStyle_selection_keyword_list,
                 selection_member_lists,
                 keyword_conversion_dict):
        """
        :param str base_selection_criteria: extra selection criteria string
        :param list alloStyle_selection_keyword_list: example: ['resid', 'segid']
        :param list selection_member_lists list: a list of lists of members, i.e. [[101, 102, 103],['A','B']]
        :param dict keyword_conversion_dict: a python dictionary for converting
                    :class:`Allostery` style to target style keywords (e.g. MDAnalysis style)
        """

        # replace Allo-style selection keywords with target style
        for _key, _val in keyword_conversion_dict.iteritems():
            base_selection_criteria = re.sub(_key, _val, base_selection_criteria)
            alloStyle_selection_keyword_list = [re.sub(_key,_val,x) for x in alloStyle_selection_keyword_list ]

        self.base_selection_criteria = base_selection_criteria
        self.alloStyle_selection_keyword_list = alloStyle_selection_keyword_list
        self.selection_member_lists = selection_member_lists
        self.keyword_conversion_dict = keyword_conversion_dict
        self.selection_string = self.__make(self.alloStyle_selection_keyword_list,
                                              self.selection_member_lists,
                                              self.base_selection_criteria)

    def __make(self, keyword_list, member_list_of_lists, extra_criteria=None):
        """
        .. py:method:: SelectionStringMaker.__create(keyword : str, member_list : list, extra_criteria=None : str)

            Role: make an atom selection string according to the [[MDAnalysis]] module style

            :return: a string like "((resid 1) and (resid 2) and (resid 3))"
        """
        if extra_criteria is None:
            selection_terms = []  # list to store intermediate atom selections
        else:
            selection_terms = ["({0})".format(extra_criteria)]  # list to store intermediate atom selections

        for _keyword, _member_list in zip(keyword_list, member_list_of_lists):
            selection_terms.append(self.__make_sub_selection(_keyword, _member_list))

        return "{0}".format(" and ".join(selection_terms))

    def __make_sub_selection(self, keyword, member_list):
        """
        .. py:method:: SelectionStringMaker.__make_sub_selection(keyword : str, member_list : list)

            Make an atom selection string using the given keyword and member_list

            :param str keyword: example: "segid"
            :param list member_list: example: [1, 2, 3]
            :return: an atom selection string, e.g. "(segid 1 or segid 2)"
        """
        selection_terms = []
        for member in member_list:
            selection_terms.append("{0} {1}".format(keyword, member))
        return "({0})".format(" or ".join(selection_terms))

    def get_selection_string(self):
        """
        .. py:method:: SelectionStringMaker.get_selection_string()

            get the atom selection string

            :return: an atom selection string conforming to the target style
        """
        return self.selection_string

    def __dir__(self):
        """
        Role: define public accessible methods
        :return: numpy list
        """
        output = dir(SelectionStringMaker)
        return [name for name in output if not re.match(r'^_.*', name)]


#============================================================================
#               [[[Selection operators]]]
#       (helper classes for selection string parsing)
#============================================================================
class BinaryOperator(object):
    """
    Role: provide a base class for all binary operators
    """
    def __init__(self):
        self.precedence = 0
        self.operator = ''

    def __call__(self, operand1, operand2):
        return "({1} {0} {2})".format(self.operator, operand1, operand2)


class UnaryOperator(object):
    """
    Role: provide a base class for unary operators
    """
    def __init__(self):
        self.precedence = 0
        self.operator = ''

    def __call__(self, operand):
        return "({0} {1})".format(self.operator, operand)


class And(BinaryOperator):
    """
    Role: AND operation
    """
    def __init__(self, and_operator):
        self.precedence = 1
        self.operator = and_operator







