"""
.. py:module:: warn_color

    Provide a class :py:class`.TerminalColor` for adding colors to the error/warning messages
    to be printed on the terminal
"""

class TerminalColor(object):
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END_COLORING = '\033[0m'

    @staticmethod
    def give_warning(msg):
        """
        .. py:method:: TerminalColor.give_warning(msg)

            return a a string will color coding
            :param msg: string
            :return: color encoded string
        """
        return "{0}{1}{2}".format(TerminalColor.RED, msg, TerminalColor.END_COLORING)