"""
.. py:module:: algorithm.nearest_neighbors

    Role: calculate the nearest neighbors related quantities
"""
#=============================================================
# Compatibility with python 3
from __future__ import print_function, division
#============================================================

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
__author__ = 'Yuhang Wang'
__date__ = '11-30-2014'
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#============================================================
# External Module Dependencies
#============================================================
import numpy
import re
import numpy.linalg as linear_algebra
from scipy.spatial import distance as distance_kit
from scipy import spatial as spatial
#------------------------------
# `allostery` package modules
#------------------------------
from allostery.algorithms._dependency import PluginsForNearestNeighborMixin
from allostery.pkgtools.pkg_manager import DependencyResolver

#----------------------
#  aliases
#----------------------
pdist_function = distance_kit.pdist
norm_function = linear_algebra.norm
square_matrix_form = distance_kit.squareform
matrix_sort = numpy.sort # sort matrix along rows/columns
matrix_argsort = numpy.argsort # return the indices that will sort the input
KDTree = spatial.cKDTree

#========================================================================
#                   [[[ CorrelationCoefficientMixin ]]]
#========================================================================
class NearestNeighborMixin(object):
    """
    .. py:class:: NearestNeighborMixin
        Goal: compute nearest-neighbor related quantities,
            including the number of data points within a cutoff
    """

    def __init__(self):
        self.delegate_dict = dict()
        self.attr_delegate_dict = dict()
        self.attr_self_builtin = [name for name in dir(self.__class__) if not re.match(r'^_.*', name)]
        self.public_method_register = self.attr_self_builtin

        self._add_delegates(PluginsForNearestNeighborMixin.mixin_list)
        self.accepted_norm_types = ['max']
        self.tol = 1E-15 # tolerance for comparing floating point numbers

    def batch_count_number_of_nearest_neighbors(self, X, cutoffs, method="simple", norm_type='max'):
        """
        .. py:method:: NearestNeighborMixin.iter_count_within_cutoff(X_matrix,cutoff_array)
            Role: count the number of data points within
                cutoff using each data point in X_matrix as the center
            :param X: a 2D matrix
            :param cutoffs: cutoff distance array
            :param norm_type: type of the norm (default: 'max')
                    this has same options as in numpy.norm
            :param method: options: "simple", "kdtree".
                    If choosing "simple", then pairwise distances based approach will be used.
                    If choosing "kdtree", then kDTree algorithm will be used.
            :return: an array of integers, which are the number
                    of nearest neighbors within the cutoff
            :raise: error when cutoff_array is not a 2D array
            :raise: error when cutoff_array does not have shape [m,1]
            :raise: error when method is not recognized.
        """
        N_points = len(cutoffs)
        n_rows, n_cols = numpy.shape(X)

        ## Saftey measure
        if N_points != n_rows:
            msg = "ERROR HINT: number of rows in input X should equal that of  " \
                  "the cutoffs.\n"
            msg += "number of rows in X: {0} \n".format(n_rows)
            msg += "number of rows in cutoffs: {0}\n".format(N_points)
            raise UserWarning(msg)

        ## Do the work!
        if method == "simple":
            n_neighbors = self._simple_count_nearest_neighbors(X, cutoffs, norm_type)
        else:
            msg = "ERROR HINT: method must be one of the followings:\n"
            msg += "simple\n"
            msg += "kdtree\n"
            msg += "The method you specified was {0}".format(method)
            raise UserWarning(msg)

        return n_neighbors

    def _simple_count_nearest_neighbors(self, X, cutoffs, norm_type="max"):
        """
        .. py:method:: NearestNeighborMixin.count_within_cutoff()
            Role: count the number of data points within
                cutoff from the center point
                using simple pairwise distances (O(N^2) algorithm)
            :param: X a 2D matrix
            :param cutoffs: cutoff distance
            :return: a scalar
        """
        ## Safety measures
        if norm_type == 'max':
            norm_type_pdist = 'chebyshev'
        else:
            msg = self._error_msg_unknown_norm_type(norm_type)
            raise UserWarning(msg)

        if len(numpy.shape(cutoffs)) != 2:
            msg = "ERROR HINT: input cutoffs must be an 2D-array"
            raise UserWarning(msg)

        if numpy.shape(cutoffs)[1] != 1:
            msg = "ERROR HINT: input cutoffs must be a column vector. "
            msg += "Expecting 1 column, but got {0} column(s).".format(numpy.shape(cutoffs)[1])
            raise UserWarning(msg)

        ## compute pairwise distances
        _pairwise_distances = square_matrix_form(pdist_function(X,metric=norm_type_pdist))

        ## developer's note: this must be a strict less-than,
        #  meaning the k-th neighbor point that defines the cutoff should not be included.
        _filtered_matrix = _pairwise_distances < cutoffs

        ## compute the number of nearest-neighbors and remember to offset by 1
        #  because there is always a 0 in each row, which is the distance to the
        #  center point itself.
        offset = 1
        n_neighbors = numpy.sum(_filtered_matrix,axis=1) - offset

        return n_neighbors

    def _kdtree_count_nearest_neighbors(self, X, cutoffs, norm_type='max'):
        """
        .. py:method:: NearestNeighborMixin.count_nearest_neighbors_kdtree()
        :param X: input data array of shape (m,n)
            where m is the total number of sample points
            and n is the dimension of each point.
        :param cutoffs: an array of cutoff values
        :param norm_type: type of the norm (defulat: 'max')
        :return: an array of shape (m,1), where i-th row has the number
            of the nearest neighbors with distance to point X[i]
            strictly less than cutoffs[i]
        :raise: error if norm_type is not supported.
        """
        ## note: want kdtreeObj to be a local variable
        #   such that the object will be destroyed
        #   when exiting the function
        if norm_type == 'max':
            norm_type_kdtree = numpy.inf
        else:
            msg = self._error_msg_unknown_norm_type()
            raise UserWarning(msg)

        kdtreeObj = KDTree(X)
        n_data_points, _ = numpy.shape(X)
        n_neighbors = numpy.zeros(n_data_points, dtype=numpy.int64)  # output array

        # note: when querying the number of neighbors, the center itself is also included,
        #   which needs to be subtracted off.
        offset = 1

        for _i in range(n_data_points):
            _point = X[_i,:]
            ## we need the distances to be strictly less thant cutoff
            _cutoff = cutoffs[_i] - self.tol
            n_neighbors[_i] = len(kdtreeObj.query_ball_point(_point,_cutoff,p=norm_type_kdtree)) - offset

        return n_neighbors


    def simple_find_kNN_distance(self,array_points, k, norm_type='max'):
        """
        .. py:method:: MutalInfoMixin.find_kNN_distance(k)
            Role: find k-th nearest neighbor and
                    return the distance to it.
            Note: the distance metric is the infinity-norm
            :param array_points: an mxn array of points
                    with m points in n-dimension
            :param k: which nearest neighbor
            :param norm_type: type of the norm (defalt: 'max')
            :return: an array of kNN distances
        """
        if norm_type == 'max':
            norm_type_pdist = 'chebyshev'
        else:
            msg = self._error_msg_unknown_norm_type(norm_type)
            raise UserWarning(msg)

        ## compute pairwise distances
        _pairwise_distance_matrix = \
            square_matrix_form(pdist_function(array_points, norm_type_pdist))

        ## sort the distances
        _sorted_distance_matrix = matrix_sort(_pairwise_distance_matrix, axis=1, kind='quicksort')

        ## extract k-th nearest neighbor distances
        output_distances = _sorted_distance_matrix[:,k].reshape((-1,1))

        return output_distances

    def simple_find_kNN_distance_subdim(self,Z, k, norm_type='max',subdim=1):
        """
        .. py:method:: MutalInfoMixin.find_kNN_distance(k)
            Role: find k-th nearest neighbor and
                return the distance to it along each dimension
                Therefore, the output will be of shape [m,n]
                where "m" is the number of rows, and "n"
                is the number of dimension.
            Note: the distance metric is the infinity-norm
            :param Z: an array of points of shape (m,n)
                    with m points in n-dimension
            :param k: which nearest neighbor
            :param norm_type: type of the norm (defalt: 'max')
            :param subdim: sub-dimension (default: 1).
                This is useful when input array_points is composed of
                many data points of dimension 'subdim'.
                Example: if each row in array_points has dimension 6,
                and it is composed of two points, each of dimension 3.
            :return: an array of kNN distances of shape [m,n]
            :raise: error if the dimension (number of columns) is not
                a multiple of 'subdim'.
            :raise: error if subdim < 1
            :raise: error if norm_type is unknown
        """
        if norm_type == 'max':
            norm_type_pdist = 'chebyshev'
            norm_type_linalg = numpy.inf
        else:
            msg = self._error_msg_unknown_norm_type(norm_type)
            raise UserWarning(msg)


        ## get pairwise distances for Z
        _pairwise_distance_matrix = \
            square_matrix_form(pdist_function(Z, norm_type_pdist))

        ## get the indices for k-th neighbor using argsort
        _ids_sorted = matrix_argsort(_pairwise_distance_matrix, axis=1, kind='quicksort')
        # note: _id_kNN is a Nx1 array of indices for the k-th nearest-neighbors (for each data point)
        _id_kNN = _ids_sorted[:,k]
        _coords_kNN = Z[_id_kNN, :]

        ## compute the difference between i-th point and its k-th neighbor
        difference_array = Z - _coords_kNN

        if subdim >= 1:
            n_data_points, n_dim = numpy.shape(Z)
            if n_dim%subdim != 0:
                msg = "ERROR HINT: input data array must be a multiple of subdim.\n"
                msg += "your input data array has dimension {0}\n".format(n_dim)
                msg += "but you specified subdim = {0}".format(subdim)
                raise UserWarning(msg)
            ## compute the distance along each sub-dimension
            n_subdims = n_dim//subdim
            output_distances_multi = numpy.zeros((n_data_points, n_subdims))
            for _i in range(n_subdims):
                _id_one = _i*subdim
                _id_end = (_i+1)*subdim
                output_distances_multi[:,_i] = \
                    linear_algebra.norm(difference_array[:,_id_one:_id_end], ord=norm_type_linalg, axis=1)
        else:
            msg = "ERROR HINT: subdim must be >= 1 (got subdim={0})".format(subdim)
            raise UserWarning(msg)
        return output_distances_multi

    def kdtree_find_kNN_distance(self,Z,k,norm_type, subdim=None):
        """
        .. py:method::NearestNeighborMixin.kdtree_find_kNN_distance(X,k,norm_type)
            Role: find k-th nearest neighbor and
                    return the distance to it using KDTree algorithm
            Note: the distance metric is the infinity-norm
            :param Z: an mxn array of points
                    with m points in n-dimension
            :param k: which nearest neighbor
            :param norm_type: type of the norm (default: 'max')
            :param subdim: dimension of the sub-component of Z
                for example: if Z = [X;Y]
                then subdim = dimension(X) = dimension(Y)
                Default: subdim=None
                If subdim >= 1, then the returned array has
                shape (m,n) where m is the number of data points
                and n=dimension(Z)%subdim
                Note: this method assume Z's sub-components
                 (e.g. X and Y) have the same dimension!
            :return: an array of kNN distances
            :raise: error if norm_type is not supported
        """
        if norm_type == 'max':
            norm_type_kdtree = numpy.inf
            norm_type_linalg = numpy.inf
        else:
            msg = self._error_msg_unknown_norm_type()
            raise UserWarning(msg)

        kdtreeObj = KDTree(Z)
        ## note: in scipy.spatial.cKDTree implementation,
        #   keyword argument 'k=1' actually refers to the points itself,
        #   which is always 0.
        #   Therefore we to offset it by 1 in order to match
        #   the conventional definition of k and scipy's definition
        kdtree_kNN_offset = 1

        # get distances and indices for all neighbors with order <= k
        distances_all_kNN, indices_all_kNN = kdtreeObj.query(Z,k=k+kdtree_kNN_offset,p=norm_type_kdtree)

        # get the k-th neighbor distances
        # note: since python use 0-based indexing, we can directly use 'k'
        #   to get information of k-th neighbor
        if subdim is None:
            distances_kth_neighbor = distances_all_kNN[:,k].reshape((-1,1))
        else:
            indices_kth_neighbor = indices_all_kNN[:,k]
            n_data_points, dim_of_Z = numpy.shape(Z)
            n_subdims = dim_of_Z//subdim # number of sub-dimensions

            distances_kth_neighbor = numpy.zeros((n_data_points,n_subdims))
            difference_vectors = Z - Z[indices_kth_neighbor,:]
            for _i in range(n_subdims):
                _id_one = _i*subdim
                _id_end = (_i+1)*subdim
                distances_kth_neighbor[:,_i] = \
                    linear_algebra.norm(difference_vectors[:,_id_one:_id_end], ord=norm_type_linalg, axis=1)

        return distances_kth_neighbor


    def _error_msg_unknown_norm_type(self, unknown_norm_type):
        """
        Role: return an msg about the unknown norm type
        :return: msg (a string)
        """
        msg = "ERROR HINT: you have specified an unknown norm type: \"{0}\"".format(unknown_norm_type)
        msg += "\t Please choose one of the following norm types:\n"
        for _norm_type in self.accepted_norm_types:
            msg += "{0}\n".format(msg)
        return msg

    def _add_delegates(self, list_of_mixins):
        """
        Define axis property refinement delegates
        """
        for _class in DependencyResolver(list_of_mixins).get_sorted():
            _class_name = _class.__name__
            self.delegate_dict[_class_name] = _class(**self.delegate_dict)
            self.attr_delegate_dict[_class_name] = dir(_class)

        for _delegate in self.attr_delegate_dict:
            self.public_method_register += self.attr_delegate_dict[_delegate]

