.. allostery documentation master file, created by
   sphinx-quickstart on Mon Oct  6 23:47:36 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to allostery's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 10

   allostery


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

