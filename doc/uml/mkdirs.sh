#!/bin/sh

file_formats=(pdf svg png)
new_dirs=(allostery factory iolib plotting atomselectionlib calc)
for mydir in ${file_formats[@]} ; do
    for newdir in ${new_dirs[@]} ; do
	if [ ! -e $newdir ] ; then
	    mkdir -p $mydir/$newdir
	fi
    done
done
