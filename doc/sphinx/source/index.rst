.. allostery documentation master file, created by
   sphinx-quickstart on Mon Oct  6 23:21:59 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to allostery's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: allostery.allo
.. autoclass:: allostery.allo.Allostery

.. automodule:: allostery
    :members:
    :show-inheritance


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

