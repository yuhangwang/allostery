"""
Try out a new way to automatically add getter for data attributes
"""

class A:
    def __init__(self):
        self.att_list = ['a', 'b', 'c']
        for i in range(len(self.att_list)):
            _name_attr = self.att_list[i]
            setattr(self, _name_attr, i)
            self._auto_add_attr_getter(_name_attr)

    def _auto_add_attr_getter(self, name_attr):
        """
        Automatically add a getter for a data attribute
        :param name_attr: name of the data attribute
        # """
        # def tmp(self=self, name=name_attr):
        #     return getattr(self, name)
        setattr(self, 'get_{0}'.format(name_attr), lambda: getattr(self, name_attr))
        return


if __name__ == '__main__':
    print("Play ...")
    a = A()

    for name_attr in a.att_list:
        print("a.get_{0}() = {1}".format(name_attr, getattr(a, name_attr)))

    a.a = 1001
    for name_attr in a.att_list:
        print("a.get_{0}() = {1}".format(name_attr, getattr(a, name_attr)))

    s1 = set([0,1,2])
    s1.add(3)
    print(s1)
