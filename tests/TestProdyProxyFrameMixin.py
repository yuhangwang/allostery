"""
Test the :py:class:`allostery.proxy.mdtrajProxy.FrameMixin`
"""
from __future__ import print_function, division

__author__ = 'Yuhang Wang'
__date__ = '02-02-2015'


#---------------------------------------------------
import unittest
import os
import numpy
from allostery.proxy.prodyProxy.frameMixin import FrameMixin
#---------------------------------------------------


class TestProdyProxyFrameMixin(unittest.TestCase):
    """
    .. py:class:: TestMdtrajProxyFrameMixin

        Test the module :py:mod:`allostery.proxy.prodyProxy.frameMixin.FrameMixin`
    """
    def setUp(self):
        data_dir = "data"
        self.data_dir = os.path.realpath(data_dir)

        psf_filename = 'segA3.psf'
        dcd_filename = 'segA3_md1.dcd'
        dcd2_filename = 'segA3_md1.dcd'
        ref_coord_npy = 'segA3_md1.npy'
        frame_stride = 1
        self.tol = 1E-7

        topology_filename = os.path.join(self.data_dir, psf_filename)
        trajectory_filename = os.path.join(self.data_dir, dcd_filename)
        trajectory2_filename = os.path.join(self.data_dir, dcd2_filename)
        ref_coord_npy = os.path.join(self.data_dir, ref_coord_npy)
        self.ref_trajectory = numpy.load(ref_coord_npy)

        traj_list = [trajectory_filename]

        self.chosen_atomIds = [0]
        self.atom_selection_str = 'index {0}'.format(" ".join([str(i) for i in self.chosen_atomIds]))

        self.frameObj = FrameMixin(traj_list,topology_filename,frame_stride=frame_stride)

    def tearDown(self):
        pass

    # def test_getPublicMethods(self):
    #     """
    #     Test .getPublicMethods()
    #     """
    #     print("\n All Public Methods \n")
    #     for _method in FrameMixin.getPublicMethods():
    #         print(_method)

    def est_getCoreMethods(self):
        """
        Test .getCoreMethods()
        """
        print("\n Core Methods \n")
        for _method in FrameMixin.getCoreMethods():
            print(_method)

    def est_get_selected_coords(self):
        """
        Test all getter functions
        """
        print("\n Test get_selected_coords()\n")
        self.frameObj.make_selection(self.atom_selection_str)
        coords = self.frameObj.get_selected_coords()
        print("shape of selected coords: {0}".format(coords.shape))
        print("selected coordinates:\n {0}".format(coords))

    def est_get_selected_com_coords(self):
        """
        Test all getter functions
        """
        print("\n Test get_selected_com_coords()\n")
        self.frameObj.make_selection(self.atom_selection_str)
        atomId_ranges = self.frameObj.get_atomId_range_per_selected_residue()
        print("atomId ranges: {0}".format(atomId_ranges))

        coords = self.frameObj.get_selected_com_coords()
        print("shape of selected com coords: {0}".format(coords.shape))
        print("selected  com coordinates:\n {0}".format(coords))

    def test_goto_frame(self):
        print("\n Test go to frame")
        self.frameObj.make_selection(self.atom_selection_str)
        totalFrames = self.frameObj.get_total_number_of_frames()
        print("reference trajectory: ")
        for frameId in range(0,totalFrames):
            self.frameObj.goto_frame(frameId)
            coords = self.frameObj.get_selected_coords()
            expected_coords = self.ref_trajectory[frameId,self.chosen_atomIds,:]
            print("frame {0}: {1}".format(frameId, coords))
            print("ref.  {0}: {1}".format(frameId, expected_coords))
            diff = numpy.abs(coords - expected_coords)
            error = numpy.sum(diff)
            print("\t error", error)
            self.assertTrue(numpy.sum(diff)< self.tol)




