"""
Goal: test "allostery" module
Author: Yuhang Wang
Date: 2014-09-16
"""
# ============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import os
import time
import pandas

#--------------------------------------------------
# testing target  module
from allostery import allo
#============================================================


class TestAllosteryBasics(unittest.TestCase):
    def setUp(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))
        #------------------------------------------------------------
        # Parameters
        #------------------------------------------------------------
        data_dir = "data"
        self.output_dir = "output"
        self.tol = 1E-5
        self.data_dir = os.path.realpath(data_dir)

        trajectory_name = "segA3_pdb.dcd"
        dcd_filename = "segA3_md1.dcd"
        psf_name = "segA3.psf"
        com_coord_file = "segA3_com_frames.dat"
        self.com_coord_file = os.path.join(self.data_dir, com_coord_file)
        self.total_number_of_frames = 5
        self.resid_list2 = [42, 43]
        n_residues = len(self.resid_list2)
        n_atoms = 13
        ndim = 3
        n_frames = self.total_number_of_frames

        self.resname_list2 = ["ASP", "ALA"]
        self.segid_list = ['A']
        # self.extra_criteria = "not hydrogen and name CA"
        self.extra_criteria = "not hydrogen"
        self.extra_criteria_mdanalysis = "not name H*"
        self.extra_criteria_prody = "not hydrogen"



        self.expected_selection_str1 = \
            "({0}) and (resid {1} or resid {2}) and (segid {3})".format(self.extra_criteria_mdanalysis,
                                                                         self.resid_list2[0],
                                                                         self.resid_list2[1],
                                                                         self.segid_list[0])

        self.expected_selection_str2 = \
            "({0}) and (resid {1} or resid {2}) and (segment {3})".format(self.extra_criteria_prody,
                                                                         self.resid_list2[0],
                                                                         self.resid_list2[1],
                                                                         self.segid_list[0])
        raw_coords = numpy.loadtxt(os.path.join(self.data_dir,"segA3_coord_frames.dat"))
        self.coord_frames = numpy.zeros((n_atoms,ndim,n_frames))
        for _f in range(n_frames):
            _beg = _f*n_atoms
            _end = (_f+1)*n_atoms
            print("frame",_f)
            self.coord_frames[:,:,_f] = raw_coords[_beg:_end,:]

        raw_com_coords = numpy.loadtxt(self.com_coord_file)
        self.com_data_frames = numpy.zeros((n_residues,ndim,n_frames))
        for _f in range(n_frames):
            _beg = _f*n_residues
            _end = (_f+1)*n_residues
            print("frame",_f)
            self.com_data_frames[:,:,_f] = raw_com_coords[_beg:_end,:]


        self.pairwise_dist = numpy.load(os.path.join(self.data_dir,"segA3_pair_dist.npy"))
        self.pairwise_com_dist = numpy.load(os.path.join(self.data_dir,"segA3_pair_com_dist.npy"))
        self.comdist_data_filename = "segA3_comdist.dat"
        self.commute_time_filename = os.path.join(self.data_dir,"segA3_commute_time.npy")
        self.com_data_filename = "segA3_com.dat"
        self.com_data_filename2 = "segA3_com_ver2.dat"
        self.expected_numAtomsSelected = 18
        self.expected_numAtomsSelected2 = 13
        self.coord_filename = "segA3_coord.dat"
        self.atom_mass_filename = "segA3_atom_masses.dat"
        self.residue_mass_filename = "segA3_residue_masses.dat"
        self.residue_index_range_filename = "segA3_residue_index_range.dat"
        self.mass_weighting_vector_filename = "segA3_mass_weighting_vector.dat"

        self.my_trajectory = os.path.join(self.data_dir, trajectory_name)
        self.my_dcd = os.path.join(self.data_dir, dcd_filename)
        self.my_psf = os.path.join(self.data_dir, psf_name)
        self.allos = allo.Allostery(self.my_psf, self.my_trajectory)

        self.selection_keyword_tuple = ("resid","segid")
        self.selection_member_tuple = (self.resid_list2, self.segid_list)


        self.start_time = time.time()
        # print("start time:", self.start_time)


    def tearDown(self):
        print("-----Tearing down {0} -------\n".format(self.__class__.__name__))
        self.end_time = time.time()
        # print("end time:", self.end_time)
        time_cost = self.end_time - self.start_time
        print("{0:s}: {1:.3e} sec".format(self.id(), time_cost))
        del self.allos

    def test_instantiation(self):
        print("\t[[[ Testing Allostery() object instantiation ]]]")
        self.assertEqual(self.allos.psf_filename, self.my_psf)
        self.assertEqual(self.allos.trajectories[0], self.my_trajectory)


    def test_atom_selection_string(self):
        print("\t[[[ Testing atom_selection_string() ]]]")

        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        self.assertEqual(self.expected_selection_str2,self.allos.get_atom_selection_string())
        print("Atom selection: \t", self.allos.selection_string)
        print("expected string:\t", self.expected_selection_str2)

    def test_atom_selection_without_arg_list(self):
        print("\t[[[ Testing atom_selection_string() ]]]")
        self.allos.select(self.extra_criteria)


    def test_number_selected_atoms(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing get_number_selected_atoms() ]]]")
        self.assertEqual(self.allos.get_number_of_selected_atoms(), self.expected_numAtomsSelected2)
        print("number of atoms selected: {0}; expected: {1}".format(
            self.allos.get_number_of_selected_atoms(),self.expected_numAtomsSelected2))


    #
    def test_reading_atom_coords(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing reading atom coordinates ]]]")
        print("Number of selected atoms: {0}".format(self.allos.get_number_of_selected_atoms()))
        coords = self.allos.get_selected_coords()
        filename = os.path.join(self.data_dir, self.coord_filename)
        expected = numpy.loadtxt(filename)
        self.assertTrue((numpy.abs(coords - expected) < self.tol).all())

    def test_get_selected_atom_masses(self):
        """
        Role: test get_selected_atom_masses()
        :type self: object
        """
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing get mass of selected atoms ]]]")
        masses = self.allos.get_selected_atom_masses()
        filename = os.path.join(self.data_dir, self.atom_mass_filename)
        expected = numpy.loadtxt(filename)
        self.assertTrue(((masses - expected) < self.tol).all())

    def test_get_atomId_range_per_selected_residue(self):
        """
        Role: test get_selected_atom_masses()
        :type self: object
        """
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing get mass of selected residues ]]]")
        ranges = self.allos.get_atomId_range_per_selected_residue()
        filename = os.path.join(self.data_dir, self.residue_index_range_filename)
        expected = numpy.loadtxt(filename)
        print("residue index range:",ranges)
        print("expected ranges:",expected)
        self.assertTrue(((ranges - expected) < self.tol).all())


    def test_get_selected_residue_masses(self):
        """
        Role: test get_selected_atom_masses()
        :type self: object
        """
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing get mass of selected residues ]]]")
        masses = self.allos.get_selected_residue_masses()
        filename = os.path.join(self.data_dir, self.residue_mass_filename)
        expected = numpy.loadtxt(filename)
        print("residue masses:",masses)
        print("expected masses:",expected)
        self.assertTrue(((masses - expected) < self.tol).all())

    def test_get_mass_weighting_vector(self):
        """
        Role: test get_mass_weighting_vector
        """
        print("\t[[[test mass weighting vector]]]")
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_mass_weighting_vector()
        filename = os.path.join(self.data_dir, self.mass_weighting_vector_filename)
        expected = numpy.loadtxt(filename)

        print("result:",result)
        print("expected:",expected)
        self.assertTrue(((result - expected) < self.tol).all())

    def test_get_selected_resids(self):
        """
          Role: test get_selected_resids()
          """
        print("\t[[[ Testing get_selected_resids() ]]]")
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_selected_resids()
        print("Resulting selected resids:", result)
        print("Expected selected resids:", self.resid_list2)
        self.assertTrue((result == self.resid_list2))

    def test_get_selected_resnames(self):
        """
      Role: test get_selected_resnames()
      """
        print("\t[[[ Testing get_selected_resnames() ]]]")
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_selected_resnames()
        expected = self.resname_list2
        print("Resulting selected resnames:", result)
        print("Expected selected resnames:", expected)
        self.assertTrue((result == expected))

    def test_get_selected_segids(self):
        """
      Role: test get_selected_resids()
      """
        print("\t[[[ Testing get_selected_segids() ]]]")
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_selected_segids()
        expected = self.segid_list
        print("Resulting selected resnums:", result)
        print("Expected selected resnums:", expected)
        self.assertTrue((result == expected))

    def test_get_com_matrix(self):
        print("\t[[[ Testing build_com_matrix() ]]]")
        N = len(self.resid_list2)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_selected_com_coords()
        filename = os.path.join(self.data_dir, self.com_data_filename)
        expected = numpy.loadtxt(filename)
        print("resulting comMatrix\n", result)
        print("expected comMatrix\n", expected)
        self.assertEqual(numpy.shape(result), (N, 3))
        self.assertTrue(numpy.abs((result - expected) < self.tol).all())

    def test_iter_get_coords(self):
        print("\t[[[ Test iter_get_coords ]]]")
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        ccc = 0
        # self.allos.goto_frame(0)
        print("frame",self.allos.get_current_frame_id())
        for com_coords in self.allos.iter_get_selected_coords():
            result = com_coords
            expected = self.coord_frames[:,:,ccc]
            print("result",result)
            print("expected",expected)
            ccc += 1
            self.assertTrue((numpy.abs(result-expected)< self.tol).all())

    def test_iter_get_com_coords(self):
        print("\t[[[ Test iter_get_com_coords ]]]")
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        ccc = 0
        for com_coords in self.allos.iter_get_selected_com_coords():
            result = com_coords
            expected = self.com_data_frames[:,:,ccc]
            # print("result",result)
            # print("expected",expected)
            ccc += 1
            self.assertTrue((numpy.abs(result-expected)< self.tol).all())

    def test_get_pair_dist_com_matrix(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing build_pairwise_dist_com_matrix() ]]]")
        result = self.allos.get_pairwise_com_distances()
        filename = os.path.join(self.data_dir, self.comdist_data_filename)
        expected = numpy.loadtxt(filename)
        self.assertTrue((numpy.abs(result - expected) < self.tol).all())

    def test_iter_get_pair_dist_matrix(self):
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing iter_get_pairwise_distances() ]]]")
        ccc = 0
        for _coords in self.allos.iter_get_pairwise_distances():
            result = _coords
            expected = self.pairwise_dist[:,:,ccc]
            ccc += 1
            self.assertTrue((numpy.abs(result - expected) < self.tol).all())

    def test_iter_get_pair_com_dist_matrix(self):
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing iter_get_pairwise_com_distances() ]]]")
        ccc = 0
        for _coords in self.allos.iter_get_pairwise_com_distances():
            result = _coords
            expected = self.pairwise_com_dist[:,:,ccc]
            ccc += 1
            self.assertTrue((numpy.abs(result - expected) < self.tol).all())

    def test_get_number_of_frames(self):
        print("\t[[[ Testing get number of frames]]]")
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_total_number_of_frames()
        expected = self.total_number_of_frames
        self.assertEqual(result, expected)

    def test_get_commute_time_matrix(self):
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing get_commute_time_matrix() ]]]")
        result = self.allos.get_commute_time()
        expected = numpy.load(self.commute_time_filename)
        print("commute time\n", result)
        print("expected\n",expected)
        self.assertTrue((numpy.abs(result - expected) < self.tol).all())

    def test_raise_no_commute_time_yet_error(self):
        print("\t[[[ Testing raise no commute time yes error() ]]]")
        self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        self.assertRaises(UserWarning, self.allos.get_commute_time, False)

    def test_save_commute_time_using_DataIO(self):
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        commuteTime = self.allos.get_commute_time()
        self.dataio = self.allos.get_module("DataIO")
        header = range(self.allos.get_number_of_selected_residues())
        output_data_tag = "commute_time"

        print("\t[[[ Testing save_commute_time_matrix() using DataIO module ]]]")
        filename = "commute_time"
        cwd = os.getcwd()
        myfile_prefix = os.path.join(cwd, self.output_dir, filename)
        myfile = myfile_prefix + ".h5"
        print(myfile)

        if os.path.isfile(myfile):  # remove previous version
            os.remove(myfile)

        self.dataio.write(commuteTime,header,myfile_prefix,output_data_tag)

        self.assertTrue(os.path.isfile(myfile))
        data = pandas.read_hdf(myfile, 'commute_time')
        print("file content: {0}\n".format(myfile), data.values)

    def test_plot_commute_time(self):
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        commuteTime = self.allos.get_commute_time()
        print("\t[[[ Testing plot_commute_time() ]]]")
        self.my_plot = self.allos.get_module("PlotMatrix")
        self.my_plot.plot_matrix(numpy.sqrt(commuteTime))
        offset = self.allos.get_selected_resids()[0]
        new_ticks = [1001, 1002]
        self.my_plot.set_xtick_labels(new_ticks)
        self.my_plot.set_ytick_labels(new_ticks)
        self.my_plot.set_fig_title("Testing Title")
        cmin, cmax = self.my_plot.get_color_limits()
        print("cmin: {0}; cmax: {1}".format(cmin, cmax))
        # self.my_plot.set_color_limits(cmin, 1.0)
        cmin, cmax = self.my_plot.get_color_limits()
        print("new cmin: {0}; new cmax: {1}".format(cmin, cmax))
        # self.my_plot.show()

    # def test_raise_zero_atom_selection_error(self):
        #     print("\t[[[ Testing raise_zero_atom_selection_error() ]]]")
        #     nonexistent_resid = [0]
        #     nonexistent_segid = ['A']
        #     keyword_tuple = ('resid','segid')
        #     member_list = [nonexistent_resid, nonexistent_segid]
        #     base_selection = "all"
        #     self.assertRaises(UserWarning, self.allos.select, base_selection, keyword_tuple, member_list)


if __name__ == '__main__':
    unittest.main()
