"""
Goal: test "allostery" module: algorithms.nearest_neighbor.NearestNeighborMixin
Author: Yuhang Wang
Date: 2014-12-01
"""
# ============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import os
import random
import matplotlib.pylab as plt
import numpy.random as RNG

#--------------------------------------------------
# testing target  module
from allostery.algorithms.nearest_neighbors import NearestNeighborMixin
from app.mutualinfoApp.mutual_information import MutualInfoMixin
#============================================================

def rng_gaussian(n, mu=0, sigma=1, seed=None):
    """
    .. py:function:: rng_gaussian(n)
        Role: generate n random numbers from Gaussian distribution
        :param n: number of desired random numbers
        :param mu: mean
        :param sigma: standard deviation
        :param seed: seed for the random number generation
        :return: a numpy array of random numbers
    """
    random.seed(seed)
    output = numpy.zeros((n,1))
    for i in range(n):
        output[i] = random.gauss(mu, sigma)
    return output


class MI(unittest.TestCase):
    def setUp(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))

        # set numpy random seed
        numpy.random.seed(301415)
        #------------------------------------------------------------
        # test data
        #------------------------------------------------------------
        ## global parameters
        # self.r_corrcoef_pool = [0, 0.3, 0.6, 0.9]
        self.r_corrcoef_pool = [0, 0.3, 0.6, 0.9]
        self.k_kNN_pool = [1, 2, 3, 4, 5]
        self.n_points_pool = numpy.arange(100,3100,100)
        self.N_repeats = 5

        self.tol_cmp = 1E-10 # tolerance for floating point comparison

        ## first make two random variables from Gaussian distribution
        self.dim = 3
        self.norm_type = 'max'
        self.n_sample_points = 4
        self.k_kNN = 1
        self.r_corrcoef = 0.9
        self.dim_of_Z = self.dim*2
        self.n_rand_var = 2 # i.e. random variables X and Y

        ## define covariance matrix
        self.cov  = numpy.eye(self.dim_of_Z)
        for _i in range(self.dim):
            _j = _i + self.dim
            self.cov[_i,_j] = self.r_corrcoef
            self.cov[_j,_i] = self.r_corrcoef

        #self.cov *= 1.0/self.dim # each dimension only share part of the total covariance
        ## define mean of Z for each dimension
        self.mean = numpy.zeros(self.dim_of_Z)

        self.Z = RNG.multivariate_normal(self.mean, self.cov, size=self.n_sample_points)

        self.X = self.Z[:,0:self.dim].reshape((self.n_sample_points,self.dim))
        self.Y = self.Z[:,self.dim:].reshape((self.n_sample_points,self.dim))

        ## objects
        self.NNObj = NearestNeighborMixin()
        self.MIObj = MutualInfoMixin()


    def tearDown(self):
        pass
        # print("\n-----Tear down {0} -------".format(self.__class__.__name__))
        # print("==== DONE ! ====")


    def get_analytical_MI(self, dim, r_corrcoef):
        """
        Role: get analytical solution of the mututal info
        :param dim: dimension of the space
        :param r_corrcoef: correlation coefficient for the same dimension
        :return: mututal info
        """
        return -dim/2.0*numpy.log(1-r_corrcoef**2)

    def get_XY(self, n_sample_poinits, r_corrcoef, mean=None,dim=None):
        """
        Role: set up data
        :param n_sample_poinits: total number of random sample points
        :param r_corrcoef: correlation coefficient between X and Y
        :param mean: mean of the Gaussian distribution (default: 0)
        :param dim: dimension of X or Y
        :return: [X, Y]
        """
        ## first make two random variables from Gaussian distribution
        if dim is None:
            dim = self.dim
        n_rand_var = 2 # number of random varaibles, i.e., X and Y
        dim_of_Z = dim*n_rand_var
        Z = numpy.zeros((n_sample_poinits,dim_of_Z))

        ## define covaraince matrix
        cov = numpy.eye(dim_of_Z)
        for _i in range(dim):
            _j = _i + dim
            cov[_i,_j] = r_corrcoef
            cov[_j,_i] = r_corrcoef
        ## define mean
        if mean is None:
            mean = numpy.zeros(dim_of_Z)

        Z = RNG.multivariate_normal(mean, cov, size=n_sample_poinits)
        ## make sure X and Y are 2D arrays
        X = Z[:,0:dim].reshape((n_sample_poinits,dim))
        Y = Z[:,dim:].reshape((n_sample_poinits,dim))
        return [X, Y]


    def get_mutual_info_KSG1_avg(self, N_repeats, n_points, k, r_corrcoef,method='simple',norm_type='max'):
        """
        Role: batch mode for testing calculation of mututal info using KSG1
        """
        results = numpy.zeros(N_repeats)
        expected_MI = self.get_analytical_MI(self.dim, r_corrcoef)
        # print("MI: {0} vs {1} (self)".format(expected_MI, self.expected_mutualInfo))
        errors = numpy.zeros(N_repeats)
        for i in range(N_repeats):
            X,Y = self.get_XY(n_points, r_corrcoef)
            results[i] = self.MIObj.get_mutual_information(X, Y, k,
                                                           algo='KSG1',
                                                           method=method,
                                                           norm_type=norm_type)
            errors[i] = abs(results[i] - expected_MI)

        mean_error = numpy.mean(errors)
        std_error = numpy.std(errors)

        return [mean_error, std_error]

    def get_mutual_info_KSG2_avg(self, N_repeats, n_points, k, r_corrcoef, method='simple',norm_type='max'):
        """
        Role: batch mode for testing calculation of mututal info using KSG2
        """
        results = numpy.zeros(N_repeats)
        expected_MI = self.get_analytical_MI(self.dim, r_corrcoef)
        errors = numpy.zeros(N_repeats)
        for i in range(N_repeats):
            X,Y = self.get_XY(n_points, r_corrcoef)
            results[i] = self.MIObj.get_mutual_information(X, Y, k,
                                                           algo='KSG2',
                                                           method=method,
                                                           norm_type=norm_type)
            errors[i] = abs(results[i] - expected_MI)

        mean_error = numpy.mean(errors)
        std_error = numpy.std(errors)

        return [mean_error, std_error]

    def vary_n_KSG1(self, k=None, r=None, n_repeats=None, method='kdtree'):
        """
        Role: change number of sample points and calculate mututal information
            using KSG1
        """
        print("=== runGSK: testing varying n (KSG1) ===")
        norm_type = 'max'
        if k is None:
            k = self.k_kNN
        if r is None:
            r = self.r_corrcoef
        if n_repeats is None:
            n_repeats = self.N_repeats

        errors = numpy.zeros((len(self.n_points_pool),3)) + numpy.inf

        print("runGSK:vary_n_KSG1>> r: {0}; k: {1}".format(r, k))
        ccc = 0
        for _n in self.n_points_pool:
            errors[ccc][0] = _n
            errors[ccc][1], errors[ccc][2] = self.get_mutual_info_KSG1_avg(n_repeats,
                                                                           _n,
                                                                           k,
                                                                           r,
                                                                           method,
                                                                           norm_type)
            print("n: {0}; r: {1};  error = {2:.6f} +/- {3:.5f}".format(_n, r, errors[ccc][1], errors[ccc][2]))
            ccc += 1
        output_filename = \
            os.path.join("output","ksg1_vary_n_errors_r{0}_k{1}.npy".format(r, k))
        numpy.save(output_filename, errors)
        errors = None

    def vary_n_KSG2(self, k=None, r=None, n_repeats=None, method='kdtree'):
        """
        Role: change number of sample points and calculate mututal information
            using KSG2
        """
        print("=== runGSK:  varying n (KSG2) ===")
        norm_type = 'max'
        if k is None:
            k = self.k_kNN
        if r is None:
            r = self.r_corrcoef
        if n_repeats is None:
            n_repeats = self.N_repeats

        errors = numpy.zeros((len(self.n_points_pool),3)) + numpy.inf

        print("r: {0}; k: {1}".format(r, k))
        ccc = 0
        for _n in self.n_points_pool:
            errors[ccc][0] = _n
            errors[ccc][1], errors[ccc][2] = self.get_mutual_info_KSG2_avg(n_repeats,
                                                                           _n,
                                                                           k,
                                                                           r,
                                                                           method,
                                                                           norm_type)
            print("n: {0}; error = {1:.6f} +/- {2:.5f}".format(_n, errors[ccc][1], errors[ccc][2]))
            ccc += 1
        output_filename = \
            os.path.join("output","ksg2_vary_n_errors_r{0}_k{1}.npy".format(r, k))
        numpy.save(output_filename, errors)

    def vary_r_n_KSG1(self, k=None):
        """
        vary r and n using KSG1
        """
        print("=== runGSK1: vary r & n ===")
        print("r pool: {0}".format(self.r_corrcoef_pool))
        if k is None:
            k = self.k_kNN
            print("k = {0}".format(k))
        for _r in self.r_corrcoef_pool:
            self.vary_n_KSG1(k=k, r=_r)

    def vary_r_n_KSG2(self, k=None):
        """
        vary r and n using KSG2
        """
        print("=== runGSK2: vary r & n ===")
        print("r pool: {0}".format(self.r_corrcoef_pool))
        if k is None:
            k = self.k_kNN
        for _r in self.r_corrcoef_pool:
            self.vary_n_KSG2(k=k, r=_r)


    def vary_k_n_KSG1(self):
        """
        Role: change  k and n when calculating mututal information
            using KSG1
        """
        print("=== testing varying k & n (KSG1) ===")
        errors = numpy.zeros((len(self.k_kNN_pool),3)) + numpy.inf

        print("r: {0};".format(self.r_corrcoef, self.n_sample_points))
        for _k in self.k_kNN_pool:
           self.vary_n_KSG1(_k, r=self.r_corrcoef)

    def vary_k_n_KSG2(self):
        """
        Role: change  k and n when calculating mututal information
            using KSG2
        """
        print("=== testing varying k & n (KSG2) ===")

        print("r: {0};".format(self.r_corrcoef, self.n_sample_points))
        for _k in self.k_kNN_pool:
           self.vary_n_KSG2(_k, r=self.r_corrcoef)



    def plot_vary_r_n(self, method, r_pool, k):
        """
        plot varying n and keep others constant
        """
        fig,axes = plt.subplots(figsize=(10,8))

        for _r in r_pool:
            print("method: {0}".format(method))
            filename = \
                os.path.join("output", "{0}_vary_n_errors_r{1}_k{2}.npy".format(method,
                                                                                _r,
                                                                                k))
            data = numpy.load(filename)
            X = data[:,0]
            Y = data[:,1]
            stderr = data[:,2]

            axes.errorbar(X,Y, yerr=stderr, linestyle='-', marker='o', markersize=15,
                          label="r: {0}".format(_r))
        # axes.set_xscale('log')
        axes.set_yscale('log')
        axes.set_xlabel("N sample points", fontsize=30)
        axes.set_ylabel("Error", fontsize=30)

        axes.legend(bbox_to_anchor=(0.27,0.28), bbox_transform=plt.gcf().transFigure)
        axes.annotate("Method: {0}".format(method.upper()), xy=(0.15,0.94), xycoords='figure fraction',
                      fontsize=30)

        for _tick in axes.xaxis.get_major_ticks():
            _tick.label.set_fontsize(20)
        for _tick in axes.yaxis.get_major_ticks():
            _tick.label.set_fontsize(20)

        ## x range
        xmin,xmax = axes.get_xlim()
        xmax = 0.1
        # axes.set_xlim((xmin,xmax))
        # offset=50
        # axes.set_xlim(xmin-offset, xmax+offset)
        fig_filename = os.path.join("fig", "fig_{0}_vary_r_n.png".format(method))

        fig.savefig(fig_filename, bbox_inches='tight')
        plt.show()

    def plot_vary_k_n(self, method, k_pool, r):
        """
        plot varying n and keep others constant
        """
        fig,axes = plt.subplots(figsize=(10,8))

        for _k in k_pool:
            print("method: {0}".format(method))
            filename = \
                os.path.join("output", "{0}_vary_n_errors_r{1}_k{2}.npy".format(method,
                                                                                r,
                                                                                _k))
            data = numpy.load(filename)
            X = data[:,0]
            Y = data[:,1]
            stderr = data[:,2]

            axes.errorbar(X,Y, yerr=stderr, linestyle='-', marker='o', markersize=15,
                          label="k: {0}".format(_k))
        axes.set_yscale('log')
        axes.set_xlabel("N sample points", fontsize=30)
        axes.set_ylabel("Error", fontsize=30)

        axes.legend(bbox_to_anchor=(0.25,0.31), bbox_transform=plt.gcf().transFigure)
        axes.annotate("Method: {0}".format(method.upper()), xy=(0.15,0.94), xycoords='figure fraction',
                      fontsize=30)

        for _tick in axes.xaxis.get_major_ticks():
            _tick.label.set_fontsize(20)
        for _tick in axes.yaxis.get_major_ticks():
            _tick.label.set_fontsize(20)

        ## x range
        xmin,xmax = axes.get_xlim()
        offset=50
        axes.set_xlim(xmin-offset, xmax+offset)
        fig_filename = os.path.join("fig", "fig_{0}_vary_k_n.png".format(method))

        fig.savefig(fig_filename, bbox_inches='tight')
        plt.show()

    def test_plot_vary_r_n(self):
        print("=== vary r & n ===")
        # self.vary_r_n_KSG1()
        # self.vary_r_n_KSG2()
        for method in ['ksg1','ksg2']:
          self.plot_vary_r_n(method,self.r_corrcoef_pool, k=self.k_kNN)

    def est_vary_k_n(self):
        print("=== vary k & n ===")
        # self.vary_k_n_KSG1()
        # self.vary_k_n_KSG2()
        for method in ['ksg1','ksg2']:
          self.plot_vary_k_n(method,self.k_kNN_pool, r=self.r_corrcoef)

    def test_speed(self):
        """
        Role: test speed of
        :return:
        """
        pass

if __name__ == "__main__":
  unittest.main()
