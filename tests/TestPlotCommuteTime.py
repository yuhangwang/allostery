"""
Goal: test "allostery" module
Author: Yuhang Wang
Date: 2014-09-16
"""
#============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import unittest
import os
import numpy

#--------------------------------------------------
# testing target  module
from allostery import allo
#============================================================


class TestPlotting(unittest.TestCase):
  def setUp(self):
    print("\n-----Setting up {0} -------".format(self.__class__.__name__))
    #------------------------------------------------------------
    # Parameters
    #------------------------------------------------------------
    data_dir = "data"
    self.output_dir = "output"
    self.tol = 1E-5;
    self.data_dir = os.path.realpath(data_dir)

    trajectory_name = "segA3.pdb"
    dcd_filename = "segA3_md1.dcd"
    psf_name = "segA3.psf"
    self.resid_list = (41, 42, 43)
    self.resid_list2 = [42, 43]
    self.segid_list = ['A']
    self.selection_keyword_tuple = ("resid","segid")
    self.selection_member_tuple = (self.resid_list2, self.segid_list)

    self.extra_criteria = "not hydrogen"
    self.expected_selection_str = \
      "({0}) and (resid {1} or resid {2} or resid {3}) and (segid {4})".format(self.extra_criteria,
                                                                               self.resid_list[0],
                                                                               self.resid_list[1],
                                                                               self.resid_list[2],
                                                                               self.segid_list[0])
    self.comdist_data_filename = "segA3_comdist.dat"
    self.com_data_filename = "segA3_com.dat"
    self.com_data_filename2 = "segA3_com_ver2.dat"
    self.expected_numAtomsSelected = 18
    self.coord_filename = "segA3_coord.dat"
    self.mass_filename = "segA3_mass.dat"

    self.my_trajectory = os.path.join(self.data_dir, trajectory_name)
    self.my_dcd = os.path.join(self.data_dir, dcd_filename)
    self.my_psf = os.path.join(self.data_dir, psf_name)
    self.allos = allo.Allostery(self.my_psf, self.my_trajectory)
    self.allos_dcd = allo.Allostery(self.my_psf, self.my_dcd)



  def tearDown(self):
    print("-----Tearing down {0} -------\n".format(self.__class__.__name__))
    del self.allos

  def test_plot_commute_time(self):
      """
      Role: test the Allostery() class's ability to plotting commute time matrix
      :return: None (visual inspection the result)
      """
      self.allos_dcd.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
      self.allos_dcd.calc_commute_time()
      print("commute time matrix:\n", self.allos_dcd.get_commute_time())
      self.allos_dcd.plot_commute_time()

  def test_save_commute_time(self):
    self.allos_dcd.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
    self.allos_dcd.get_commute_time()
    commuteTime = self.allos.get_commute_time()
    print("\t[[[ Testing plot_commute_time() ]]]")
    self.my_plot = self.allos.get_module("PlotMatrix")
    self.my_plot.plot_matrix(numpy.sqrt(commuteTime))
    offset = self.allos.get_selected_resids()[0]
    new_ticks = [1001, 1002]
    self.my_plot.set_xtick_labels(new_ticks)
    self.my_plot.set_ytick_labels(new_ticks)
    self.my_plot.set_fig_title("Testing Title")
    cmin, cmax = self.my_plot.get_color_limits()
    print("cmin: {0}; cmax: {1}".format(cmin, cmax))
    # self.my_plot.set_color_limits(cmin, 1.0)
    cmin, cmax = self.my_plot.get_color_limits()
    print("new cmin: {0}; new cmax: {1}".format(cmin, cmax))

    myfile_prefix = 'test_commute_time'
    myfile = 'test_commute_time.png'
    myfile = os.path.join(os.getcwd(), self.output_dir, myfile)
    self.my_plot.save(myfile)
    # self.my_plot.show()

    if os.path.isfile(myfile): # remove previous version
      os.remove(myfile)

    self.allos_dcd.savefig_commute_time(myfile_prefix, self.output_dir)
    self.assertTrue(os.path.isfile(myfile))
