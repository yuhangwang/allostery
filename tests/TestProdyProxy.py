"""
Goal: test "alloObjtery" module
Author: Yuhang Wang
Date: 2014-09-16
"""
# ============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import os
import time
import pandas

#--------------------------------------------------
# testing target  module
from allostery import allo
#============================================================


class TestAllosteryBasics(unittest.TestCase):
    def setUp(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))
        #------------------------------------------------------------
        # Parameters
        #------------------------------------------------------------
        data_dir = "data"
        self.output_dir = "output"
        self.tol = 1E-5
        self.data_dir = os.path.realpath(data_dir)


        self.psf_name = os.path.join(self.data_dir, "bigdojf3.psf")

        self.trajectory_name = os.path.join(self.data_dir, "bigdojf3.dcd")
        self.dcd_list = [self.trajectory_name]

        resid_list = range(41,60)
        segid_list = ['A','B','C']
        self.extra_criteria = "not hydrogen and name CA"
        
        self.output_data_filename = os.path.join(self.output_dir, "bigdojf3_gcc.npy")
        
        
        self.selection_keyword_tuple = ('resid', 'segid')
        self.selection_member_tuple = (resid_list, segid_list)
        
        self.frame_stride = 1
        self.alloObj = allo.Allostery(self.psf_name, self.dcd_list, self.frame_stride)

        self.start_time = time.time()

    def tearDown(self):
        print("-----Tearing down {0} -------\n".format(self.__class__.__name__))
        self.end_time = time.time()
        # print("end time:", self.end_time)
        time_cost = self.end_time - self.start_time
        print("{0:s}: {1:.3e} sec".format(self.id(), time_cost))
        del self.alloObj


    def test_gcc(self):
        """
        Test generalized correlation coefficients
        """
        self.alloObj.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)

        t0 = time.time()
        result = self.alloObj.get_generalized_correlation_coefficient(mode="com",
                                                                      rand_seed=301415,
                                                                      parallel=True,
                                                                      frame_skip=self.frame_stride)
        t1 = time.time()
        print("time cost: {0} s".format(t1-t0))
        print("result: \n {0}".format(result))


if __name__ == '__main__':
    unittest.main()
