"""
Goal: test "allostery" module
Author: Yuhang Wang
Date: 2014-09-16
"""
# ============================================================
# Compatibility with Python 3D
# ============================================================
from __future__ import print_function, division, absolute_import

# ============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import pandas
import os
import time

#--------------------------------------------------
# testing target  module
from allostery import allo
#============================================================

def toMillisec(n):
    return n*1000.

class TestTrimer(unittest.TestCase):
    def setUp(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))
        #------------------------------------------------------------
        # Parameters
        #------------------------------------------------------------
        data_dir = "data"
        self.output_dir = "output"
        self.tol = 1E-3
        self.data_dir = os.path.realpath(data_dir)

        base_name = "liteABC"
        psf_name = "{0}.psf".format(base_name)
        pdb_name = "{0}.pdb".format(base_name)
        dcd_filename = "{0}.dcd".format(base_name)

        self.resid_list = [_i for _i in range(105, 108+1)]
        self.segid_list = ['A','B','C']
        self.extra_criteria = "not hydrogen"

        self.expected_masses = numpy.loadtxt(os.path.join(self.data_dir, "{0}_atom_masses.dat".format(base_name)))
        self.expected_com = numpy.loadtxt(os.path.join(self.data_dir, "{0}_com.dat".format(base_name)))
        self.expected_pair_com = numpy.load(os.path.join(self.data_dir,"{0}_pair_com_dist.npy".format(base_name)))
        self.expected_commute_time = numpy.load(os.path.join(self.data_dir, "{0}_commute_time.npy".format(base_name)))
        self.my_pdb = os.path.join(self.data_dir, pdb_name)
        self.my_dcd = os.path.join(self.data_dir, dcd_filename)
        self.my_psf = os.path.join(self.data_dir, psf_name)
        self.allos = allo.Allostery(self.my_psf, self.my_dcd)

        self.selection_keyword_tuple = ("resid","segid")
        self.selection_member_tuple = (self.resid_list, self.segid_list)

        self.start_time = time.time()
        # print("start time:", self.start_time)

    def tearDown(self):
        print("-----Tearing down {0} -------\n".format(self.__class__.__name__))
        self.end_time = time.time()
        # print("end time:", self.end_time)
        time_cost = self.end_time - self.start_time
        print("{0:s}: {1:.3f} ms".format(self.id(), toMillisec(time_cost)))
        del self.allos

    def test_get_selected_atom_masses(self):
        """
        Role: test get_selected_atom_masses()
        :type self: object
        """
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing get mass of selected atoms ]]]")
        result = self.allos.get_selected_atom_masses()
        expected = self.expected_masses
        self.assertTrue(((result - expected) < self.tol).all())
    #
    def test_build_com_matrix(self):
        print("\t[[[ Testing build_com_matrix() ]]]")
        totalNumRes = len(self.resid_list)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        self.start_time = time.time()
        result = self.allos.get_selected_com_coords()
        expected = self.expected_com
        self.assertTrue(((result - expected) < self.tol).all())
    #
    def test_build_pair_dist_com_matrix(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        self.start_time = time.time()
        print("\t[[[ Testing build_pairwise_dist_com_matrix() ]]]")
        n_frames = self.allos.get_total_number_of_frames()
        for _f in range(n_frames):
            print("frame:",_f)
            result = self.allos.get_pairwise_com_distances()
            expected = self.expected_pair_com[:,:,_f]
            if _f < n_frames-1:
                self.allos.goto_next_frame()
            self.assertTrue(((result - expected) < self.tol).all())

    def test_get_commute_time_matrix(self):
        self.allos = self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing get_commute_time_matrix() ]]]")
        result = self.allos.get_commute_time()
        expected = self.expected_commute_time
        print("result\n", result)
        print("expected\n",expected)
        self.assertTrue((numpy.abs(result - expected) < self.tol).all())

    def test_plot_commute_time(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        commuteTime = self.allos.get_commute_time()
        print("\t[[[ Testing plot_commute_time() ]]]")
        self.my_plot = self.allos.get_module("PlotMatrix")
        self.my_plot.plot_matrix(numpy.sqrt(commuteTime))
        new_ticks = self.allos.get_selected_resids()
        n_ticks = len(new_ticks)
        period =  n_ticks//3
        interval = 2
        rotation = 90
        print("new ticks",new_ticks)
        self.my_plot.set_xtick_labels(new_ticks,interval=interval,period=period, rotation=90)
        self.my_plot.set_ytick_labels(new_ticks,interval=interval,period=period)
        hline_pos = range(period, n_ticks, period)
        vline_pos = hline_pos
        print("hlines",hline_pos)
        self.my_plot.add_hline(hline_pos, style='--')
        self.my_plot.add_vline(vline_pos, style='--')
    #

if __name__ == '__main__':
    unittest.main()
