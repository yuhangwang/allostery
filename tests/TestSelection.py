"""
Goal: test "allostery" module
Author: Yuhang Wang
Date: 2014-09-16
"""
#============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import unittest

#--------------------------------------------------
# testing target  module
from allostery.atomselectionlib.selection_tools import SelectionStringMaker
#--------------------------------------------------

#============================================================

class Test1(unittest.TestCase):
    """
    Role: test atom selection string generation
    """

    def setUp(self):
        self.obj = SelectionStringMaker()
        self.str1 = "(resid 1)"
        self.answer1 = ['(', 'resid', '1', ')']+[self.obj.EOF]

    def test_selection_string_split(self):
        """
        Role: test the ability to split selection string correctly into a list
        """
        self.obj.__convert_keywords(self.str1)
        result = self.obj.target
        expected = self.answer1
        self.assertEqual(result, expected)
