"""
Goal: test "allostery" module: algorithms.nearest_neighbor.NearestNeighborMixin
Author: Yuhang Wang
Date: 2014-12-01
"""
# ============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import os
import random
import numpy.random as RNG

#--------------------------------------------------
# testing target  module
from allostery.algorithms.nearest_neighbors import NearestNeighborMixin
from app.mutualinfoApp.mutual_information import MutualInfoMixin
import allostery.guestpkg.NPEET.entropy_estimators as EE
#============================================================

def rng_gaussian(n, mu=0, sigma=1, seed=None):
    """
    .. py:function:: rng_gaussian(n)
        Role: generate n random numbers from Gaussian distribution
        :param n: number of desired random numbers
        :param mu: mean
        :param sigma: standard deviation
        :param seed: seed for the random number generation
        :return: a numpy array of random numbers
    """
    random.seed(seed)
    output = numpy.zeros((n,1))
    for i in range(n):
        output[i] = random.gauss(mu, sigma)
    return output

def add_rand_noise(X, scale=1E-10):
    """
    Role: add some low-level random noise to the input data
        in order to break degeneracy.
    :param X: a mxn matrix where n is the dimension and
        m is the number of data points
    :param scale: scaling factor to the random noise (default: 1E-10)
    :return: X+random_noise
    """
    return X + scale*RNG.rand(*numpy.shape(X))

class TestMI(unittest.TestCase):
    def setUp(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))

        # set numpy random seed
        numpy.random.seed(301415)
        #------------------------------------------------------------
        # test data
        #------------------------------------------------------------
        ## global parameters
        self.r_corrcoef_pool = [0, 0.3, 0.6, 0.9]
        self.k_kNN_pool = [1, 2, 3, 4, 5]
        self.n_points_pool = numpy.arange(100,2000,100)
        self.N_repeats = 5

        self.tol_cmp = 1E-10 # tolerance for floating point comparison

        ## first make two random variables from Gaussian distribution
        self.dim = 3
        self.norm_type = 'max'
        self.n_sample_points = 4
        self.k_kNN = 1
        self.r_corrcoef = 0.9
        self.dim_of_Z = self.dim*2
        self.n_rand_var = 2 # i.e. random variables X and Y

        ## define covariance matrix
        self.cov  = numpy.eye(self.dim_of_Z)
        for _i in range(self.dim):
            _j = _i + self.dim
            self.cov[_i,_j] = self.r_corrcoef
            self.cov[_j,_i] = self.r_corrcoef

        #self.cov *= 1.0/self.dim # each dimension only share part of the total covariance
        ## define mean of Z for each dimension
        self.mean = numpy.zeros(self.dim_of_Z)

        self.Z = RNG.multivariate_normal(self.mean, self.cov, size=self.n_sample_points)

        self.X = self.Z[:,0:self.dim].reshape((self.n_sample_points,self.dim))
        self.Y = self.Z[:,self.dim:].reshape((self.n_sample_points,self.dim))


        self.expected_mutualInfo = -self.dim/2.0*numpy.log(1-self.r_corrcoef**2)

        ## objects
        self.NNObj = NearestNeighborMixin()
        self.MIObj = MutualInfoMixin()


    def tearDown(self):
        pass
        # print("\n-----Tear down {0} -------".format(self.__class__.__name__))
        # print("==== DONE ! ====")



    def get_XY(self, n_sample_poinits, r_corrcoef, mean=None,dim=None):
        """
        Role: set up data
        :param n_sample_poinits: total number of random sample points
        :param r_corrcoef: correlation coefficient between X and Y
        :param mean: mean of the Gaussian distribution (default: 0)
        :param dim: dimension of X or Y
        :return: [X, Y]
        """
        ## first make two random variables from Gaussian distribution
        if dim is None:
            dim = self.dim
        n_rand_var = 2 # number of random varaibles, i.e., X and Y
        dim_of_Z = dim*n_rand_var
        Z = numpy.zeros((n_sample_poinits,dim_of_Z))

        ## define covaraince matrix
        cov = numpy.eye(dim_of_Z)
        for _i in range(dim):
            _j = _i + dim
            cov[_i,_j] = r_corrcoef
            cov[_j,_i] = r_corrcoef
        ## define mean
        if mean is None:
            mean = numpy.zeros(dim_of_Z)

        Z = RNG.multivariate_normal(mean, cov, size=n_sample_poinits)
        ## make sure X and Y are 2D arrays
        X = Z[:,0:dim].reshape((n_sample_poinits,dim))
        Y = Z[:,dim:].reshape((n_sample_poinits,dim))
        return [X, Y]


    def get_mutual_info_KSG1_avg(self, N_repeats, n_points, k, r_corrcoef,method='simple',norm_type='max'):
        """
        Role: batch mode for testing calculation of mututal info using KSG1
        """
        results = numpy.zeros(N_repeats)
        errors = numpy.zeros(N_repeats)
        for i in range(N_repeats):
            X,Y = self.get_XY(n_points, r_corrcoef)
            results[i] = self.MIObj.get_mutual_information(X, Y, k,
                                                           algo='KSG1',
                                                           method=method,
                                                           norm_type=norm_type)
            errors[i] = abs(results[i] - self.expected_mutualInfo)
        mean_result = numpy.mean(results)
        std_result = numpy.std(results)
        mean_error = numpy.mean(errors)
        std_error = numpy.std(errors)

        # # print("kNN_distances\n", kNN_distances)
        # print("key=\t{0}".format(self.expected_mutualInfo))
        # print("MI =\t{0} +/- {1:.4f}".format(mean_result, std_result))
        # print("err=\t{0} +/- {1:.4f}".format(abs(mean_error), std_error))
        return [mean_error, std_error]

    def get_mutual_info_KSG2_avg(self, N_repeats, n_points, k, r_corrcoef, method='simple',norm_type='max'):
        """
        Role: batch mode for testing calculation of mututal info using KSG1
        """
        results = numpy.zeros(N_repeats)
        errors = numpy.zeros(N_repeats)
        for i in range(N_repeats):
            X,Y = self.get_XY(n_points, r_corrcoef)
            results[i] = self.MIObj.get_mutual_information(X, Y, k,
                                                           algo='KSG2',
                                                           method=method,
                                                           norm_type=norm_type)
            errors[i] = abs(results[i] - self.expected_mutualInfo)
        mean_result = numpy.mean(results)
        std_result = numpy.std(results)
        mean_error = numpy.mean(errors)
        std_error = numpy.std(errors)

        # # print("kNN_distances\n", kNN_distances)
        # print("key=\t{0}".format(self.expected_mutualInfo))
        # print("MI =\t{0} +/- {1:.4f}".format(mean_result, std_result))
        # print("err=\t{0} +/- {1:.4f}".format(abs(mean_error), std_error))
        return [mean_error, std_error]

    def get_mutual_info_guest_avg(self, N_repeats, n_points, k, r_corrcoef):
            """
            Role: batch mode for testing calculation of mututal info using KSG1
            """
            results = numpy.zeros(N_repeats)
            errors = numpy.zeros(N_repeats)
            for i in range(N_repeats):
                X,Y = self.get_XY(n_points, r_corrcoef)
                results[i] = EE.mi(X, Y, k, base=numpy.e)
                errors[i] = abs(results[i] - self.expected_mutualInfo)
            mean_result = numpy.mean(results)
            std_result = numpy.std(results)
            mean_error = numpy.mean(errors)
            std_error = numpy.std(errors)

            # # print("kNN_distances\n", kNN_distances)
            # print("key=\t{0}".format(self.expected_mutualInfo))
            # print("MI =\t{0} +/- {1:.4f}".format(mean_result, std_result))
            # print("err=\t{0} +/- {1:.4f}".format(abs(mean_error), std_error))
            return [mean_error, std_error]



    def test_mutual_info_guest_batch(self):
        """
        Role: batch mode for testing calculation of mututal info using guest
        """
        print("=== batch testing guest Mutual Info ===")
        print("r: {0}; k: {1}; dim: {2}".format(self.r_corrcoef, self.k_kNN, self.dim))
        errors = numpy.zeros((len(self.n_points_pool),3)) + numpy.inf
        ccc = 0
        for _n in self.n_points_pool:
            errors[ccc][0] = _n
            errors[ccc][1], errors[ccc][2] = self.get_mutual_info_guest_avg(self.N_repeats,
                                                                           _n,
                                                                           self.k_kNN,
                                                                           self.r_corrcoef)
            print("n: {0}; error = {1:.6f} +/- {2:.5f}".format(_n, errors[ccc][1], errors[ccc][2]))
            ccc += 1


    def est_vary_n_simple_KSG1(self):
        """
        Role: change number of sample points and calculate mututal information
            using KSG1
        """
        print("=== testing varying n (KSG1) ===")
        print("r: {0}; k: {1}; dim: {2}".format(self.r_corrcoef, self.k_kNN, self.dim))
        errors = numpy.zeros((len(self.n_points_pool),3)) + numpy.inf

        ccc = 0
        for _n in self.n_points_pool:
            errors[ccc][0] = _n
            errors[ccc][1], errors[ccc][2] = self.get_mutual_info_KSG1_avg(self.N_repeats,
                                                                           _n,
                                                                           self.k_kNN,
                                                                           self.r_corrcoef)
            print("n: {0}; error = {1:.6f} +/- {2:.5f}".format(_n, errors[ccc][1], errors[ccc][2]))
            ccc += 1
        output_filename = \
            os.path.join("output","ksg1_simple_vary_n_errors_r{0}_k{1}.npy".format(self.r_corrcoef, self.k_kNN))
        numpy.save(output_filename, errors)

    def test_vary_n_KDTree_KSG1(self):
        """
        Role: change number of sample points and calculate mututal information
            using KSG1
        """
        print("=== testing varying n (KSG1) using KDTree ===")
        print("r: {0}; k: {1}; dim: {2}".format(self.r_corrcoef, self.k_kNN, self.dim))
        errors = numpy.zeros((len(self.n_points_pool),3)) + numpy.inf

        ccc = 0
        for _n in self.n_points_pool:
            errors[ccc][0] = _n
            errors[ccc][1], errors[ccc][2] = self.get_mutual_info_KSG1_avg(self.N_repeats,
                                                                           _n,
                                                                           self.k_kNN,
                                                                           self.r_corrcoef,
                                                                           'kdtree',
                                                                           'max')
            print("n: {0}; error = {1:.6f} +/- {2:.5f}".format(_n, errors[ccc][1], errors[ccc][2]))
            ccc += 1
        output_filename = \
            os.path.join("output","ksg1_kdtree_vary_n_errors_r{0}_k{1}.npy".format(self.r_corrcoef, self.k_kNN))
        numpy.save(output_filename, errors)

    def est_vary_n_simple_KSG2(self):
        """
        Role: change number of sample points and calculate mututal information
            using KSG2
        """
        print("=== testing varying n (KSG2) ===")
        errors = numpy.zeros((len(self.n_points_pool),3)) + numpy.inf

        print("r: {0}; k: {1}; dim: {2}".format(self.r_corrcoef, self.k_kNN, self.dim))
        ccc = 0
        for _n in self.n_points_pool:
            errors[ccc][0] = _n
            errors[ccc][1], errors[ccc][2] = self.get_mutual_info_KSG2_avg(self.N_repeats,
                                                                           _n,
                                                                           self.k_kNN,
                                                                           self.r_corrcoef)
            print("n: {0}; error = {1:.6f} +/- {2:.5f}".format(_n, errors[ccc][1], errors[ccc][2]))
            ccc += 1
        output_filename = \
            os.path.join("output","ksg2_vary_n_errors_r{0}_k{1}.npy".format(self.r_corrcoef, self.k_kNN))
        numpy.save(output_filename, errors)

    def test_vary_n_KDTree_KSG2(self):
        """
        Role: change number of sample points and calculate mututal information
            using KSG2
        """
        print("=== testing varying n (KSG2) using KDTree ===")
        errors = numpy.zeros((len(self.n_points_pool),3)) + numpy.inf

        print("r: {0}; k: {1}; dim: {2}".format(self.r_corrcoef, self.k_kNN, self.dim))
        ccc = 0
        for _n in self.n_points_pool:
            errors[ccc][0] = _n
            errors[ccc][1], errors[ccc][2] = self.get_mutual_info_KSG2_avg(self.N_repeats,
                                                                           _n,
                                                                           self.k_kNN,
                                                                           self.r_corrcoef,
                                                                           'kdtree',
                                                                           'max')
            print("n: {0}; error = {1:.6f} +/- {2:.5f}".format(_n, errors[ccc][1], errors[ccc][2]))
            ccc += 1
        output_filename = \
            os.path.join("output","ksg2_kdtree_vary_n_errors_r{0}_k{1}.npy".format(self.r_corrcoef, self.k_kNN))
        numpy.save(output_filename, errors)

    # def test_vary_k_KSG1(self):
    #     """
    #     Role: change  k and calculate mututal information
    #         using KSG1
    #     """
    #     print("=== testing varying k (KSG1) ===")
    #     errors = numpy.zeros((len(self.k_kNN_pool),3)) + numpy.inf
    #
    #     print("r: {0}; n: {1}".format(self.r_corrcoef, self.n_sample_points))
    #     ccc = 0
    #     for _k in self.k_kNN_pool:
    #         errors[ccc][0] = _k
    #         errors[ccc][1], errors[ccc][2] = self.get_mutual_info_KSG1_avg(self.N_repeats,
    #                                                                        self.n_sample_points,
    #                                                                         _k,
    #                                                                         self.r_corrcoef)
    #         print("k: {0}; error = {1:.6f} +/- {2:.5f}".format(_k, errors[ccc][1], errors[ccc][2]))
    #         ccc += 1
    #     output_filename = \
    #         os.path.join("output","ksg1_vary_k_errors_r{0}_n{1}.npy".format(self.r_corrcoef,
    #                                                                         self.n_sample_points))
    #     numpy.save(output_filename, errors)
    #
    # def test_vary_k_KSG2(self):
    #     """
    #     Role: change  k and calculate mututal information
    #         using KSG1
    #     """
    #     print("=== testing varying k (KSG2) ===")
    #     errors = numpy.zeros((len(self.k_kNN_pool),3)) + numpy.inf
    #
    #     print("r: {0}; n: {1}".format(self.r_corrcoef, self.n_sample_points))
    #     ccc = 0
    #     for _k in self.k_kNN_pool:
    #         errors[ccc][0] = _k
    #         errors[ccc][1], errors[ccc][2] = self.get_mutual_info_KSG2_avg(self.N_repeats,
    #                                                                        self.n_sample_points,
    #                                                                         _k,
    #                                                                         self.r_corrcoef)
    #         print("k: {0}; error = {1:.6f} +/- {2:.5f}".format(_k, errors[ccc][1], errors[ccc][2]))
    #         ccc += 1
    #     output_filename = \
    #         os.path.join("output","ksg2_vary_k_errors_r{0}_n{1}.npy".format(self.r_corrcoef,
    #                                                                         self.n_sample_points))
    #     numpy.save(output_filename, errors)

    # def test_plot_vary_n(self):
    #     """
    #     plot varying n and keep others constant
    #     """
    #     fig,axes = plt.subplots(figsize=(10,8))
    #     for _method in ['ksg1','ksg2']:
    #         print("method: {0}".format(_method))
    #         filename = \
    #             os.path.join("output", "{0}_vary_n_errors_r{1}_k{2}.npy".format(_method,
    #                                                                             self.r_corrcoef,
    #                                                                             self.k_kNN))
    #         data = numpy.load(filename)
    #         X = data[:,0]
    #         Y = data[:,1]
    #         stderr = data[:,2]
    #
    #         axes.errorbar(X,Y, stderr, linestyle='-', marker='o', markersize=15)
    #         axes.set_yscale('log')
    #         plt.show()
    #

    # def test_mutual_info_KSG1(self):
    #     """
    #     Role: testing calculation of mutual information
    #     """
    #     print("=== testing KSG1 Mutual Info ===")
    #     mutual_info = self.MIObj.get_mutual_information(self.X, self.Y, self.k_kNN, method='KSG1')
    #     # print("kNN_distances\n", kNN_distances)
    #     print("key=\t{0}".format(self.expected_mutualInfo))
    #     print("MI =\t{0}".format(mutual_info))
    #     err = mutual_info - self.expected_mutualInfo
    #     print("err=\t{0}".format(abs(err)))
    #
    # def test_mutual_info_KSG2(self):
    #     """
    #     Role: testing calculation of mutual information
    #     """
    #     print("=== testing KSG2 Mutual Info ===")
    #     mutual_info = self.MIObj.get_mutual_information(self.X, self.Y, self.k_kNN, method='KSG2')
    #     # print("kNN_distances\n", kNN_distances)
    #     print("key=\t{0}".format(self.expected_mutualInfo))
    #     print("MI =\t{0}".format(mutual_info))
    #     err = mutual_info - self.expected_mutualInfo
    #     print("err=\t{0}".format(abs(err)))
    #
    # def est_guest_entropy_estimator(self):
    #     """
    #     Role: test the entropy estimator written by Greg Ver Steeg
    #     """
    #     print("=== Testing Guest Entropy Estimator ===")
    #     mutual_info = EE.mi(self.X, self.Y, k=self.k_kNN,base=numpy.exp(1))
    #     print("ans=\t{0}".format(self.expected_mutualInfo))
    #     print("MI =\t{0}".format(mutual_info))
    #     err = mutual_info - self.expected_mutualInfo
    #     print("err=\t{0}".format(abs(err)))

    # def test_find_kNN_distance_multi(self):
    #     """
    #     Role: testing NearestNeighborMixin.find_kNN_distance_multi()
    #     """
    #     distances = self.NNObj.find_kNN_distance_multi(self.Z,k=1,subdim=1)
    #     # print("1NN distances (multi-dim):\n", distances)

    def est_kdtree_kNN_distances(self):
        """
        Role: compare the results of kNN distances
            from simple pairwise distances and
            from KDTree algorithm
        """
        print("=== test kdtree kNN distances without subdim")
        results_simple = self.NNObj.simple_find_kNN_distance(self.Z,self.k_kNN,self.norm_type)
        results_kdtree = self.NNObj.kdtree_find_kNN_distance(self.Z,self.k_kNN,self.norm_type)
        # print("results(simple):\n",results_simple)
        # print("results(KDTree):\n",results_kdtree)
        self.assertTrue((numpy.absolute(results_kdtree-results_simple)<self.tol_cmp).all())

    def est_kdtree_kNN_distances_with_subdim(self):
        """
        Role: test the subdim option of kdtree_find_kNN_distance method of
            NearestNeighborMixin
        """
        print("=== test kdtree kNN distances with subdim")
        results_simple = self.NNObj.simple_find_kNN_distance_subdim(self.Z,
                                                                    self.k_kNN,
                                                                    self.norm_type,
                                                                    subdim=self.dim)
        results_kdtree = self.NNObj.kdtree_find_kNN_distance(self.Z,
                                                             self.k_kNN,
                                                             self.norm_type,
                                                             subdim=self.dim)
        # print("results(simple):\n",results_simple)
        # print("results(KDTree):\n",results_kdtree)
        self.assertTrue((numpy.absolute(results_kdtree-results_simple)<self.tol_cmp).all())

    def est_kdtree_count_nearest_neighbors(self):
        """
        Role: compare results of counting number of nearest neighbors using kdtree
            vs. simple method
        """
        print("=== test kdtree count NN ===")
        cutoffs = self.NNObj.simple_find_kNN_distance(self.Z,
                                                      self.k_kNN,
                                                      self.norm_type,)
        results_simple = self.NNObj._simple_count_nearest_neighbors(self.X,cutoffs,self.norm_type)
        results_kdtree = self.NNObj._kdtree_count_nearest_neighbors(self.X,cutoffs,self.norm_type)
        print("results(simple):\n",results_simple)
        print("results(KDTree):\n",results_kdtree)
        self.assertTrue((numpy.absolute(results_kdtree-results_simple)<self.tol_cmp).all())


