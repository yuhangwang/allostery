"""
Test the :py:class:`allostery.proxy.mdtrajProxy.SelectorMixin`
"""

__author__ = 'Yuhang Wang'
__date__ = '02-02-2015'


#---------------------------------------------------
import unittest
import os
from allostery.proxy.prodyProxy.selectorMixin import SelectorMixin
#---------------------------------------------------


class TestProdyProxyFrameMixin(unittest.TestCase):
    """
    .. py:class:: TestMdtrajProxyFrameMixin

        Test the module :py:mod:`allostery.proxy.prodyProxy.frameMixin.FrameMixin`
    """
    def setUp(self):
        data_dir = "data"
        self.data_dir = os.path.realpath(data_dir)

        psf_filename = 'segA.psf'
        dcd_filename = 'segA.dcd'
        dcd2_filename = 'segA_copy1.dcd'

        topology_filename = os.path.join(self.data_dir, psf_filename)
        trajectory_filename = os.path.join(self.data_dir, dcd_filename)
        trajectory2_filename = os.path.join(self.data_dir, dcd2_filename)

        traj_list = [trajectory_filename, trajectory2_filename]

        self.atom_selection_str = 'resname ALA and resid 41'

        self.selectorMixin = SelectorMixin(topology_filename)

    def tearDown(self):
        pass

    def est_getPublicMethods(self):
        """
        Test .getPublicMethods()
        """
        print("\n All Public Methods \n")
        for _method in SelectorMixin.getPublicMethods():
            print(_method)

    def test_getter(self):
        """
        Test all getter functions
        """
        print("\n Test all getter functions\n")
        self.selectorMixin.update_selection(self.atom_selection_str)
        all_attr_list = self.selectorMixin.get_selector_attr_list()
        for _attr in all_attr_list:
            result = getattr(self.selectorMixin,_attr)
            print("{0} = {1}".format(_attr, result))