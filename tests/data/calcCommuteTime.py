from __future__ import print_function
import numpy as np

from scipy.spatial.distance import pdist, cdist
from numpy.linalg import norm
base_name = "liteABC"
myfile = "{0}_pair_com_dist.npy".format(base_name)
output = "{0}_commute_time.npy".format(base_name)
data = np.load(myfile)
variance = np.var(data,axis=2)
print("variance:\n ",variance)

np.save(output, variance)
