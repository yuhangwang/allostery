# extract masses of selected atoms

set base_filename "liteABC"
set psf_filename ${base_filename}.psf
set trajectory_filename ${base_filename}.pdb
set output_filename "${base_filename}_atom_masses.dat"


set OUT [open $output_filename w]


set molId [mol new $psf_filename ]
mol addfile $trajectory_filename waitfor all

set S [atomselect top "name CA"]
set resids [$S get resid]
set resids {105 106 107 108}
set segids {A B C}

set extra "noh"
foreach seg $segids {
    foreach resid_1 $resids {
	  set S1 [atomselect top "resid $resid_1 and segname $seg and $extra"]
	  set coords [$S1 get mass ]
	  foreach mass $coords {
	    puts $OUT "$mass"
	  }
    }
}

close $OUT
exit
