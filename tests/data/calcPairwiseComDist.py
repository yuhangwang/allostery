from __future__ import print_function
import numpy as np

from scipy.spatial.distance import pdist, cdist
from numpy.linalg import norm
base_name = "liteABC"
myfile = "{0}_com_frames.dat".format(base_name)
output = "{0}_pair_com_dist.npy".format(base_name)
nframes = 3
nresidues = 12
ndim = 3
raw_data = np.loadtxt(myfile)
data = np.zeros((nresidues,ndim,nframes))
pdistMatrix = np.zeros((nresidues,nresidues,nframes))
for _f in range(nframes):
    _beg = _f*nresidues
    _end = (_f+1)*nresidues
    print("frame",_f)
    data[:,:,_f] = raw_data[_beg:_end,:]

for i in range(nframes):
    print(data[:,:,i])
    print(norm(data[0,:,i]-data[1,:,i]))
    pdistMatrix[:,:,i] = cdist(data[:,:,i], data[:,:,i])
    print(pdistMatrix[:,:,i])

np.save(output, pdistMatrix)
