# calculate center of mass distance between residues
# in order to generate  comparison data set for debugging
# the python code

set base_filename "liteABC"
set psf_filename ${base_filename}.psf
set trajectory_filename ${base_filename}.dcd
set output_filename "${base_filename}_com_frames.dat"


set OUT [open $output_filename w]


set molId [mol new $psf_filename ]
mol addfile $trajectory_filename watifor all

set S [atomselect top "name CA"]
set resids [$S get resid]
set resids {105 106 107 108}
set segids {A B C}

set n_frames [molinfo top get numframes]

set extra "noh"

for {set f 0} {$f < $n_frames} {incr f} {
    foreach seg $segids {
	foreach resid_1 $resids {
	      set S1 [atomselect top "resid $resid_1 and segname $seg and $extra" frame $f]
	      set com1 [measure center $S1 weight mass]
	      puts $OUT "$com1"
	      $S1 delete
	}
    }
}

close $OUT
exit
