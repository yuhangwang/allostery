# get the mass weigthing factor (residue-wise)

set base_filename "segA3"
set psf_filename ${base_filename}.psf
set trajectory_filename ${base_filename}.pdb
set output_filename "${base_filename}_residue_index_range.dat"


set OUT [open $output_filename w]


set molId [mol new $psf_filename ]
mol addfile $trajectory_filename watifor all

set S [atomselect top "name CA"]
set all_resids [$S get resid]

set resids {42 43}
set extra "noh"
set id_end 0
foreach resid_1 $resids {
      set S1 [atomselect top "resid $resid_1 and $extra"]
      set ids [$S1 get index ]
      set n_atoms [llength $ids]
      set id_begin $id_end
      set id_end   [expr $id_begin + $n_atoms]
      puts $OUT "$id_begin $id_end"
}

close $OUT
exit
