from __future__ import print_function
import numpy as np

from scipy.spatial.distance import pdist, cdist
from numpy.linalg import norm

myfile = "segA3_coord_frames.dat"
output = "segA3_pair_dist.npy"
nframes = 5
natoms = 13
ndim = 3
raw_data = np.loadtxt(myfile)
data = np.zeros((natoms,ndim,nframes))
pdistMatrix = np.zeros((natoms,natoms,nframes))
for _f in range(nframes):
    _beg = _f*natoms
    _end = (_f+1)*natoms
    print("frame",_f)
    data[:,:,_f] = raw_data[_beg:_end,:]

for i in range(nframes):
    print(data[:,:,i])
    print(norm(data[0,:,i]-data[1,:,i]))
    pdistMatrix[:,:,i] = cdist(data[:,:,i], data[:,:,i])
    print(pdistMatrix[:,:,i])

np.save(output, pdistMatrix)
