# extract coordinates of selected atoms

set base_filename "segA3"
set psf_filename ${base_filename}.psf
set trajectory_filename ${base_filename}_md1.dcd
set output_filename "${base_filename}_coord_frames.dat"


set OUT [open $output_filename w]


set molId [mol new $psf_filename ]
mol addfile $trajectory_filename watifor all

set S [atomselect top "name CA"]
set resids [$S get resid]

set resids {42 43}
set S [atomselect top "noh and resid 42 43"]
set n [$S num]
puts "number of coords $n"
set nframes [molinfo top get numframes]
set extra "noh"
for {set f 0} {$f < $nframes} {incr f} {
    puts "frame $f"
    foreach resid $resids {
	  set S1 [atomselect top "resid $resid and $extra" frame $f]
	  set n [$S1 num]
	  set coords [$S1 get {x y z}]
	  foreach xyz $coords {
	    puts $OUT "$xyz"
	  }
	  $S1 delete
    }
}

close $OUT
exit
