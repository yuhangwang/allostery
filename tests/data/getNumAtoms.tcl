# get the mass weigthing factor (residue-wise)

set base_filename "segA3"
set psf_filename ${base_filename}.psf
set trajectory_filename ${base_filename}.pdb




set molId [mol new $psf_filename ]
mol addfile $trajectory_filename watifor all

set sele_str "(noh) and (resid 42 or resid 43) and (segid A)"
set S [atomselect top "$sele_str"]
set numAtoms [$S num]
puts "number of atoms selected: $numAtoms"
exit
