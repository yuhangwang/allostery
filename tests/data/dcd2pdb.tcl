# extract coordinates of selected atoms

set base_filename "segA3"
set psf_filename ${base_filename}.psf
set trajectory_filename ${base_filename}_md1.dcd
set theFrame 0
set output_filename "${base_filename}_md1_frame${theFrame}.dat"


set OUT [open $output_filename w]


set molId [mol new $psf_filename ]
mol addfile $trajectory_filename watifor all

set S [atomselect top "name CA"]
set resids [$S get resid]

set A [atomselect top all]

$A frame $theFrame

$A writepdb $output_filename
exit
