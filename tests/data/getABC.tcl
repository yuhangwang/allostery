# extract masses of selected atoms

set base_filename "full"
set psf_filename ${base_filename}.psf
set pdb_filename ${base_filename}.pdb
set dcd_filename ${base_filename}.dcd
set output_filename "liteABC"

set molId [mol new $psf_filename ]
mol addfile $pdb_filename waitfor all
mol addfile $dcd_filename waitfor all

set resids [$S get resid]
set resids {41 42 43 44 45}
set segids {"A" "B" "C"}

set extra "noh"
set str_resid [join $resids " "]
set str_segid [join $segids " "]
puts "str_resid $str_resid"
puts "str_segid $str_segid"
set S [atomselect top "resid 100 and chain B and $extra"]
$S get segname
$S num

$S frame 0
$S writepdb ${output_filename}.pdb
$S writepsf ${output_filename}.psf

animate write dcd ${output_filename}.dcd beg 1  sel $S


exit
