# calculate center of mass distance between residues
# in order to generate  comparison data set for debugging
# the python code

set base_filename "liteABC"
set psf_filename ${base_filename}.psf
set trajectory_filename ${base_filename}.pdb
set output_filename "${base_filename}_com.dat"


set OUT [open $output_filename w]


set molId [mol new $psf_filename ]
mol addfile $trajectory_filename watifor all

set S [atomselect top "name CA"]
set resids [$S get resid]
set resids {105 106 107 108}
set segids {A B C}

set extra "noh"
foreach seg $segids {
    foreach resid_1 $resids {
	  set S1 [atomselect top "resid $resid_1 and segname $seg and $extra"]
	  set com1 [measure center $S1 weight mass]
	  puts $OUT "$com1"
    }
}

close $OUT
exit
