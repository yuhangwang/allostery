"""
Goal: test "allostery" module
Author: Yuhang Wang
Date: 2014-10-27
"""
# ============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import os
import time

#--------------------------------------------------
# testing target  module
from allostery import allo
#============================================================


class TestCorr(unittest.TestCase):
    def setUp(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))
        #------------------------------------------------------------
        # Parameters
        #------------------------------------------------------------
        data_dir = "data"
        self.output_dir = "output"
        self.tol = 1E-5
        self.data_dir = os.path.realpath(data_dir)

        dcd_filename = "segA3_md1.dcd"
        psf_name = "segA3.psf"

        self.total_number_of_frames = 5
        self.resid_list2 = [42, 43]


        self.resname_list2 = ["ASP", "ALA"]
        self.segid_list = ['A']
        # self.extra_criteria = "not hydrogen and name CA"
        self.extra_criteria = "not hydrogen"
        self.extra_criteria_mdanalysis = "not name H*"
        self.extra_criteria_prody = "not hydrogen"


        self.expected_selection_str2 = \
            "({0}) and (resid {1} or resid {2}) and (segid {3})".format(self.extra_criteria_mdanalysis,
                                                                        self.resid_list2[0],
                                                                        self.resid_list2[1],
                                                                        self.segid_list[0])


        self.expected_com_cov = numpy.load(os.path.join(self.data_dir,"segA3_com_cov.npy"))
        self.expected_avg_com_coords = numpy.load(os.path.join(self.data_dir, "segA3_avg_com_coords.npy"))
        self.expected_com_corrcoef = numpy.load(os.path.join(self.data_dir, "segA3_com_corrcoef.npy"))
        self.my_trajectory = os.path.join(self.data_dir, dcd_filename)
        self.my_dcd = os.path.join(self.data_dir, dcd_filename)
        self.my_psf = os.path.join(self.data_dir, psf_name)
        self.allos = allo.Allostery(self.my_psf, self.my_trajectory)

        self.selection_keyword_tuple = ("resid","segid")
        self.selection_member_tuple = (self.resid_list2, self.segid_list)


        self.start_time = time.time()
        # print("start time:", self.start_time)


    def tearDown(self):
        print("-----Tearing down {0} -------\n".format(self.__class__.__name__))
        self.end_time = time.time()
        # print("end time:", self.end_time)
        time_cost = self.end_time - self.start_time
        print("{0:s}: {1:.3e} sec".format(self.id(), time_cost))
        del self.allos


    #================ Tests ====================#
    def test_avg_coords(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_avg_position()
        expected = self.expected_avg_com_coords
        print("result:\n",result)
        print("expected:\n",expected)
        self.assertTrue((abs(result-expected)<self.tol).all())


    def test_com_cov(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_cross_correlation()
        expected = self.expected_com_cov
        print("result:\n",result)
        print("expected:\n",expected)
        self.assertTrue((abs(result-expected)<self.tol).all())

    def test_com_corrcoef(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_correlation_coefficient()
        expected = self.expected_com_corrcoef
        print("result:\n",result)
        print("expected:\n",expected)
        self.assertTrue((abs(result-expected)<self.tol).all())

    def test_plot(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_correlation_coefficient()
        print("\t[[[ Testing plot() ]]]")
        self.my_plot = self.allos.get_module("PlotMatrix")
        self.my_plot.plot_matrix(result)
        new_ticks = self.allos.get_selected_resids()
        self.my_plot.set_xtick_labels(new_ticks)
        self.my_plot.set_ytick_labels(new_ticks)
        self.my_plot.show()


