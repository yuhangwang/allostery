"""
Goal: test "allostery" module
Author: Yuhang Wang
Date: 2014-09-16
"""
# ============================================================
# Compatibility with Python 3D
# ============================================================
from __future__ import print_function, division, absolute_import

# ============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import pandas
import os
import time

#--------------------------------------------------
# testing target  module
from allostery import allo
#============================================================

def toMillisec(n):
    return n*1000.

class Test2(unittest.TestCase):
    def setUp(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))
        #------------------------------------------------------------
        # Parameters
        #------------------------------------------------------------
        data_dir = "data"
        self.output_dir = "output"
        self.tol = 1E-5
        self.data_dir = os.path.realpath(data_dir)

        pdb_name = "segA.pdb"
        dcd_filename = "segA.dcd"
        psf_name = "segA.psf"
        # self.resid_list = [_i for _i in range(42, 583)]
        self.resid_list = [_i for _i in range(60, 470)]
        self.segid_list = ['A']
        self.extra_criteria = "not hydrogen"

        self.coord_filename = "segA_coord.dat"
        self.mass_filename = "segA_mass.dat"

        self.my_pdb = os.path.join(self.data_dir, pdb_name)
        self.my_dcd = os.path.join(self.data_dir, dcd_filename)
        self.my_psf = os.path.join(self.data_dir, psf_name)
        self.allos = allo.Allostery(self.my_psf, self.my_dcd)
        # self.allos = allo.Allostery(self.my_psf, self.my_dcd)

        self.selection_keyword_tuple = ("resid","segid")
        self.selection_member_tuple = (self.resid_list, self.segid_list)

        self.start_time = time.time()
        # print("start time:", self.start_time)

    def tearDown(self):
        print("-----Tearing down {0} -------\n".format(self.__class__.__name__))
        self.end_time = time.time()
        # print("end time:", self.end_time)
        time_cost = self.end_time - self.start_time
        print("{0:s}: {1:.3f} ms".format(self.id(), toMillisec(time_cost)))
        del self.allos

    def test_build_com_matrix(self):
        print("\t[[[ Testing build_com_matrix() ]]]")
        totalNumRes = len(self.resid_list)
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        self.start_time = time.time()
        result = self.allos.get_selected_com_coords()
        self.assertEqual(numpy.shape(result), (totalNumRes, 3))

    def test_build_pair_dist_com_matrix(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        self.start_time = time.time()
        print("\t[[[ Testing build_pairwise_dist_com_matrix() ]]]")
        self.allos.get_pairwise_com_distances()

    def test_get_commute_time_matrix(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        print("\t[[[ Testing get_commute_time_matrix() ]]]")
        self.start_time = time.time()
        self.allos.get_commute_time()
        print("commute time\n", self.allos.get_commute_time(reCalc=False))

    def test_plot_commute_time(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        commuteTime = self.allos.get_commute_time()
        print("\t[[[ Testing plot_commute_time() ]]]")
        self.my_plot = self.allos.get_module("PlotMatrix")
        self.my_plot.plot_matrix(numpy.sqrt(commuteTime))
        new_ticks = self.allos.get_selected_resids()
        self.my_plot.set_xtick_labels(new_ticks)
        self.my_plot.set_ytick_labels(new_ticks)
        self.my_plot.show()




if __name__ == '__main__':
    unittest.main()
