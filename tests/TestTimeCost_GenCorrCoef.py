"""
Goal: test "allostery" module: CorrelationMixin.get_generalized_correlation
    test the running time
Author: Yuhang Wang
Date: 2014-11-30
"""
# ============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import os
import numpy.random as RNG
import matplotlib.pylab as plt
import time

#--------------------------------------------------
# testing target  module
from allostery.algorithms.nearest_neighbors import NearestNeighborMixin
from app.mutualinfoApp.mutual_information import MutualInfoMixin
from app.gencorrcoefApp.generalized_correlation_coefficient import GenCorrCoefMixin
#============================================================

class GenCorrCoef(object):
    def __init__(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))

        # set numpy random seed
        numpy.random.seed(301415)
        #------------------------------------------------------------
        # test data
        #------------------------------------------------------------
        ## global parameters
        self.r_corrcoef_pool = [0.1, 0.3, 0.6, 0.9]
        # self.r_corrcoef_pool = [0.1]
        self.k_kNN_pool = [1, 2, 3, 4, 5]
        self.n_points_pool = numpy.arange(100,4100,100)
        self.N_repeats = 1

        self.tol_cmp = 1E-10 # tolerance for floating point comparison

        ## first make two random variables from Gaussian distribution
        self.dim = 3
        self.norm_type = 'max'
        self.n_sample_points = 4
        self.k_kNN = 1
        self.r_corrcoef = 0.9
        self.dim_of_Z = self.dim*2
        self.n_rand_var = 2 # i.e. random variables X and Y

        ## define covariance matrix
        self.cov  = numpy.eye(self.dim_of_Z)
        for _i in range(self.dim):
            _j = _i + self.dim
            self.cov[_i,_j] = self.r_corrcoef
            self.cov[_j,_i] = self.r_corrcoef

        #self.cov *= 1.0/self.dim # each dimension only share part of the total covariance
        ## define mean of Z for each dimension
        self.mean = numpy.zeros(self.dim_of_Z)

        self.Z = RNG.multivariate_normal(self.mean, self.cov, size=self.n_sample_points)

        self.X = self.Z[:,0:self.dim].reshape((self.n_sample_points,self.dim))
        self.Y = self.Z[:,self.dim:].reshape((self.n_sample_points,self.dim))

        ## objects
        self.NNObj = NearestNeighborMixin()
        self.MIObj = MutualInfoMixin()
        self.GCCObj = GenCorrCoefMixin()


    def get_analytical_MI(self, dim, r_corrcoef):
        """
        Role: get analytical solution of the mututal info
        :param dim: dimension of the space
        :param r_corrcoef: correlation coefficient for the same dimension
        :return: mututal info
        """
        return -dim/2.0*numpy.log(1-r_corrcoef**2)

    def get_analytical_GenCorrCoef(self, dim, r_corrcoef):
        """
        Role: get analytical solution of the generalized correlaiton coefficient
        :param dim: dimension of the space
        :param r_corrcoef: correlation coefficient for the same dimension
        :return: generalized correlaiton coefficient
        """
        MI = self.get_analytical_MI(dim,r_corrcoef)
        genCorrCoef = numpy.sqrt(1.0-numpy.exp(-2.0/dim*MI))
        return genCorrCoef

    def get_XY(self, n_sample_poinits, r_corrcoef, mean=None,dim=None):
        """
        Role: set up data
        :param n_sample_poinits: total number of random sample points
        :param r_corrcoef: correlation coefficient between X and Y
        :param mean: mean of the Gaussian distribution (default: 0)
        :param dim: dimension of X or Y
        :return: [X, Y]
        """
        ## first make two random variables from Gaussian distribution
        if dim is None:
            dim = self.dim
        n_rand_var = 2 # number of random varaibles, i.e., X and Y
        dim_of_Z = dim*n_rand_var
        Z = numpy.zeros((n_sample_poinits,dim_of_Z))

        ## define covaraince matrix
        cov = numpy.eye(dim_of_Z)
        for _i in range(dim):
            _j = _i + dim
            cov[_i,_j] = r_corrcoef
            cov[_j,_i] = r_corrcoef
        ## define mean
        if mean is None:
            mean = numpy.zeros(dim_of_Z)

        Z = RNG.multivariate_normal(mean, cov, size=n_sample_poinits)
        ## make sure X and Y are 2D arrays
        X = Z[:,0:dim].reshape((n_sample_poinits,dim))
        Y = Z[:,dim:].reshape((n_sample_poinits,dim))
        return [X, Y]

    def get_gencorrcoef_KSG1_avg(self, N_repeats, n_points, k, r_corrcoef,method='simple',norm_type='max'):
        """
        Role: batch mode for testing calculation of generalized correlation coefficient using KSG1
        """
        results = numpy.zeros(N_repeats)
        expected= self.get_analytical_GenCorrCoef(self.dim, r_corrcoef)
        # print("MI: {0} vs {1} (self)".format(expected_MI, self.expected_mutualInfo))
        errors = numpy.zeros(N_repeats)
        for i in range(N_repeats):
            X,Y = self.get_XY(n_points, r_corrcoef)
            results[i] = self.GCCObj.get_generalized_correlation_coefficient(X, Y,
                                                                             k,
                                                                             algo='KSG1',
                                                                             method=method,
                                                                             norm_type=norm_type)
            errors[i] = abs(results[i] - expected)

        mean_error = numpy.mean(errors)
        std_error = numpy.std(errors)

        return [mean_error, std_error]

    def get_gencorrcoef_KSG2_avg(self, N_repeats, n_points, k, r_corrcoef, method='simple',norm_type='max'):
        """
        Role: batch mode for testing calculation of  generalized correlation coefficient using KSG2
        """
        results = numpy.zeros(N_repeats)
        expected = self.get_analytical_GenCorrCoef(self.dim, r_corrcoef)
        errors = numpy.zeros(N_repeats)
        for i in range(N_repeats):
            X,Y = self.get_XY(n_points, r_corrcoef)
            results[i] = self.GCCObj.get_generalized_correlation_coefficient(X, Y,
                                                                             k,
                                                                             algo='KSG2',
                                                                             method=method,
                                                                             norm_type=norm_type)
            errors[i] = abs(results[i] - expected)

        mean_error = numpy.mean(errors)
        std_error = numpy.std(errors)

        return [mean_error, std_error]


    def vary_n(self, algo, method, n_data_points, k=None, r=None, n_repeats=1):
        """
        Role: change number of sample points and calculate generalized correlation coefficient
            using KSG2
        """
        # print("=== runGSK:  varying n (KSG2) ===")
        norm_type = 'max'
        if k is None:
            k = self.k_kNN
        if r is None:
            r = self.r_corrcoef
        if n_repeats is None:
            n_repeats = self.N_repeats

        if algo == 'ksg1':
            fn_gcc = self.get_gencorrcoef_KSG1_avg
        elif algo == 'ksg2':
            fn_gcc = self.get_gencorrcoef_KSG2_avg
        else:
            msg = "ERROR HINT: unknown algorithm found: {0}".format(algo)
            raise UserWarning(msg)

        errors = numpy.zeros((len(self.n_points_pool),3)) + numpy.inf

        # print("r: {0}; k: {1}".format(r, k))

        fn_gcc(n_repeats,
                n_data_points,
                k,
                r,
                method,
                norm_type)
        return


def calc_time_cost(algo, method, n_data_points, N_repeats=5):
    gcc = GenCorrCoef()
    time_array = numpy.zeros(N_repeats)

    for i in range(N_repeats):
        t0 = time.time()
        gcc.vary_n(algo=algo, method=method, n_data_points=n_data_points)
        t1 = time.time()
        time_array[i] = t1 - t0
    t_cost = numpy.mean(time_array)
    t_std = numpy.std(time_array)
    return [t_cost, t_std]

class Test(unittest.TestCase):
    def setUp(self):
        self.list_algo = ['ksg1', 'ksg2']
        self.list_method = ['simple', 'kdtree']
        self.list_n_points = numpy.arange(100, 3100, 100)

    def test_speed(self):
        """
        Role: test speed of
        :return:
        """
        for algo in self.list_algo:
            for method in self.list_method:
                output_filename = "output/time_gcc_{0}_{1}.npy".format(algo, method)
                time_cost = numpy.zeros((len(self.list_n_points), 3))
                ccc = 0
                for n in self.list_n_points:
                    time_cost[ccc,0] = n
                    t_mean, t_std = calc_time_cost(algo, method, n)
                    time_cost[ccc,1] = t_mean
                    time_cost[ccc,2] = t_std
                    ccc += 1
                    print("n {0}; t={1}+/={2} s".format(n, t_mean, t_std))
                numpy.save(output_filename, time_cost)

    def test_plot_time(self):
        """
        plot varying n and keep others constant
        """
        fig,axes = plt.subplots(figsize=(10,8))

        for _algo in self.list_algo:
            for _method in self.list_method:
                filename = os.path.join("output", "time_gcc_{0}_{1}.npy".format(_algo,_method))
                data = numpy.load(filename)
                X = data[:,0]
                Y = data[:,1]
                stderr = data[:,2]

                axes.errorbar(X,Y, yerr=stderr, linestyle='-', marker='o', markersize=5,
                              label="{0} & {1}".format(_algo.upper(), _method))
        axes.set_xscale('log')
        axes.set_yscale('log')
        axes.set_xlabel("N sample points", fontsize=30)
        axes.set_ylabel("Time cost (sec)", fontsize=30)

        axes.legend(bbox_to_anchor=(0.9,0.28), bbox_transform=plt.gcf().transFigure)

        axes.set_title("Efficiency of different algorithms", fontsize=25, y=1.04)
        # axes.annotate("", xy=(0.15,0.94), xycoords='figure fraction',
        #               fontsize=30)

        for _tick in axes.xaxis.get_major_ticks():
            _tick.label.set_fontsize(20)
        for _tick in axes.yaxis.get_major_ticks():
            _tick.label.set_fontsize(20)

        ## x range
        xmin,xmax = axes.get_xlim()
        offset=50
        axes.set_xlim(xmin-offset, xmax+offset)
        fig_filename = os.path.join("fig", "fig_time_cost_gcc.png")

        fig.savefig(fig_filename, bbox_inches='tight')
        plt.show()

if __name__ == "__main__":
  unittest.main()
