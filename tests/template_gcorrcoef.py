
# coding: utf-8

# In[26]:

"""
Goal: test "allostery" module: CorrelationMixin.get_generalized_correlation
Author: Yuhang Wang
Date: 2014-11-30
"""
# ============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import os
import time

#--------------------------------------------------
# testing target  module
from allostery import allo
#============================================================


#------------------------------------------------------------
# User input parameters
#------------------------------------------------------------
data_dir = os.path.realpath(".")
psf_filename = "data/EFTU-GTP.psf"
traj_filename_list = ["data/EFTU-GTP.dcd"]
resid_list = range(1,5)
segid_list = ['P']
extra_sele_str = "not hydrogen and name CA"
output_data_filename = "output/tmp_gcorrcoef.npy"


def plot(self, data, output_figname):
    """
    plot results of the parallel run
    """
    self.my_plot = self.allos.get_module("PlotMatrix")
    self.my_plot.plot_matrix(data)
    new_ticks = self.allos.get_selected_resids()
    tick_interval = 20
    self.my_plot.set_xtick_labels(new_ticks, interval=tick_interval, rotation=90)
    self.my_plot.set_ytick_labels(new_ticks, interval=tick_interval)
    self.my_plot.save(output_figname)
    self.my_plot.show()


#===================================================================
# Main
#===================================================================
keyword_tuple= ('resid', 'segid')
member_tuple = (resid_list, segid_list)
alloObj = allo.Allostery(psf_filename, traj_filename_list)
alloObj.select(extra_sele_str, keyword_tuple, member_tuple)

t0 = time.time()
result = alloObj.get_generalized_correlation_coefficient(mode="com",rand_seed=301415, parallel=True)
t1 = time.time()

print("parallel t cost = {0}".format(t1-t0))
print("result\n",result)

numpy.save(output_data_filename,result)
print("shape of result: {0}".format(result.shape))


