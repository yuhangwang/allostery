"""
Test the :py:class:`allostery.proxy.mdtrajProxy.TrajectorySeriesInfoMixin`
"""

__author__ = 'Yuhang Wang'
__date__ = '02-02-2015'


#---------------------------------------------------
import unittest
import os
import mdtraj as MDTraj
import prody as ProDy
from allostery.proxy.prodyProxy.trajectorySeriesInfoMixin import TrajectorySeriesInfoMixin
#---------------------------------------------------


class TestMdtrajProxy(unittest.TestCase):
    """
    .. py:class:: TestMdtrajProxy

        Test the module :py:mod:`allostery.proxy.prodyProxy`
    """
    def setUp(self):
        data_dir = "data"
        self.data_dir = os.path.realpath(data_dir)

        psf_filename = 'segA.psf'
        dcd_filename = 'segA.dcd'
        dcd2_filename = 'segA_copy1.dcd'
        topology_filename = os.path.join(self.data_dir, psf_filename)
        trajectory_filename = os.path.join(self.data_dir, dcd_filename)
        trajectory2_filename = os.path.join(self.data_dir, dcd2_filename)

        traj_list = [trajectory_filename, trajectory2_filename]

        self.userTrajectory = MDTraj.load(trajectory_filename,top=topology_filename)
        self.userTopology = self.userTrajectory.topology
        self.helperUserData =  ProDy.parsePSF(topology_filename)

        self.trajInfoObj = TrajectorySeriesInfoMixin(traj_list, topology_filename)


    def tearDown(self):
        pass

    def test_get_public_methods(self):
        method_list = self.trajInfoObj.get_public_methods()
        print(method_list)

    def test_getPublicMethods(self):
        method_list = TrajectorySeriesInfoMixin.getPublicMethods()
        print(method_list)

    def test_get_num_files(self):
        n_files = self.trajInfoObj.total_num_trajectories
        print("number of files: {0}".format(n_files))

    def test_get_total_frames(self):
        total_frames = self.trajInfoObj.get_total_num_frames()
        print("Total frames: {0}".format(total_frames))

    def test_get_list_num_frames(self):
        my_list = self.trajInfoObj.get_list_num_frames()
        print("List of number of frames:  {0}".format(my_list))

    def test_get_filename_and_frameId(self):
        test_ids = [0, 2, 5, 9]
        expected_ids = [0, 2, 0, 4]
        for i in range(len(test_ids)):
            _inputId = test_ids[i]
            _expectedId = expected_ids[i]
            _filename, _resultId = self.trajInfoObj.get_filename_and_frameId(_inputId)
            self.assertEqual(_resultId, _expectedId)
            print("{0} ==> {1}".format(_inputId, _resultId))
            print("filename: {0}".format(_filename))




