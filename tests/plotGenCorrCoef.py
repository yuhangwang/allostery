
# coding: utf-8

# In[16]:

"""
Goal: test "allostery" module: CorrelationMixin.get_generalized_correlation
Author: Yuhang Wang
Date: 2014-11-30
"""
# ============================================================
# Compatibility with Python 3D
#============================================================
from __future__ import print_function, division, absolute_import

#============================================================

#============================================================
# Dependencies
#============================================================
import numpy
import unittest
import os
import time

#--------------------------------------------------
# testing target  module
from allostery import allo
#============================================================


class TestGenCorrCoef(unittest.TestCase):
    def setUp(self):
        print("\n-----Setting up {0} -------".format(self.__class__.__name__))
        #------------------------------------------------------------
        # Parameters
        #------------------------------------------------------------
        data_dir = "data"
        self.output_dir = "output"
        self.tol = 1E-5
        self.data_dir = os.path.realpath(data_dir)

        dcd_filename = "EFTU-GTP.dcd"
        psf_name = "EFTU-GTP.psf"

        self.total_number_of_frames = 5
        self.last_resid= 405 
        self.resid_list1 =  numpy.arange(1,self.last_resid+1)

        self.rand_seed = 301415


        self.segid_list = ['P']
        # self.extra_criteria = "not hydrogen and name CA"
        extra = " and name CA"
        self.extra_criteria = "not hydrogen"+extra
        self.extra_criteria_mdanalysis = "not name H*"+extra
        self.extra_criteria_prody = "not hydrogen"+extra

        self.my_trajectory = os.path.join(self.data_dir, dcd_filename)
        self.my_dcd = os.path.join(self.data_dir, dcd_filename)
        self.my_psf = os.path.join(self.data_dir, psf_name)
        self.allos = allo.Allostery(self.my_psf, self.my_trajectory)

        self.selection_keyword_tuple = ("resid","segid")
        self.selection_member_tuple = (self.resid_list1, self.segid_list)


        self.start_time = time.time()
        # print("start time:", self.start_time)

        self.tol_cmp = 1E-3 # tolerance for comparison


    def tearDown(self):
        print("-----Tearing down {0} -------\n".format(self.__class__.__name__))
        self.end_time = time.time()
        # print("end time:", self.end_time)
        time_cost = self.end_time - self.start_time
        print("{0:s}: {1:.3e} sec".format(self.id(), time_cost))
        del self.allos
    #

    def test_plot_corrcoef(self):
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = self.allos.get_correlation_coefficient()
        output_data_filename = "data/eftu_corrcoef.npy"
        numpy.save(output_data_filename,result)
        print("\t[[[ Testing plot() ]]]")
        self.my_plot = self.allos.get_module("PlotMatrix")
        self.my_plot.plot_matrix(result)
        new_ticks = self.allos.get_selected_resids()
        tick_interval = 20
        self.my_plot.set_xtick_labels(new_ticks, interval=tick_interval, rotation=90)
        self.my_plot.set_ytick_labels(new_ticks, interval=tick_interval)
        output_figname = "fig/eftu_corrcoef.png"
        self.my_plot.save(output_figname)
        self.my_plot.show()

    def test_plot(self):
        """
        plot results of the parallel run
        """
        self.allos.select(self.extra_criteria, self.selection_keyword_tuple, self.selection_member_tuple)
        result = numpy.load(os.path.join("output", "parallel_eftu_gencorrcoef.npy"))
        print("\t[[[ Testing plot() ]]]")
        self.my_plot = self.allos.get_module("PlotMatrix")
        self.my_plot.plot_matrix(result)
        new_ticks = self.allos.get_selected_resids()
        tick_interval = 20
        self.my_plot.set_xtick_labels(new_ticks, interval=tick_interval, rotation=90)
        self.my_plot.set_ytick_labels(new_ticks, interval=tick_interval)
        output_figname = "fig/parallel_eftu_gencorrcoef.png"
        self.my_plot.save(output_figname)
        self.my_plot.show()
        
        
        
suite = unittest.TestLoader().loadTestsFromTestCase(TestGenCorrCoef)
unittest.TextTestRunner().run(suite)



# In[6]:




# In[3]:




# In[ ]:



